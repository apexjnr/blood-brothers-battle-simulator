const cardDatabase = {
  "1": {
    "name": "Black Brute",
    "stats": {
      "HP": 14254,
      "ATK": 17131,
      "DEF": 13848,
      "WIS": 11794,
      "AGI": 11699
    },
    "skills": [
      34
    ],
    "isWarlord": true,
    "img": "36f",
    "rarity": 2,
    "evo": 1,
    "fullName": "The Black Brute"
  },
  "2": {
    "name": "Blue Beard",
    "stats": {
      "HP": 12982,
      "ATK": 11344,
      "DEF": 15588,
      "WIS": 15554,
      "AGI": 13527
    },
    "skills": [
      118
    ],
    "isWarlord": true,
    "img": "10a",
    "rarity": 2,
    "evo": 1,
    "fullName": "The Blue Beard"
  },
  "3": {
    "name": "Golden Lance",
    "stats": {
      "HP": 14462,
      "ATK": 13994,
      "DEF": 11951,
      "WIS": 12227,
      "AGI": 16809
    },
    "skills": [
      10
    ],
    "isWarlord": true,
    "img": "3d6",
    "rarity": 1,
    "evo": 1,
    "fullName": "The Golden Lance"
  },
  "4": {
    "name": "Green Healer",
    "stats": {
      "HP": 13770,
      "ATK": 10556,
      "DEF": 16359,
      "WIS": 15329,
      "AGI": 13596
    },
    "skills": [
      116,
      111
    ],
    "isWarlord": true,
    "img": "265",
    "rarity": 1,
    "evo": 1,
    "fullName": "The Green Healer"
  },
  "5": {
    "name": "Grey Mage",
    "stats": {
      "HP": 13415,
      "ATK": 13838,
      "DEF": 10712,
      "WIS": 15865,
      "AGI": 16602
    },
    "skills": [
      40
    ],
    "isWarlord": true,
    "img": "248",
    "rarity": 2,
    "evo": 1,
    "fullName": "The Grey Mage"
  },
  "6": {
    "name": "Purple Knife",
    "stats": {
      "HP": 13735,
      "ATK": 16281,
      "DEF": 10712,
      "WIS": 15779,
      "AGI": 13595
    },
    "skills": [
      113
    ],
    "isWarlord": true,
    "img": "3ee",
    "rarity": 2,
    "evo": 3,
    "fullName": "The Purple Knife"
  },
  "7": {
    "name": "Red Samurai",
    "stats": {
      "HP": 13432,
      "ATK": 14783,
      "DEF": 13961,
      "WIS": 12869,
      "AGI": 14333
    },
    "skills": [
      46
    ],
    "isWarlord": true,
    "img": "4ad",
    "rarity": 1,
    "evo": 1,
    "fullName": "The Red Samurai"
  },
  "8": {
    "name": "White Knight",
    "stats": {
      "HP": 13916,
      "ATK": 14332,
      "DEF": 15311,
      "WIS": 12851,
      "AGI": 13466
    },
    "skills": [
      46
    ],
    "isWarlord": true,
    "img": "225",
    "rarity": 3,
    "evo": 1,
    "fullName": "The White Knight"
  },
  "201": {
    "name": "Gan Ceann",
    "stats": {
      "HP": 7950,
      "ATK": 10530,
      "DEF": 8830,
      "WIS": 8910,
      "AGI": 8540
    },
    "skills": [
      33
    ],
    "img": "2ca",
    "rarity": 4,
    "evo": 1,
    "fullName": "Gan Ceann"
  },
  "308": {
    "name": "Mordred",
    "stats": {
      "HP": 11000,
      "ATK": 12050,
      "DEF": 10950,
      "WIS": 11000,
      "AGI": 12500
    },
    "skills": [
      18
    ],
    "img": "16b",
    "rarity": 5,
    "evo": 1,
    "fullName": "Mordred, Drake Knight"
  },
  "327": {
    "name": "Tangata",
    "stats": {
      "HP": 10500,
      "ATK": 10800,
      "DEF": 10630,
      "WIS": 10740,
      "AGI": 12480
    },
    "skills": [
      110
    ],
    "img": "3b4",
    "rarity": 5,
    "evo": 1,
    "fullName": "Tangata Manu"
  },
  "332": {
    "name": "Mari",
    "stats": {
      "HP": 10500,
      "ATK": 10980,
      "DEF": 10850,
      "WIS": 13370,
      "AGI": 11500
    },
    "skills": [
      47
    ],
    "img": "1e4",
    "rarity": 5,
    "evo": 1,
    "fullName": "Mari the Witch"
  },
  "358": {
    "name": "Aegis",
    "stats": {
      "HP": 14560,
      "ATK": 11280,
      "DEF": 15530,
      "WIS": 10600,
      "AGI": 10100
    },
    "skills": [
      64
    ],
    "img": "235",
    "rarity": 5,
    "evo": 1,
    "fullName": "Aegis, the Bulwark"
  },
  "361": {
    "name": "Griflet",
    "stats": {
      "HP": 11520,
      "ATK": 12970,
      "DEF": 11430,
      "WIS": 10110,
      "AGI": 13780
    },
    "skills": [
      10
    ],
    "img": "2b1",
    "rarity": 5,
    "evo": 1,
    "fullName": "Griflet, Falcon Knight"
  },
  "414": {
    "name": "Steamwork",
    "stats": {
      "HP": 14360,
      "ATK": 10800,
      "DEF": 10600,
      "WIS": 12240,
      "AGI": 10560
    },
    "skills": [
      11
    ],
    "img": "3de",
    "rarity": 5,
    "evo": 1,
    "fullName": "Steamwork Dragon"
  },
  "693": {
    "name": "Ioskeha",
    "stats": {
      "HP": 13138,
      "ATK": 13611,
      "DEF": 11162,
      "WIS": 15329,
      "AGI": 13675
    },
    "skills": [
      160
    ],
    "img": "222",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ioskeha"
  },
  "695": {
    "name": "Tawiscara",
    "stats": {
      "HP": 11914,
      "ATK": 14513,
      "DEF": 14395,
      "WIS": 11366,
      "AGI": 15630
    },
    "skills": [
      161
    ],
    "img": "3f5",
    "rarity": 4,
    "evo": 2,
    "fullName": "Tawiscara"
  },
  "1609": {
    "name": "Charybdis",
    "stats": {
      "HP": 14048,
      "ATK": 16042,
      "DEF": 13918,
      "WIS": 9000,
      "AGI": 16887
    },
    "skills": [
      912
    ],
    "img": "3c9",
    "rarity": 4,
    "evo": 2,
    "fullName": "Charybdis II"
  },
  "10011": {
    "name": "Gorgon",
    "stats": {
      "HP": 10170,
      "ATK": 12436,
      "DEF": 8652,
      "WIS": 12773,
      "AGI": 10924
    },
    "skills": [
      18
    ],
    "img": "46f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Gorgon II"
  },
  "10013": {
    "name": "Pendragon",
    "stats": {
      "HP": 9844,
      "ATK": 10317,
      "DEF": 10751,
      "WIS": 12357,
      "AGI": 10861
    },
    "skills": [
      60
    ],
    "img": "345",
    "rarity": 4,
    "evo": 4,
    "fullName": "Pendragon, the Scourge II"
  },
  "10022": {
    "name": "Galahad",
    "stats": {
      "HP": 6543,
      "ATK": 7271,
      "DEF": 7349,
      "WIS": 6842,
      "AGI": 6478
    },
    "skills": [
      10000,
      33,
      5
    ],
    "isMounted": true,
    "img": "4e2",
    "rarity": 4,
    "evo": 2,
    "fullName": "Galahad, Drake Knight II"
  },
  "10075": {
    "name": "Pouliquen",
    "stats": {
      "HP": 7890,
      "ATK": 6271,
      "DEF": 8910,
      "WIS": 9439,
      "AGI": 7843
    },
    "skills": [
      16
    ],
    "img": "26c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Pouliquen, Archibishop II"
  },
  "10088": {
    "name": "Ghislandi",
    "stats": {
      "HP": 12324,
      "ATK": 13551,
      "DEF": 13525,
      "WIS": 12212,
      "AGI": 12187
    },
    "skills": [
      17
    ],
    "img": "468",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ghislandi, Iron Heart II"
  },
  "10089": {
    "name": "Granados",
    "stats": {
      "HP": 10725,
      "ATK": 11825,
      "DEF": 11350,
      "WIS": 10541,
      "AGI": 10418
    },
    "skills": [
      19
    ],
    "img": "170",
    "rarity": 4,
    "evo": 4,
    "fullName": "Granados, Lion King II"
  },
  "10177": {
    "name": "Goblin King",
    "stats": {
      "HP": 8144,
      "ATK": 8339,
      "DEF": 6400,
      "WIS": 10159,
      "AGI": 10278
    },
    "skills": [
      18
    ],
    "img": "34f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Goblin King II"
  },
  "10182": {
    "name": "Griffin",
    "stats": {
      "HP": 11887,
      "ATK": 9909,
      "DEF": 14391,
      "WIS": 14263,
      "AGI": 11960
    },
    "skills": [
      2
    ],
    "img": "457",
    "rarity": 4,
    "evo": 4,
    "fullName": "Griffin Mount II"
  },
  "10186": {
    "name": "Naberius",
    "stats": {
      "HP": 9563,
      "ATK": 9552,
      "DEF": 7828,
      "WIS": 11208,
      "AGI": 11298
    },
    "skills": [
      18
    ],
    "img": "2e9",
    "rarity": 4,
    "evo": 4,
    "fullName": "Naberius II"
  },
  "10258": {
    "name": "Sekhmet",
    "stats": {
      "HP": 12529,
      "ATK": 16780,
      "DEF": 13843,
      "WIS": 13598,
      "AGI": 13823
    },
    "skills": [
      11
    ],
    "img": "3d7",
    "rarity": 4,
    "evo": 4,
    "fullName": "Sekhmet Aflame II"
  },
  "10272": {
    "name": "Cat Sidhe",
    "stats": {
      "HP": 9614,
      "ATK": 8322,
      "DEF": 11959,
      "WIS": 11243,
      "AGI": 10056
    },
    "skills": [
      18
    ],
    "img": "448",
    "rarity": 4,
    "evo": 4,
    "fullName": "Earl Cat Sidhe II"
  },
  "10276": {
    "name": "Grim",
    "stats": {
      "HP": 11001,
      "ATK": 13047,
      "DEF": 8888,
      "WIS": 13026,
      "AGI": 11060
    },
    "skills": [
      109
    ],
    "img": "17f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Grim Executioner II"
  },
  "10303": {
    "name": "Crystal Gillant",
    "stats": {
      "HP": 11832,
      "ATK": 10896,
      "DEF": 10439,
      "WIS": 10439,
      "AGI": 13317
    },
    "skills": [
      11
    ],
    "img": "460",
    "rarity": 4,
    "evo": 4,
    "fullName": "Crystal Gillant II"
  },
  "10317": {
    "name": "Eton",
    "stats": {
      "HP": 10904,
      "ATK": 10490,
      "DEF": 10490,
      "WIS": 12952,
      "AGI": 12952
    },
    "skills": [
      94
    ],
    "img": "174",
    "rarity": 4,
    "evo": 4,
    "fullName": "Eton, Eater of Darkness II"
  },
  "10319": {
    "name": "Peryton",
    "stats": {
      "HP": 10904,
      "ATK": 9674,
      "DEF": 10490,
      "WIS": 10490,
      "AGI": 12952
    },
    "skills": [
      33
    ],
    "img": "12b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Infested Peryton II"
  },
  "10344": {
    "name": "Hydarnes",
    "stats": {
      "HP": 11928,
      "ATK": 12832,
      "DEF": 10587,
      "WIS": 14182,
      "AGI": 11928
    },
    "skills": [
      114
    ],
    "img": "4fd",
    "rarity": 4,
    "evo": 4,
    "fullName": "Undead General, Hydarnes II"
  },
  "10348": {
    "name": "Pegasus",
    "stats": {
      "HP": 8756,
      "ATK": 10200,
      "DEF": 8843,
      "WIS": 10880,
      "AGI": 9181
    },
    "skills": [
      111
    ],
    "img": "469",
    "rarity": 4,
    "evo": 4,
    "fullName": "Pegasus, the Light Divine II"
  },
  "10365": {
    "name": "Makalipon",
    "stats": {
      "HP": 10343,
      "ATK": 8405,
      "DEF": 10611,
      "WIS": 12280,
      "AGI": 10343
    },
    "skills": [
      60
    ],
    "img": "1f1",
    "rarity": 4,
    "evo": 4,
    "fullName": "Makalipon, Sacred Fruit II"
  },
  "10372": {
    "name": "Artemisia",
    "stats": {
      "HP": 10042,
      "ATK": 10977,
      "DEF": 10977,
      "WIS": 10042,
      "AGI": 12589
    },
    "skills": [
      18
    ],
    "img": "3aa",
    "rarity": 4,
    "evo": 4,
    "fullName": "Artemisia Swiftfoot II"
  },
  "10381": {
    "name": "Nine-tailed",
    "stats": {
      "HP": 10673,
      "ATK": 11807,
      "DEF": 12235,
      "WIS": 11170,
      "AGI": 12500
    },
    "skills": [
      19
    ],
    "img": "306",
    "rarity": 4,
    "evo": 4,
    "fullName": "Nine-tailed Fox II"
  },
  "10409": {
    "name": "Magma Giant",
    "stats": {
      "HP": 12832,
      "ATK": 12380,
      "DEF": 13097,
      "WIS": 11477,
      "AGI": 11928
    },
    "skills": [
      123
    ],
    "img": "363",
    "rarity": 4,
    "evo": 4,
    "fullName": "Chaotic Magma Giant II"
  },
  "10420": {
    "name": "Ziz",
    "stats": {
      "HP": 11741,
      "ATK": 10042,
      "DEF": 11900,
      "WIS": 10624,
      "AGI": 10042
    },
    "skills": [
      19
    ],
    "img": "35e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ziz, Wings Divine II"
  },
  "10423": {
    "name": "Doppeladler",
    "stats": {
      "HP": 13940,
      "ATK": 14709,
      "DEF": 14417,
      "WIS": 14092,
      "AGI": 14850
    },
    "skills": [
      33
    ],
    "img": "168",
    "rarity": 5,
    "evo": 2,
    "fullName": "Doppeladler II"
  },
  "10432": {
    "name": "Lanvall",
    "stats": {
      "HP": 12914,
      "ATK": 14639,
      "DEF": 12245,
      "WIS": 12210,
      "AGI": 15040
    },
    "skills": [
      18
    ],
    "img": "163",
    "rarity": 4,
    "evo": 4,
    "fullName": "Lanvall, Lizard Cavalier II"
  },
  "10438": {
    "name": "Odin Stormgod",
    "stats": {
      "HP": 12855,
      "ATK": 14346,
      "DEF": 12378,
      "WIS": 14929,
      "AGI": 12245
    },
    "skills": [
      119
    ],
    "img": "15c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Odin Stormgod II"
  },
  "10445": {
    "name": "Managarmr",
    "stats": {
      "HP": 12210,
      "ATK": 12258,
      "DEF": 13266,
      "WIS": 13887,
      "AGI": 11688
    },
    "skills": [
      108
    ],
    "img": "151",
    "rarity": 4,
    "evo": 4,
    "fullName": "Managarmr Frost Touch II"
  },
  "10450": {
    "name": "Snow Queen",
    "stats": {
      "HP": 14070,
      "ATK": 13994,
      "DEF": 13940,
      "WIS": 15229,
      "AGI": 14449
    },
    "skills": [
      128
    ],
    "img": "399",
    "rarity": 5,
    "evo": 2,
    "fullName": "Snow Queen II"
  },
  "10452": {
    "name": "Evil Eye",
    "stats": {
      "HP": 10770,
      "ATK": 10394,
      "DEF": 10490,
      "WIS": 12221,
      "AGI": 11721
    },
    "skills": [
      120
    ],
    "img": "2bf",
    "rarity": 4,
    "evo": 4,
    "fullName": "Evil Eye II"
  },
  "10454": {
    "name": "Stormwyrm",
    "stats": {
      "HP": 11025,
      "ATK": 11514,
      "DEF": 9646,
      "WIS": 14489,
      "AGI": 11318
    },
    "skills": [
      47
    ],
    "img": "3ee",
    "rarity": 4,
    "evo": 4,
    "fullName": "Two-Headed Stormwyrm II"
  },
  "10461": {
    "name": "Sulima",
    "stats": {
      "HP": 13417,
      "ATK": 13583,
      "DEF": 12194,
      "WIS": 12293,
      "AGI": 12269
    },
    "skills": [
      17
    ],
    "img": "1ec",
    "rarity": 4,
    "evo": 4,
    "fullName": "Sulima, Executioner II"
  },
  "10464": {
    "name": "Andorra",
    "stats": {
      "HP": 12538,
      "ATK": 13621,
      "DEF": 13510,
      "WIS": 12134,
      "AGI": 12342
    },
    "skills": [
      142
    ],
    "img": "252",
    "rarity": 4,
    "evo": 4,
    "fullName": "Andorra the Indomitable II"
  },
  "10465": {
    "name": "Heinrich",
    "stats": {
      "HP": 16887,
      "ATK": 13940,
      "DEF": 15132,
      "WIS": 13290,
      "AGI": 14005
    },
    "skills": [
      133
    ],
    "img": "305",
    "rarity": 5,
    "evo": 2,
    "fullName": "Heinrich the Bold II"
  },
  "10470": {
    "name": "Flame Dragon",
    "stats": {
      "HP": 14601,
      "ATK": 14449,
      "DEF": 13756,
      "WIS": 15153,
      "AGI": 13940
    },
    "skills": [
      23
    ],
    "img": "18e",
    "rarity": 5,
    "evo": 2,
    "fullName": "Flame Dragon II"
  },
  "10473": {
    "name": "Freila",
    "stats": {
      "HP": 11928,
      "ATK": 10490,
      "DEF": 12453,
      "WIS": 12221,
      "AGI": 11417
    },
    "skills": [
      16
    ],
    "img": "3f2",
    "rarity": 4,
    "evo": 4,
    "fullName": "Freila the Bountiful II"
  },
  "10474": {
    "name": "Zuniga",
    "stats": {
      "HP": 12987,
      "ATK": 15132,
      "DEF": 14276,
      "WIS": 14839,
      "AGI": 14709
    },
    "skills": [
      132
    ],
    "img": "322",
    "rarity": 5,
    "evo": 2,
    "fullName": "Zuniga, Guard Captain II"
  },
  "10480": {
    "name": "Thor",
    "stats": {
      "HP": 10343,
      "ATK": 13245,
      "DEF": 11807,
      "WIS": 13842,
      "AGI": 11917
    },
    "skills": [
      114
    ],
    "img": "3a1",
    "rarity": 4,
    "evo": 4,
    "fullName": "Thor, God of Lightning II"
  },
  "10486": {
    "name": "Yulia",
    "stats": {
      "HP": 14081,
      "ATK": 14664,
      "DEF": 12052,
      "WIS": 13544,
      "AGI": 12524
    },
    "skills": [
      134
    ],
    "img": "341",
    "rarity": 4,
    "evo": 4,
    "fullName": "Yulia, Snakesage II"
  },
  "10488": {
    "name": "Bunga",
    "stats": {
      "HP": 12269,
      "ATK": 11049,
      "DEF": 14182,
      "WIS": 9612,
      "AGI": 10343
    },
    "skills": [
      125
    ],
    "img": "25d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Bunga, the Stalwart II"
  },
  "10496": {
    "name": "Bat Demon",
    "stats": {
      "HP": 12538,
      "ATK": 14182,
      "DEF": 12648,
      "WIS": 11928,
      "AGI": 12720
    },
    "skills": [
      131
    ],
    "img": "10e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Fiendish Bat Demon II"
  },
  "10497": {
    "name": "Zagan",
    "stats": {
      "HP": 16128,
      "ATK": 16941,
      "DEF": 14709,
      "WIS": 12423,
      "AGI": 13052
    },
    "skills": [
      143
    ],
    "img": "192",
    "rarity": 5,
    "evo": 2,
    "fullName": "Zagan II"
  },
  "10503": {
    "name": "Desna",
    "stats": {
      "HP": 13146,
      "ATK": 15089,
      "DEF": 14287,
      "WIS": 12137,
      "AGI": 12378
    },
    "skills": [
      124
    ],
    "img": "245",
    "rarity": 4,
    "evo": 4,
    "fullName": "Desna, Mythic Wendigo II"
  },
  "10505": {
    "name": "Oniroku",
    "stats": {
      "HP": 12207,
      "ATK": 13731,
      "DEF": 12235,
      "WIS": 12194,
      "AGI": 13621
    },
    "skills": [
      115
    ],
    "img": "196",
    "rarity": 4,
    "evo": 4,
    "fullName": "Oniroku the Slayer II"
  },
  "10510": {
    "name": "Kagemaru",
    "stats": {
      "HP": 14319,
      "ATK": 16973,
      "DEF": 13940,
      "WIS": 13420,
      "AGI": 14568
    },
    "skills": [
      137
    ],
    "img": "430",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kagemaru, Master Ninja II"
  },
  "10527": {
    "name": "Melusine",
    "stats": {
      "HP": 11417,
      "ATK": 11976,
      "DEF": 10490,
      "WIS": 13562,
      "AGI": 11210
    },
    "skills": [
      155
    ],
    "img": "272",
    "rarity": 4,
    "evo": 4,
    "fullName": "Melusine the Witch II"
  },
  "10551": {
    "name": "Grandor",
    "stats": {
      "HP": 14709,
      "ATK": 17277,
      "DEF": 15738,
      "WIS": 13756,
      "AGI": 11903
    },
    "skills": [
      149
    ],
    "img": "365",
    "rarity": 5,
    "evo": 2,
    "fullName": "Grandor, Giant of Old II"
  },
  "10558": {
    "name": "Magdal",
    "stats": {
      "HP": 13929,
      "ATK": 15110,
      "DEF": 15132,
      "WIS": 13810,
      "AGI": 15359
    },
    "skills": [
      120
    ],
    "img": "1c0",
    "rarity": 5,
    "evo": 2,
    "fullName": "Magdal Dragonheart II"
  },
  "10560": {
    "name": "Hippogriff",
    "stats": {
      "HP": 9978,
      "ATK": 11063,
      "DEF": 11942,
      "WIS": 9295,
      "AGI": 10074
    },
    "skills": [
      133
    ],
    "img": "43e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hippogriff of Rites II"
  },
  "10566": {
    "name": "Bedwyr",
    "stats": {
      "HP": 12235,
      "ATK": 11318,
      "DEF": 12221,
      "WIS": 13510,
      "AGI": 10598
    },
    "skills": [
      145
    ],
    "img": "321",
    "rarity": 4,
    "evo": 4,
    "fullName": "Sir Bedwyr of the Garden II"
  },
  "10568": {
    "name": "Spellforged Cyclops",
    "stats": {
      "HP": 17047,
      "ATK": 11683,
      "DEF": 14096,
      "WIS": 11111,
      "AGI": 10380
    },
    "skills": [
      61
    ],
    "img": "2c7",
    "rarity": 4,
    "evo": 4,
    "fullName": "Spellforged Cyclops II"
  },
  "10569": {
    "name": "Jinx-eye",
    "stats": {
      "HP": 14709,
      "ATK": 15998,
      "DEF": 13832,
      "WIS": 13832,
      "AGI": 14915
    },
    "skills": [
      146
    ],
    "img": "1c4",
    "rarity": 5,
    "evo": 2,
    "fullName": "Jinx-eye Dragon II"
  },
  "10570": {
    "name": "Wolfert",
    "stats": {
      "HP": 14189,
      "ATK": 23972,
      "DEF": 13723,
      "WIS": 13290,
      "AGI": 13431
    },
    "skills": [
      118
    ],
    "img": "391",
    "rarity": 5,
    "evo": 2,
    "fullName": "Wolfert, Grave Keeper II"
  },
  "10571": {
    "name": "Gathgoic",
    "stats": {
      "HP": 14839,
      "ATK": 16128,
      "DEF": 14980,
      "WIS": 17948,
      "AGI": 14709
    },
    "skills": [
      141
    ],
    "img": "3fb",
    "rarity": 5,
    "evo": 2,
    "fullName": "Gathgoic the Other II"
  },
  "10572": {
    "name": "Vivian",
    "stats": {
      "HP": 14677,
      "ATK": 17851,
      "DEF": 15229,
      "WIS": 13095,
      "AGI": 14677
    },
    "skills": [
      224
    ],
    "img": "25f",
    "rarity": 5,
    "evo": 2,
    "fullName": "Vivian Griffinrider II"
  },
  "10582": {
    "name": "Tepaxtl",
    "stats": {
      "HP": 10831,
      "ATK": 13562,
      "DEF": 9209,
      "WIS": 13110,
      "AGI": 12100
    },
    "skills": [
      115
    ],
    "img": "37d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Tepaxtl, Fatal Fang II"
  },
  "10586": {
    "name": "Gregoire",
    "stats": {
      "HP": 11708,
      "ATK": 12121,
      "DEF": 10318,
      "WIS": 14854,
      "AGI": 10159
    },
    "skills": [
      144
    ],
    "img": "308",
    "rarity": 4,
    "evo": 4,
    "fullName": "Gregoire, Weaponmaster II"
  },
  "10592": {
    "name": "Ira",
    "stats": {
      "HP": 12832,
      "ATK": 14489,
      "DEF": 8770,
      "WIS": 11172,
      "AGI": 17254
    },
    "skills": [
      138
    ],
    "img": "46c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ira, Hypnotic Specter II"
  },
  "10593": {
    "name": "Apocalyptic Beast",
    "stats": {
      "HP": 14189,
      "ATK": 15977,
      "DEF": 15413,
      "WIS": 13420,
      "AGI": 14969
    },
    "skills": [
      123
    ],
    "img": "15a",
    "rarity": 5,
    "evo": 2,
    "fullName": "Apocalyptic Beast II"
  },
  "10595": {
    "name": "Astaroth",
    "stats": {
      "HP": 12194,
      "ATK": 13965,
      "DEF": 10087,
      "WIS": 15278,
      "AGI": 14280
    },
    "skills": [
      155
    ],
    "img": "22e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Astaroth, Duke of Fear II"
  },
  "10599": {
    "name": "Princeps",
    "stats": {
      "HP": 9360,
      "ATK": 10772,
      "DEF": 9674,
      "WIS": 10181,
      "AGI": 11667
    },
    "skills": [
      156
    ],
    "img": "4dc",
    "rarity": 4,
    "evo": 4,
    "fullName": "Princeps, Angel of Doom II"
  },
  "10600": {
    "name": "Ose",
    "stats": {
      "HP": 16995,
      "ATK": 14395,
      "DEF": 15023,
      "WIS": 14850,
      "AGI": 11990
    },
    "skills": [
      154
    ],
    "img": "300",
    "rarity": 5,
    "evo": 2,
    "fullName": "Archduke Ose II"
  },
  "10606": {
    "name": "Fomor",
    "stats": {
      "HP": 13052,
      "ATK": 14645,
      "DEF": 11928,
      "WIS": 9967,
      "AGI": 9781
    },
    "skills": [
      138
    ],
    "img": "143",
    "rarity": 4,
    "evo": 4,
    "fullName": "Fomor the Savage II"
  },
  "10609": {
    "name": "Garuda",
    "stats": {
      "HP": 14417,
      "ATK": 14677,
      "DEF": 14081,
      "WIS": 15814,
      "AGI": 15023
    },
    "skills": [
      47
    ],
    "img": "1bf",
    "rarity": 5,
    "evo": 2,
    "fullName": "Garuda II"
  },
  "10611": {
    "name": "Gorlin",
    "stats": {
      "HP": 11928,
      "ATK": 12380,
      "DEF": 17000,
      "WIS": 6809,
      "AGI": 10904
    },
    "skills": [
      167
    ],
    "img": "150",
    "rarity": 4,
    "evo": 4,
    "fullName": "Gorlin Gold Helm II"
  },
  "10613": {
    "name": "Adara",
    "stats": {
      "HP": 16024,
      "ATK": 12134,
      "DEF": 17620,
      "WIS": 10857,
      "AGI": 9370
    },
    "skills": [
      166
    ],
    "img": "268",
    "rarity": 4,
    "evo": 4,
    "fullName": "Adara Luck Shot II"
  },
  "10614": {
    "name": "Solsten",
    "stats": {
      "HP": 13940,
      "ATK": 14449,
      "DEF": 15998,
      "WIS": 17233,
      "AGI": 12900
    },
    "skills": [
      165
    ],
    "img": "37a",
    "rarity": 5,
    "evo": 2,
    "fullName": "Solsten the Really Wanted II"
  },
  "10619": {
    "name": "Ebon",
    "stats": {
      "HP": 17493,
      "ATK": 15543,
      "DEF": 13431,
      "WIS": 14330,
      "AGI": 13788
    },
    "skills": [
      157
    ],
    "img": "248",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ebon Dragon II"
  },
  "10621": {
    "name": "Montu",
    "stats": {
      "HP": 12952,
      "ATK": 12904,
      "DEF": 12269,
      "WIS": 12269,
      "AGI": 15306
    },
    "skills": [
      170
    ],
    "img": "21d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Montu, God of War II"
  },
  "10623": {
    "name": "Warfist",
    "stats": {
      "HP": 10904,
      "ATK": 11417,
      "DEF": 10466,
      "WIS": 10660,
      "AGI": 11830
    },
    "skills": [
      156
    ],
    "img": "21a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Amazon Warfist II"
  },
  "10625": {
    "name": "Moren",
    "stats": {
      "HP": 8502,
      "ATK": 11318,
      "DEF": 7759,
      "WIS": 16803,
      "AGI": 8039
    },
    "skills": [
      10000,
      71,
      85
    ],
    "isMounted": true,
    "img": "34a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Moren, Rime Mage II"
  },
  "10626": {
    "name": "Marid",
    "stats": {
      "HP": 14070,
      "ATK": 17851,
      "DEF": 14449,
      "WIS": 12597,
      "AGI": 15478
    },
    "skills": [
      169
    ],
    "img": "2ed",
    "rarity": 5,
    "evo": 2,
    "fullName": "Scorching Marid II"
  },
  "10632": {
    "name": "Doog",
    "stats": {
      "HP": 10560,
      "ATK": 10549,
      "DEF": 10777,
      "WIS": 14330,
      "AGI": 11925
    },
    "skills": [
      94
    ],
    "img": "409",
    "rarity": 4,
    "evo": 2,
    "fullName": "Mauthe Doog II"
  },
  "10634": {
    "name": "Hel",
    "stats": {
      "HP": 14709,
      "ATK": 17450,
      "DEF": 14709,
      "WIS": 15771,
      "AGI": 18057
    },
    "skills": [
      239,
      240
    ],
    "img": "1e8",
    "rarity": 5,
    "evo": 2,
    "fullName": "Hel, Goddess of Death II"
  },
  "10635": {
    "name": "Hollofernyiges",
    "stats": {
      "HP": 16551,
      "ATK": 16757,
      "DEF": 13875,
      "WIS": 14568,
      "AGI": 16941
    },
    "skills": [
      33
    ],
    "img": "320",
    "rarity": 5,
    "evo": 2,
    "fullName": "Hollofernyiges II"
  },
  "10647": {
    "name": "Tuniq",
    "stats": {
      "HP": 13635,
      "ATK": 16709,
      "DEF": 12062,
      "WIS": 12086,
      "AGI": 9794
    },
    "skills": [
      150
    ],
    "img": "29c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Tuniq, Guardian Colossus II"
  },
  "10652": {
    "name": "Batraz",
    "stats": {
      "HP": 14471,
      "ATK": 15511,
      "DEF": 13442,
      "WIS": 12293,
      "AGI": 12174
    },
    "skills": [
      142
    ],
    "img": "4e3",
    "rarity": 4,
    "evo": 2,
    "fullName": "Batraz, the Immortal Hero II"
  },
  "10656": {
    "name": "Mathilda",
    "stats": {
      "HP": 11841,
      "ATK": 15172,
      "DEF": 10639,
      "WIS": 12718,
      "AGI": 15218
    },
    "skills": [
      115
    ],
    "img": "368",
    "rarity": 4,
    "evo": 4,
    "fullName": "Mathilda the Tarantula II"
  },
  "10657": {
    "name": "Baal",
    "stats": {
      "HP": 14677,
      "ATK": 15457,
      "DEF": 12813,
      "WIS": 14482,
      "AGI": 16551
    },
    "skills": [
      178
    ],
    "img": "22f",
    "rarity": 5,
    "evo": 2,
    "fullName": "Baal, Thunder Lord of Hell II"
  },
  "10659": {
    "name": "Behemoth",
    "stats": {
      "HP": 12442,
      "ATK": 14755,
      "DEF": 13269,
      "WIS": 12380,
      "AGI": 12999
    },
    "skills": [
      186
    ],
    "img": "230",
    "rarity": 4,
    "evo": 4,
    "fullName": "Behemoth, Thunder Beast II"
  },
  "10664": {
    "name": "Ramiel",
    "stats": {
      "HP": 15543,
      "ATK": 13929,
      "DEF": 13431,
      "WIS": 16388,
      "AGI": 14709
    },
    "skills": [
      185
    ],
    "img": "3da",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ramiel, Angel of the Storm II"
  },
  "10673": {
    "name": "Cernunnos",
    "stats": {
      "HP": 16446,
      "ATK": 15351,
      "DEF": 13761,
      "WIS": 13181,
      "AGI": 14330
    },
    "skills": [
      177
    ],
    "img": "25b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Cernunnos II"
  },
  "10674": {
    "name": "Fenrir",
    "stats": {
      "HP": 15099,
      "ATK": 16865,
      "DEF": 22498,
      "WIS": 13008,
      "AGI": 11167
    },
    "skills": [
      154
    ],
    "img": "1dd",
    "rarity": 5,
    "evo": 2,
    "fullName": "Fenrir II"
  },
  "10675": {
    "name": "Void Yaksha",
    "stats": {
      "HP": 15706,
      "ATK": 18013,
      "DEF": 14471,
      "WIS": 14276,
      "AGI": 15814
    },
    "skills": [
      199
    ],
    "img": "297",
    "rarity": 5,
    "evo": 2,
    "fullName": "Void Yaksha II"
  },
  "10676": {
    "name": "Scirocco",
    "stats": {
      "HP": 15002,
      "ATK": 14503,
      "DEF": 14503,
      "WIS": 18999,
      "AGI": 16497
    },
    "skills": [
      331,
      301
    ],
    "img": "3d5",
    "rarity": 5,
    "evo": 2,
    "fullName": "Scirocco, Father of Winds II"
  },
  "10681": {
    "name": "Iron Golem",
    "stats": {
      "HP": 16778,
      "ATK": 13615,
      "DEF": 17818,
      "WIS": 9867,
      "AGI": 8848
    },
    "skills": [
      152
    ],
    "img": "29f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Iron Golem II"
  },
  "10684": {
    "name": "Biast",
    "stats": {
      "HP": 13879,
      "ATK": 12655,
      "DEF": 10163,
      "WIS": 13611,
      "AGI": 9798
    },
    "skills": [
      163
    ],
    "img": "229",
    "rarity": 4,
    "evo": 2,
    "fullName": "Biast II"
  },
  "10688": {
    "name": "Ignis",
    "stats": {
      "HP": 11022,
      "ATK": 11312,
      "DEF": 10818,
      "WIS": 13460,
      "AGI": 12859
    },
    "skills": [
      164
    ],
    "img": "22f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ignis Fatuus II"
  },
  "10691": {
    "name": "Dors",
    "stats": {
      "HP": 15435,
      "ATK": 9433,
      "DEF": 13268,
      "WIS": 16464,
      "AGI": 13019
    },
    "skills": [
      446
    ],
    "img": "11d",
    "rarity": 4,
    "evo": 2,
    "fullName": "Dors, Demiwyrm Warrior II"
  },
  "10699": {
    "name": "Rampant Lion",
    "stats": {
      "HP": 16291,
      "ATK": 17569,
      "DEF": 16518,
      "WIS": 12564,
      "AGI": 18035
    },
    "skills": [
      380,
      381
    ],
    "img": "387",
    "rarity": 5,
    "evo": 2,
    "fullName": "Rampant Lion II"
  },
  "10704": {
    "name": "Hraesvelg",
    "stats": {
      "HP": 12499,
      "ATK": 17472,
      "DEF": 11784,
      "WIS": 12662,
      "AGI": 13799
    },
    "skills": [
      251
    ],
    "img": "3cd",
    "rarity": 4,
    "evo": 2,
    "fullName": "Hraesvelg, Corpse Feaster II"
  },
  "10705": {
    "name": "Melanippe",
    "stats": {
      "HP": 16139,
      "ATK": 16800,
      "DEF": 13929,
      "WIS": 11849,
      "AGI": 15132
    },
    "skills": [
      195
    ],
    "img": "44f",
    "rarity": 5,
    "evo": 2,
    "fullName": "Melanippe, Wolfrider II"
  },
  "10706": {
    "name": "Ijiraq",
    "stats": {
      "HP": 13929,
      "ATK": 14536,
      "DEF": 9791,
      "WIS": 17797,
      "AGI": 12012
    },
    "skills": [
      168
    ],
    "img": "21b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ijiraq, the Glacier II"
  },
  "10708": {
    "name": "Ettin",
    "stats": {
      "HP": 16063,
      "ATK": 14482,
      "DEF": 14677,
      "WIS": 9498,
      "AGI": 13702
    },
    "skills": [
      304
    ],
    "autoAttack": 10006,
    "img": "31f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ettin II"
  },
  "10710": {
    "name": "Phantom Assassin",
    "stats": {
      "HP": 13507,
      "ATK": 13951,
      "DEF": 11102,
      "WIS": 14341,
      "AGI": 14081
    },
    "skills": [
      193
    ],
    "img": "110",
    "rarity": 4,
    "evo": 2,
    "fullName": "Phantom Assassin II"
  },
  "10712": {
    "name": "Cuelebre",
    "stats": {
      "HP": 13702,
      "ATK": 16096,
      "DEF": 12954,
      "WIS": 11134,
      "AGI": 13572
    },
    "skills": [
      249
    ],
    "img": "28c",
    "rarity": 4,
    "evo": 2,
    "fullName": "Cuelebre the Ironscaled II"
  },
  "10715": {
    "name": "Hrimthurs",
    "stats": {
      "HP": 13414,
      "ATK": 15572,
      "DEF": 16144,
      "WIS": 9783,
      "AGI": 10600
    },
    "skills": [
      205
    ],
    "img": "2e9",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hrimthurs the Blizzard II"
  },
  "10717": {
    "name": "Amon",
    "stats": {
      "HP": 13171,
      "ATK": 16128,
      "DEF": 10755,
      "WIS": 14861,
      "AGI": 13214
    },
    "skills": [
      47
    ],
    "img": "386",
    "rarity": 4,
    "evo": 2,
    "fullName": "Amon, Marquis of Blaze II"
  },
  "10720": {
    "name": "Goviel",
    "stats": {
      "HP": 14135,
      "ATK": 14547,
      "DEF": 13604,
      "WIS": 14926,
      "AGI": 16616
    },
    "skills": [
      204
    ],
    "img": "290",
    "rarity": 5,
    "evo": 2,
    "fullName": "Goviel, Hail Knight II"
  },
  "10722": {
    "name": "Delphyne",
    "stats": {
      "HP": 11990,
      "ATK": 14601,
      "DEF": 11882,
      "WIS": 18858,
      "AGI": 11080
    },
    "skills": [
      288
    ],
    "img": "415",
    "rarity": 4,
    "evo": 2,
    "fullName": "Delphyne, Thunder Dragon II"
  },
  "10726": {
    "name": "Hlokk",
    "stats": {
      "HP": 14328,
      "ATK": 14462,
      "DEF": 12832,
      "WIS": 9271,
      "AGI": 17133
    },
    "skills": [
      502,
      503
    ],
    "img": "37a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hlokk, Blade of Thunder II"
  },
  "10735": {
    "name": "Typhon",
    "stats": {
      "HP": 14677,
      "ATK": 13355,
      "DEF": 14341,
      "WIS": 17959,
      "AGI": 13626
    },
    "skills": [
      117
    ],
    "autoAttack": 10001,
    "img": "283",
    "rarity": 5,
    "evo": 2,
    "fullName": "Typhon II"
  },
  "10742": {
    "name": "Gevi",
    "stats": {
      "HP": 15565,
      "ATK": 15424,
      "DEF": 18447,
      "WIS": 13593,
      "AGI": 11015
    },
    "skills": [
      180
    ],
    "img": "255",
    "rarity": 5,
    "evo": 2,
    "fullName": "Gevi, Crystal Troll Master II"
  },
  "10746": {
    "name": "Iseult",
    "stats": {
      "HP": 12731,
      "ATK": 10977,
      "DEF": 11708,
      "WIS": 15865,
      "AGI": 14193
    },
    "skills": [
      144
    ],
    "img": "13b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Iseult the Redeemer II"
  },
  "10747": {
    "name": "Tristan",
    "stats": {
      "HP": 13832,
      "ATK": 16193,
      "DEF": 15197,
      "WIS": 13052,
      "AGI": 15771
    },
    "skills": [
      122
    ],
    "img": "3c3",
    "rarity": 5,
    "evo": 2,
    "fullName": "Tristan the Sorrowful II"
  },
  "10754": {
    "name": "Lucia",
    "stats": {
      "HP": 17106,
      "ATK": 13878,
      "DEF": 16633,
      "WIS": 9881,
      "AGI": 10857
    },
    "skills": [
      16
    ],
    "img": "197",
    "rarity": 4,
    "evo": 4,
    "fullName": "Lucia, Petal-Shears II"
  },
  "10756": {
    "name": "Edgardo",
    "stats": {
      "HP": 10904,
      "ATK": 15485,
      "DEF": 14389,
      "WIS": 8978,
      "AGI": 14755
    },
    "skills": [
      179
    ],
    "img": "25f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Edgardo, Grand Inquisitor II"
  },
  "10757": {
    "name": "Amphisbaena",
    "stats": {
      "HP": 14861,
      "ATK": 14850,
      "DEF": 13030,
      "WIS": 19855,
      "AGI": 18024
    },
    "skills": [
      202,
      203
    ],
    "isMounted": true,
    "img": "346",
    "rarity": 5,
    "evo": 2,
    "fullName": "Amphisbaena II"
  },
  "10767": {
    "name": "Kelaino",
    "stats": {
      "HP": 12538,
      "ATK": 12707,
      "DEF": 10490,
      "WIS": 15047,
      "AGI": 14999
    },
    "skills": [
      197
    ],
    "img": "405",
    "rarity": 4,
    "evo": 4,
    "fullName": "Kelaino, the Dark Cloud II"
  },
  "10784": {
    "name": "Gretch",
    "stats": {
      "HP": 16280,
      "ATK": 15305,
      "DEF": 12683,
      "WIS": 15652,
      "AGI": 13875
    },
    "skills": [
      196
    ],
    "img": "3a9",
    "rarity": 5,
    "evo": 2,
    "fullName": "Gretch, Chimaera Mistress II"
  },
  "10785": {
    "name": "Premyslid",
    "stats": {
      "HP": 13626,
      "ATK": 16984,
      "DEF": 14926,
      "WIS": 18772,
      "AGI": 11232
    },
    "skills": [
      244
    ],
    "img": "2c7",
    "rarity": 5,
    "evo": 2,
    "fullName": "Premyslid, the Black King II"
  },
  "10787": {
    "name": "Black Knight",
    "stats": {
      "HP": 12648,
      "ATK": 16097,
      "DEF": 11623,
      "WIS": 11574,
      "AGI": 13842
    },
    "skills": [
      211
    ],
    "img": "19e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Black Knight, Soul Hunter II"
  },
  "10789": {
    "name": "Katiria",
    "stats": {
      "HP": 10807,
      "ATK": 11318,
      "DEF": 11356,
      "WIS": 10245,
      "AGI": 11623
    },
    "skills": [
      156
    ],
    "img": "2b6",
    "rarity": 4,
    "evo": 4,
    "fullName": "Katiria Nullblade II"
  },
  "10791": {
    "name": "Grellas",
    "stats": {
      "HP": 12066,
      "ATK": 14796,
      "DEF": 10636,
      "WIS": 17374,
      "AGI": 13073
    },
    "skills": [
      212
    ],
    "img": "211",
    "rarity": 4,
    "evo": 2,
    "fullName": "Grellas Fellstaff II"
  },
  "10792": {
    "name": "Marchosias",
    "stats": {
      "HP": 18165,
      "ATK": 15424,
      "DEF": 12781,
      "WIS": 18566,
      "AGI": 13561
    },
    "skills": [
      210
    ],
    "img": "271",
    "rarity": 5,
    "evo": 2,
    "fullName": "Marchosias, Pit Beast II"
  },
  "10794": {
    "name": "Ma-Gu",
    "stats": {
      "HP": 14182,
      "ATK": 12438,
      "DEF": 11477,
      "WIS": 15306,
      "AGI": 12438
    },
    "skills": [
      4
    ],
    "img": "2a8",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ma-Gu the Enlightened II"
  },
  "10798": {
    "name": "Wu Chang",
    "stats": {
      "HP": 10294,
      "ATK": 14182,
      "DEF": 10977,
      "WIS": 10600,
      "AGI": 11928
    },
    "skills": [
      115
    ],
    "img": "365",
    "rarity": 4,
    "evo": 4,
    "fullName": "Wu Chang the Infernal II"
  },
  "10799": {
    "name": "Niu Mo Wang",
    "stats": {
      "HP": 14276,
      "ATK": 17071,
      "DEF": 15998,
      "WIS": 13420,
      "AGI": 13138
    },
    "skills": [
      133
    ],
    "img": "126",
    "rarity": 5,
    "evo": 2,
    "fullName": "Niu Mo Wang II"
  },
  "10804": {
    "name": "Kangana",
    "stats": {
      "HP": 15803,
      "ATK": 18750,
      "DEF": 14872,
      "WIS": 12813,
      "AGI": 13247
    },
    "skills": [
      216
    ],
    "img": "2b1",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kangana, the Maelstrom II"
  },
  "10806": {
    "name": "Rapse",
    "stats": {
      "HP": 11928,
      "ATK": 14182,
      "DEF": 13110,
      "WIS": 11270,
      "AGI": 15524
    },
    "skills": [
      179
    ],
    "img": "4e0",
    "rarity": 4,
    "evo": 4,
    "fullName": "Rapse, the Bloody Horns II"
  },
  "10807": {
    "name": "Vezat",
    "stats": {
      "HP": 16648,
      "ATK": 18165,
      "DEF": 14709,
      "WIS": 13431,
      "AGI": 17721
    },
    "skills": [
      214
    ],
    "img": "429",
    "rarity": 5,
    "evo": 2,
    "fullName": "Vezat, Dragonbone Warrior II"
  },
  "10813": {
    "name": "ASK",
    "stats": {
      "HP": 12952,
      "ATK": 14282,
      "DEF": 11477,
      "WIS": 10490,
      "AGI": 17133
    },
    "skills": [
      219
    ],
    "img": "339",
    "rarity": 4,
    "evo": 4,
    "fullName": "All-Seeing Keeper II"
  },
  "10820": {
    "name": "Cyclops",
    "stats": {
      "HP": 15868,
      "ATK": 17147,
      "DEF": 18360,
      "WIS": 13214,
      "AGI": 14449
    },
    "skills": [
      218
    ],
    "img": "3ba",
    "rarity": 5,
    "evo": 2,
    "fullName": "Cyclops, the Rocky Cliff II"
  },
  "10824": {
    "name": "Bolus",
    "stats": {
      "HP": 12086,
      "ATK": 16889,
      "DEF": 12427,
      "WIS": 11610,
      "AGI": 12832
    },
    "skills": [
      152
    ],
    "img": "4a0",
    "rarity": 4,
    "evo": 4,
    "fullName": "Bolus, the Blue Bolt II"
  },
  "10831": {
    "name": "Pegasus Knight",
    "stats": {
      "HP": 15251,
      "ATK": 19032,
      "DEF": 15370,
      "WIS": 13073,
      "AGI": 18046
    },
    "skills": [
      311,
      312
    ],
    "isMounted": true,
    "img": "3e4",
    "rarity": 5,
    "evo": 2,
    "fullName": "Pegasus Knight II"
  },
  "10841": {
    "name": "Alcina",
    "stats": {
      "HP": 12684,
      "ATK": 14169,
      "DEF": 11356,
      "WIS": 13682,
      "AGI": 15755
    },
    "skills": [
      269
    ],
    "img": "31b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Alcina the Soulsucker II"
  },
  "10842": {
    "name": "Gargoyle Gatekeeper",
    "stats": {
      "HP": 15608,
      "ATK": 17602,
      "DEF": 14503,
      "WIS": 15002,
      "AGI": 18035
    },
    "skills": [
      268
    ],
    "img": "277",
    "rarity": 5,
    "evo": 2,
    "fullName": "Gargoyle Gatekeeper II"
  },
  "10844": {
    "name": "Regin",
    "stats": {
      "HP": 12734,
      "ATK": 13342,
      "DEF": 12832,
      "WIS": 16144,
      "AGI": 11270
    },
    "skills": [
      155
    ],
    "img": "2b6",
    "rarity": 4,
    "evo": 4,
    "fullName": "Regin, the Brass Mantis II"
  },
  "10845": {
    "name": "Rovn",
    "stats": {
      "HP": 16269,
      "ATK": 19086,
      "DEF": 18772,
      "WIS": 13214,
      "AGI": 13355
    },
    "skills": [
      228
    ],
    "img": "2a4",
    "rarity": 5,
    "evo": 2,
    "fullName": "Rovn, the Brass Panzer II"
  },
  "10849": {
    "name": "Fimbul",
    "stats": {
      "HP": 12086,
      "ATK": 13489,
      "DEF": 12562,
      "WIS": 16743,
      "AGI": 12597
    },
    "skills": [
      242
    ],
    "img": "24a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Fimbul Frostclad II"
  },
  "10850": {
    "name": "Stalo",
    "stats": {
      "HP": 16269,
      "ATK": 16280,
      "DEF": 16681,
      "WIS": 12792,
      "AGI": 13496
    },
    "skills": [
      241
    ],
    "img": "296",
    "rarity": 5,
    "evo": 2,
    "fullName": "Stalo, Glacial Giant II"
  },
  "10852": {
    "name": "Libuse",
    "stats": {
      "HP": 11221,
      "ATK": 13782,
      "DEF": 13379,
      "WIS": 16048,
      "AGI": 13038
    },
    "skills": [
      245
    ],
    "img": "27e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Libuse, the Black Queen II"
  },
  "10859": {
    "name": "Thunderbird",
    "stats": {
      "HP": 15912,
      "ATK": 16995,
      "DEF": 13572,
      "WIS": 15771,
      "AGI": 17006
    },
    "skills": [
      231
    ],
    "img": "2be",
    "rarity": 5,
    "evo": 2,
    "fullName": "Thunderbird II"
  },
  "10861": {
    "name": "Haokah",
    "stats": {
      "HP": 13476,
      "ATK": 13928,
      "DEF": 11111,
      "WIS": 15706,
      "AGI": 13245
    },
    "skills": [
      232
    ],
    "img": "198",
    "rarity": 4,
    "evo": 4,
    "fullName": "Haokah, the Lightning Brave II"
  },
  "10863": {
    "name": "Rasiel",
    "stats": {
      "HP": 11936,
      "ATK": 15587,
      "DEF": 11817,
      "WIS": 17797,
      "AGI": 11004
    },
    "skills": [
      234
    ],
    "img": "213",
    "rarity": 4,
    "evo": 2,
    "fullName": "Rasiel, Angel All-Knowing II"
  },
  "10869": {
    "name": "Zanga",
    "stats": {
      "HP": 10218,
      "ATK": 10787,
      "DEF": 9694,
      "WIS": 9512,
      "AGI": 12780
    },
    "skills": [
      161
    ],
    "img": "1cf",
    "rarity": 4,
    "evo": 4,
    "fullName": "Zanga, the Iron Storm II"
  },
  "10876": {
    "name": "Pontifex",
    "stats": {
      "HP": 14590,
      "ATK": 16410,
      "DEF": 13507,
      "WIS": 18371,
      "AGI": 17797
    },
    "skills": [
      229,
      167
    ],
    "img": "2bd",
    "rarity": 5,
    "evo": 2,
    "fullName": "Pontifex Antiquus II"
  },
  "10888": {
    "name": "Flesh Collector Golem",
    "stats": {
      "HP": 17450,
      "ATK": 14536,
      "DEF": 18089,
      "WIS": 8664,
      "AGI": 9661
    },
    "skills": [
      253
    ],
    "img": "252",
    "rarity": 4,
    "evo": 2,
    "fullName": "Flesh Collector Golem II"
  },
  "10889": {
    "name": "Olitiau",
    "stats": {
      "HP": 14081,
      "ATK": 15760,
      "DEF": 11676,
      "WIS": 11232,
      "AGI": 15197
    },
    "skills": [
      221
    ],
    "img": "133",
    "rarity": 4,
    "evo": 2,
    "fullName": "Olitiau, the Great Bat II"
  },
  "10895": {
    "name": "Hercinia",
    "stats": {
      "HP": 14062,
      "ATK": 13414,
      "DEF": 12562,
      "WIS": 12686,
      "AGI": 15876
    },
    "skills": [
      225
    ],
    "img": "1a4",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hercinia the Blest II"
  },
  "10896": {
    "name": "Valin",
    "stats": {
      "HP": 15500,
      "ATK": 16865,
      "DEF": 22953,
      "WIS": 12716,
      "AGI": 11167
    },
    "skills": [
      263
    ],
    "img": "34a",
    "rarity": 5,
    "evo": 2,
    "fullName": "Valin the Terrible II"
  },
  "10898": {
    "name": "Hamad",
    "stats": {
      "HP": 10294,
      "ATK": 10367,
      "DEF": 9881,
      "WIS": 16416,
      "AGI": 10951
    },
    "skills": [
      265
    ],
    "img": "3fd",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hamad, the Sweeping Wind II"
  },
  "10900": {
    "name": "Aurboda",
    "stats": {
      "HP": 11903,
      "ATK": 15348,
      "DEF": 11773,
      "WIS": 18468,
      "AGI": 11015
    },
    "skills": [
      261
    ],
    "img": "315",
    "rarity": 4,
    "evo": 2,
    "fullName": "Aurboda, the Great Mother II"
  },
  "10905": {
    "name": "Danzo",
    "stats": {
      "HP": 14774,
      "ATK": 17277,
      "DEF": 14872,
      "WIS": 17667,
      "AGI": 16128
    },
    "skills": [
      237
    ],
    "img": "464",
    "rarity": 5,
    "evo": 2,
    "fullName": "Danzo, Falcon Ninja II"
  },
  "10907": {
    "name": "Chiyome",
    "stats": {
      "HP": 12635,
      "ATK": 14148,
      "DEF": 11369,
      "WIS": 15817,
      "AGI": 13510
    },
    "skills": [
      238
    ],
    "img": "183",
    "rarity": 4,
    "evo": 4,
    "fullName": "Chiyome, the Kamaitachi II"
  },
  "10911": {
    "name": "Kyteler",
    "stats": {
      "HP": 11721,
      "ATK": 12524,
      "DEF": 9892,
      "WIS": 17254,
      "AGI": 16416
    },
    "skills": [
      258
    ],
    "img": "4d4",
    "rarity": 4,
    "evo": 4,
    "fullName": "Kyteler the Corrupted II"
  },
  "10914": {
    "name": "Dharva",
    "stats": {
      "HP": 14096,
      "ATK": 13742,
      "DEF": 12280,
      "WIS": 11942,
      "AGI": 15427
    },
    "skills": [
      254
    ],
    "img": "297",
    "rarity": 4,
    "evo": 4,
    "fullName": "Dharva Fangclad II"
  },
  "10920": {
    "name": "Unicorn",
    "stats": {
      "HP": 10807,
      "ATK": 12600,
      "DEF": 8770,
      "WIS": 11721,
      "AGI": 12001
    },
    "skills": [
      156
    ],
    "img": "204",
    "rarity": 4,
    "evo": 4,
    "fullName": "Unicorn, Spirit Eater II"
  },
  "10921": {
    "name": "Brandiles",
    "stats": {
      "HP": 17017,
      "ATK": 18100,
      "DEF": 16269,
      "WIS": 13940,
      "AGI": 14070
    },
    "skills": [
      252
    ],
    "img": "106",
    "rarity": 5,
    "evo": 2,
    "fullName": "Sir Brandiles, the Flameblade II"
  },
  "10925": {
    "name": "Grimoire",
    "stats": {
      "HP": 15231,
      "ATK": 18609,
      "DEF": 10441,
      "WIS": 8064,
      "AGI": 15451
    },
    "skills": [
      134
    ],
    "img": "49b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Grimoire Beast II"
  },
  "10933": {
    "name": "Linnorm",
    "stats": {
      "HP": 12326,
      "ATK": 11102,
      "DEF": 11979,
      "WIS": 16605,
      "AGI": 16497
    },
    "skills": [
      313
    ],
    "img": "30b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Linnorm, the Hailstorm II"
  },
  "10935": {
    "name": "Belisama",
    "stats": {
      "HP": 17777,
      "ATK": 17071,
      "DEF": 17000,
      "WIS": 11111,
      "AGI": 4981
    },
    "skills": [
      628
    ],
    "img": "39e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Belisama, Flame Goddess II"
  },
  "10936": {
    "name": "Merrow",
    "stats": {
      "HP": 16811,
      "ATK": 14709,
      "DEF": 13723,
      "WIS": 17537,
      "AGI": 17320
    },
    "skills": [
      217
    ],
    "img": "26d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Alluring Merrow II"
  },
  "10941": {
    "name": "Soura",
    "stats": {
      "HP": 12012,
      "ATK": 12261,
      "DEF": 7917,
      "WIS": 16930,
      "AGI": 17667
    },
    "skills": [
      287,
      291
    ],
    "img": "4f1",
    "rarity": 4,
    "evo": 2,
    "fullName": "Soura, Inferno Shaman II"
  },
  "10947": {
    "name": "Ankou",
    "stats": {
      "HP": 17017,
      "ATK": 9628,
      "DEF": 16854,
      "WIS": 14308,
      "AGI": 10246
    },
    "skills": [
      345,
      346
    ],
    "autoAttack": 10007,
    "isMounted": true,
    "img": "4d6",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ankou, Harbinger of Death II"
  },
  "10949": {
    "name": "Najeeba",
    "stats": {
      "HP": 16230,
      "ATK": 7539,
      "DEF": 10660,
      "WIS": 16681,
      "AGI": 16803
    },
    "skills": [
      642
    ],
    "autoAttack": 10003,
    "img": "48a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Najeeba, the Mapleblade II"
  },
  "10951": {
    "name": "Hecatoncheir",
    "stats": {
      "HP": 11807,
      "ATK": 13902,
      "DEF": 14768,
      "WIS": 13928,
      "AGI": 13366
    },
    "skills": [
      264
    ],
    "img": "488",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hecatoncheir the Adamantine II"
  },
  "10955": {
    "name": "Sugaar",
    "stats": {
      "HP": 13110,
      "ATK": 7481,
      "DEF": 14293,
      "WIS": 16950,
      "AGI": 16097
    },
    "skills": [
      465
    ],
    "autoAttack": 10007,
    "img": "19b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Sugaar, the Thunderstorm II"
  },
  "10966": {
    "name": "Saurva",
    "stats": {
      "HP": 14958,
      "ATK": 15305,
      "DEF": 11329,
      "WIS": 11362,
      "AGI": 15002
    },
    "skills": [
      259
    ],
    "img": "1f3",
    "rarity": 4,
    "evo": 2,
    "fullName": "Saurva, the Lawless Lord II"
  },
  "10967": {
    "name": "Deborah",
    "stats": {
      "HP": 13550,
      "ATK": 14157,
      "DEF": 13442,
      "WIS": 12987,
      "AGI": 13929
    },
    "skills": [
      222
    ],
    "img": "373",
    "rarity": 4,
    "evo": 2,
    "fullName": "Deborah, Knight Immaculate II"
  },
  "10970": {
    "name": "Hypnos",
    "stats": {
      "HP": 16291,
      "ATK": 17277,
      "DEF": 15446,
      "WIS": 12488,
      "AGI": 17992
    },
    "skills": [
      274
    ],
    "img": "43b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Hypnos, Lord of Dreams II"
  },
  "10972": {
    "name": "Alp",
    "stats": {
      "HP": 11917,
      "ATK": 14120,
      "DEF": 10928,
      "WIS": 17168,
      "AGI": 13366
    },
    "skills": [
      277
    ],
    "img": "20d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Alp, Dynast of Darkness II"
  },
  "10973": {
    "name": "Dagr",
    "stats": {
      "HP": 12012,
      "ATK": 14059,
      "DEF": 10712,
      "WIS": 17818,
      "AGI": 13810
    },
    "skills": [
      275
    ],
    "img": "4d2",
    "rarity": 4,
    "evo": 2,
    "fullName": "Dagr Sunrider II"
  },
  "10977": {
    "name": "Boudica",
    "stats": {
      "HP": 9967,
      "ATK": 11914,
      "DEF": 8918,
      "WIS": 13110,
      "AGI": 12014
    },
    "skills": [
      276
    ],
    "img": "2ab",
    "rarity": 4,
    "evo": 4,
    "fullName": "Boudica, the Dawn Chief II"
  },
  "10980": {
    "name": "Hundred-eyed Warrior",
    "stats": {
      "HP": 17385,
      "ATK": 18501,
      "DEF": 15641,
      "WIS": 10452,
      "AGI": 17634
    },
    "skills": [
      289
    ],
    "img": "221",
    "rarity": 5,
    "evo": 2,
    "fullName": "Hundred-eyed Warrior II"
  },
  "10983": {
    "name": "Danniel",
    "stats": {
      "HP": 23571,
      "ATK": 24990,
      "DEF": 21458,
      "WIS": 13951,
      "AGI": 16204
    },
    "skills": [
      292
    ],
    "img": "1e2",
    "rarity": 6,
    "evo": 2,
    "fullName": "Danniel, Golden Paladin II"
  },
  "10985": {
    "name": "Lahamu",
    "stats": {
      "HP": 14024,
      "ATK": 10784,
      "DEF": 15999,
      "WIS": 16010,
      "AGI": 11001
    },
    "skills": [
      281
    ],
    "autoAttack": 10004,
    "img": "2fe",
    "rarity": 4,
    "evo": 4,
    "fullName": "Lahamu, Royal Viper II"
  },
  "10987": {
    "name": "Sihn",
    "stats": {
      "HP": 12001,
      "ATK": 10495,
      "DEF": 12001,
      "WIS": 17504,
      "AGI": 16497
    },
    "skills": [
      285
    ],
    "img": "453",
    "rarity": 4,
    "evo": 2,
    "fullName": "Sihn, Moonlight King II"
  },
  "10989": {
    "name": "Nehasim",
    "stats": {
      "HP": 12707,
      "ATK": 16071,
      "DEF": 11390,
      "WIS": 12466,
      "AGI": 15172
    },
    "skills": [
      294
    ],
    "img": "28b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Nehasim the Seething II"
  },
  "10992": {
    "name": "Zeruel",
    "stats": {
      "HP": 16995,
      "ATK": 19573,
      "DEF": 13886,
      "WIS": 13507,
      "AGI": 16984
    },
    "skills": [
      351,
      352
    ],
    "img": "4a7",
    "rarity": 5,
    "evo": 2,
    "fullName": "Zeruel, Angel of War II"
  },
  "10994": {
    "name": "Nergal",
    "stats": {
      "HP": 13008,
      "ATK": 15392,
      "DEF": 11947,
      "WIS": 11643,
      "AGI": 16518
    },
    "skills": [
      282
    ],
    "img": "175",
    "rarity": 4,
    "evo": 2,
    "fullName": "Nergal, Abyssal Overseer II"
  },
  "10995": {
    "name": "Ymir",
    "stats": {
      "HP": 22650,
      "ATK": 24600,
      "DEF": 16464,
      "WIS": 20592,
      "AGI": 15933
    },
    "skills": [
      227
    ],
    "img": "167",
    "rarity": 6,
    "evo": 2,
    "fullName": "Ymir, Primordial Giant II"
  },
  "10997": {
    "name": "Jolly",
    "stats": {
      "HP": 14200,
      "ATK": 16594,
      "DEF": 14070,
      "WIS": 18956,
      "AGI": 15424
    },
    "skills": [
      226
    ],
    "img": "214",
    "rarity": 5,
    "evo": 2,
    "fullName": "Cap'n Jolly, Sea Scourge II"
  },
  "10999": {
    "name": "Anne",
    "stats": {
      "HP": 12232,
      "ATK": 13782,
      "DEF": 12342,
      "WIS": 13510,
      "AGI": 15599
    },
    "skills": [
      250
    ],
    "img": "13d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Anne, the Whirlwind II"
  },
  "11000": {
    "name": "Tanba",
    "stats": {
      "HP": 17580,
      "ATK": 23213,
      "DEF": 17883,
      "WIS": 23289,
      "AGI": 18057
    },
    "skills": [
      236
    ],
    "img": "3a8",
    "rarity": 6,
    "evo": 2,
    "fullName": "Tanba, Founder of the Ninja II"
  },
  "11006": {
    "name": "Siby",
    "stats": {
      "HP": 15558,
      "ATK": 8005,
      "DEF": 11442,
      "WIS": 17120,
      "AGI": 15804
    },
    "skills": [
      550
    ],
    "autoAttack": 10018,
    "img": "20c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Siby, Sea Seer II"
  },
  "11008": {
    "name": "Karkadann",
    "stats": {
      "HP": 17034,
      "ATK": 16475,
      "DEF": 13510,
      "WIS": 7822,
      "AGI": 13097
    },
    "skills": [
      521
    ],
    "img": "422",
    "rarity": 4,
    "evo": 4,
    "fullName": "Venomhorn Karkadann II"
  },
  "11009": {
    "name": "Jabberwock",
    "stats": {
      "HP": 13994,
      "ATK": 16193,
      "DEF": 13008,
      "WIS": 19508,
      "AGI": 18024
    },
    "skills": [
      271,
      270
    ],
    "img": "41f",
    "rarity": 5,
    "evo": 2,
    "fullName": "Jabberwock, Phantom Dragon II"
  },
  "11013": {
    "name": "Marraco",
    "stats": {
      "HP": 18716,
      "ATK": 15876,
      "DEF": 17254,
      "WIS": 7381,
      "AGI": 8809
    },
    "skills": [
      167,
      61
    ],
    "img": "47b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Marraco, Crusted Wyrm II"
  },
  "11015": {
    "name": "Narmer",
    "stats": {
      "HP": 15876,
      "ATK": 12194,
      "DEF": 15172,
      "WIS": 8870,
      "AGI": 15924
    },
    "skills": [
      260
    ],
    "img": "12d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Narmer, Mummy King II"
  },
  "11017": {
    "name": "Svadilfari",
    "stats": {
      "HP": 15977,
      "ATK": 19595,
      "DEF": 13442,
      "WIS": 15998,
      "AGI": 14503
    },
    "skills": [
      369,
      370
    ],
    "img": "1ce",
    "rarity": 5,
    "evo": 2,
    "fullName": "Svadilfari II"
  },
  "11018": {
    "name": "Warden",
    "stats": {
      "HP": 19400,
      "ATK": 17504,
      "DEF": 18273,
      "WIS": 11026,
      "AGI": 11795
    },
    "skills": [
      532
    ],
    "img": "33d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Wyrm Warden, Everwakeful II"
  },
  "11019": {
    "name": "Cursebone",
    "stats": {
      "HP": 14807,
      "ATK": 16952,
      "DEF": 14146,
      "WIS": 15652,
      "AGI": 17721
    },
    "skills": [
      248
    ],
    "img": "33e",
    "rarity": 5,
    "evo": 2,
    "fullName": "Cursebone Pterosaur II"
  },
  "11020": {
    "name": "Phantasmal Succubus",
    "stats": {
      "HP": 18013,
      "ATK": 13604,
      "DEF": 20007,
      "WIS": 17190,
      "AGI": 10701
    },
    "skills": [
      272,
      273
    ],
    "img": "1fb",
    "rarity": 5,
    "evo": 2,
    "fullName": "Phantasmal Succubus II"
  },
  "11021": {
    "name": "Vlad",
    "stats": {
      "HP": 16323,
      "ATK": 19508,
      "DEF": 13680,
      "WIS": 14709,
      "AGI": 16529
    },
    "skills": [
      296,
      295
    ],
    "img": "356",
    "rarity": 5,
    "evo": 2,
    "fullName": "Vlad the Impaler II"
  },
  "11022": {
    "name": "Phantom Knight",
    "stats": {
      "HP": 19877,
      "ATK": 23213,
      "DEF": 19270,
      "WIS": 19682,
      "AGI": 18057
    },
    "skills": [
      267
    ],
    "img": "461",
    "rarity": 6,
    "evo": 2,
    "fullName": "Phantom Knight, the Vagabond II"
  },
  "11025": {
    "name": "Scarecrow",
    "stats": {
      "HP": 10625,
      "ATK": 13756,
      "DEF": 10490,
      "WIS": 11001,
      "AGI": 9342
    },
    "skills": [
      256
    ],
    "img": "34d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Beheading Scarecrow II"
  },
  "11036": {
    "name": "Sea Serpent",
    "stats": {
      "HP": 16020,
      "ATK": 12012,
      "DEF": 15121,
      "WIS": 19259,
      "AGI": 17103
    },
    "skills": [
      302
    ],
    "img": "165",
    "rarity": 5,
    "evo": 2,
    "fullName": "Sea Serpent II"
  },
  "11038": {
    "name": "Skrimsl",
    "stats": {
      "HP": 13049,
      "ATK": 11417,
      "DEF": 12466,
      "WIS": 17182,
      "AGI": 13379
    },
    "skills": [
      303
    ],
    "img": "278",
    "rarity": 4,
    "evo": 4,
    "fullName": "Skrimsl the Freezing II"
  },
  "11039": {
    "name": "Phoenix",
    "stats": {
      "HP": 14005,
      "ATK": 11188,
      "DEF": 12033,
      "WIS": 19010,
      "AGI": 12185
    },
    "skills": [
      305
    ],
    "img": "125",
    "rarity": 4,
    "evo": 2,
    "fullName": "Phoenix, the Metempsychosis II"
  },
  "11041": {
    "name": "Ahab",
    "stats": {
      "HP": 10273,
      "ATK": 12001,
      "DEF": 11342,
      "WIS": 9978,
      "AGI": 12342
    },
    "skills": [
      195
    ],
    "img": "2ec",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ahab, the Colossal Anchor II"
  },
  "11046": {
    "name": "Waheela",
    "stats": {
      "HP": 17006,
      "ATK": 13008,
      "DEF": 16204,
      "WIS": 16692,
      "AGI": 18100
    },
    "skills": [
      19,
      134
    ],
    "img": "2dc",
    "rarity": 5,
    "evo": 2,
    "fullName": "Waheela, Dire Wolf II"
  },
  "11048": {
    "name": "Ragnar",
    "stats": {
      "HP": 13245,
      "ATK": 15804,
      "DEF": 12001,
      "WIS": 10294,
      "AGI": 16510
    },
    "skills": [
      314
    ],
    "img": "497",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ragnar, Dragonslayer II"
  },
  "11056": {
    "name": "Selk",
    "stats": {
      "HP": 13902,
      "ATK": 15854,
      "DEF": 11976,
      "WIS": 11208,
      "AGI": 14927
    },
    "skills": [
      327
    ],
    "img": "403",
    "rarity": 4,
    "evo": 4,
    "fullName": "Selk, Desert King II"
  },
  "11057": {
    "name": "Neith",
    "stats": {
      "HP": 18999,
      "ATK": 19660,
      "DEF": 15002,
      "WIS": 12001,
      "AGI": 15305
    },
    "skills": [
      326
    ],
    "img": "23b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Neith, Goddess of War II"
  },
  "11058": {
    "name": "Ammit",
    "stats": {
      "HP": 18306,
      "ATK": 23495,
      "DEF": 18501,
      "WIS": 18490,
      "AGI": 18057
    },
    "skills": [
      325
    ],
    "img": "2f9",
    "rarity": 6,
    "evo": 2,
    "fullName": "Ammit, Soul Destroyer II"
  },
  "11062": {
    "name": "Chillweaver",
    "stats": {
      "HP": 13293,
      "ATK": 13196,
      "DEF": 10611,
      "WIS": 16144,
      "AGI": 14489
    },
    "skills": [
      2
    ],
    "img": "2b2",
    "rarity": 4,
    "evo": 4,
    "fullName": "Cat Sith Chillweaver II"
  },
  "11063": {
    "name": "Treant",
    "stats": {
      "HP": 18566,
      "ATK": 17017,
      "DEF": 22542,
      "WIS": 13626,
      "AGI": 8014
    },
    "skills": [
      154
    ],
    "img": "167",
    "rarity": 5,
    "evo": 2,
    "fullName": "Sagacious Treant II"
  },
  "11064": {
    "name": "Ijiraq L",
    "stats": {
      "HP": 16995,
      "ATK": 14449,
      "DEF": 17006,
      "WIS": 19508,
      "AGI": 12987
    },
    "skills": [
      328,
      329
    ],
    "img": "33c",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ijiraq the Brinicle II"
  },
  "11065": {
    "name": "ABS",
    "stats": {
      "HP": 14005,
      "ATK": 15901,
      "DEF": 11903,
      "WIS": 11838,
      "AGI": 14904
    },
    "skills": [
      365
    ],
    "img": "1e0",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ancient Beetle Soldier II"
  },
  "11066": {
    "name": "Ruprecht",
    "stats": {
      "HP": 12911,
      "ATK": 15316,
      "DEF": 11795,
      "WIS": 17504,
      "AGI": 11199
    },
    "skills": [
      330,
      334
    ],
    "img": "479",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ruprecht the Punisher II"
  },
  "11073": {
    "name": "Hippocamp",
    "stats": {
      "HP": 14514,
      "ATK": 16486,
      "DEF": 14926,
      "WIS": 19855,
      "AGI": 15002
    },
    "skills": [
      360,
      167
    ],
    "img": "4f8",
    "rarity": 5,
    "evo": 2,
    "fullName": "Hippocamp II"
  },
  "11074": {
    "name": "Skoll",
    "stats": {
      "HP": 15002,
      "ATK": 13160,
      "DEF": 15153,
      "WIS": 9000,
      "AGI": 16302
    },
    "skills": [
      367,
      301
    ],
    "img": "3e8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Skoll, Dark Wolf II"
  },
  "11077": {
    "name": "Zahhak",
    "stats": {
      "HP": 16789,
      "ATK": 10051,
      "DEF": 19151,
      "WIS": 17797,
      "AGI": 17168
    },
    "skills": [
      339
    ],
    "autoAttack": 10001,
    "img": "194",
    "rarity": 5,
    "evo": 2,
    "fullName": "Zahhak, Dragon Marshal II"
  },
  "11079": {
    "name": "Nightblade",
    "stats": {
      "HP": 12196,
      "ATK": 16995,
      "DEF": 13528,
      "WIS": 10896,
      "AGI": 14915
    },
    "skills": [
      341
    ],
    "img": "164",
    "rarity": 4,
    "evo": 2,
    "fullName": "Nightblade, Archsage of Winds II"
  },
  "11081": {
    "name": "Moni",
    "stats": {
      "HP": 13562,
      "ATK": 15537,
      "DEF": 12121,
      "WIS": 10234,
      "AGI": 16448
    },
    "skills": [
      340
    ],
    "img": "343",
    "rarity": 4,
    "evo": 4,
    "fullName": "Moni the Dismemberer II"
  },
  "11088": {
    "name": "Ovinnik",
    "stats": {
      "HP": 19010,
      "ATK": 11210,
      "DEF": 20592,
      "WIS": 16627,
      "AGI": 12315
    },
    "skills": [
      356,
      342
    ],
    "autoAttack": 10007,
    "img": "3c1",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ovinnik, Hex Beast II"
  },
  "11090": {
    "name": "CSMM",
    "stats": {
      "HP": 14096,
      "ATK": 10112,
      "DEF": 10549,
      "WIS": 15804,
      "AGI": 17095
    },
    "skills": [
      343
    ],
    "autoAttack": 10007,
    "img": "26d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Cat Sith Magus Master II"
  },
  "11093": {
    "name": "Sinbad",
    "stats": {
      "HP": 15868,
      "ATK": 18154,
      "DEF": 14644,
      "WIS": 13853,
      "AGI": 17006
    },
    "skills": [
      318
    ],
    "img": "29e",
    "rarity": 5,
    "evo": 2,
    "fullName": "Sinbad the Adventurer II"
  },
  "11095": {
    "name": "Roc",
    "stats": {
      "HP": 12073,
      "ATK": 14879,
      "DEF": 12559,
      "WIS": 11501,
      "AGI": 16510
    },
    "skills": [
      322
    ],
    "img": "220",
    "rarity": 4,
    "evo": 4,
    "fullName": "Crystalwing Roc II"
  },
  "11096": {
    "name": "Djinn",
    "stats": {
      "HP": 14048,
      "ATK": 17363,
      "DEF": 13333,
      "WIS": 19422,
      "AGI": 16605
    },
    "skills": [
      319,
      320
    ],
    "img": "18d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Djinn of the Lamp II"
  },
  "11099": {
    "name": "Adranus",
    "stats": {
      "HP": 20223,
      "ATK": 23517,
      "DEF": 19855,
      "WIS": 18609,
      "AGI": 18046
    },
    "skills": [
      347
    ],
    "img": "275",
    "rarity": 6,
    "evo": 2,
    "fullName": "Adranus, Lava Beast II"
  },
  "11100": {
    "name": "Queen Waspmen",
    "stats": {
      "HP": 14070,
      "ATK": 19898,
      "DEF": 13247,
      "WIS": 15998,
      "AGI": 17829
    },
    "skills": [
      348
    ],
    "img": "1f6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Queen of the Waspmen II"
  },
  "11102": {
    "name": "Balgo",
    "stats": {
      "HP": 18585,
      "ATK": 16037,
      "DEF": 13962,
      "WIS": 5799,
      "AGI": 13510
    },
    "skills": [
      349
    ],
    "img": "2fd",
    "rarity": 4,
    "evo": 4,
    "fullName": "Balgo, the Cursed Flame II"
  },
  "11103": {
    "name": "Tiamat",
    "stats": {
      "HP": 13702,
      "ATK": 14698,
      "DEF": 16497,
      "WIS": 18869,
      "AGI": 15738
    },
    "skills": [
      280
    ],
    "img": "2c5",
    "rarity": 5,
    "evo": 2,
    "fullName": "Tiamat, Mother of Dragons II"
  },
  "11105": {
    "name": "Ares",
    "stats": {
      "HP": 25434,
      "ATK": 21285,
      "DEF": 21047,
      "WIS": 16345,
      "AGI": 17407
    },
    "skills": [
      542
    ],
    "img": "180",
    "rarity": 6,
    "evo": 2,
    "fullName": "Ares, God of Ruin II"
  },
  "11114": {
    "name": "Brownies",
    "stats": {
      "HP": 9821,
      "ATK": 11283,
      "DEF": 9515,
      "WIS": 13196,
      "AGI": 11414
    },
    "skills": [
      307
    ],
    "img": "190",
    "rarity": 4,
    "evo": 4,
    "fullName": "Brownies, the Uproarious II"
  },
  "11115": {
    "name": "Bearwolf",
    "stats": {
      "HP": 14503,
      "ATK": 24513,
      "DEF": 11492,
      "WIS": 11405,
      "AGI": 17992
    },
    "skills": [
      353
    ],
    "img": "25b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Frost Bearwolf II"
  },
  "11119": {
    "name": "Canhel",
    "stats": {
      "HP": 15608,
      "ATK": 19606,
      "DEF": 17992,
      "WIS": 11329,
      "AGI": 16399
    },
    "skills": [
      293
    ],
    "img": "254",
    "rarity": 5,
    "evo": 2,
    "fullName": "Canhel, Guardian Dragon II"
  },
  "11120": {
    "name": "Infested Minotaur",
    "stats": {
      "HP": 13691,
      "ATK": 15294,
      "DEF": 16031,
      "WIS": 9390,
      "AGI": 14070
    },
    "skills": [
      299,
      301
    ],
    "img": "3ab",
    "rarity": 4,
    "evo": 2,
    "fullName": "Infested Minotaur II"
  },
  "11121": {
    "name": "Kalevan",
    "stats": {
      "HP": 12629,
      "ATK": 18013,
      "DEF": 11914,
      "WIS": 12055,
      "AGI": 13821
    },
    "skills": [
      297,
      240
    ],
    "img": "3bd",
    "rarity": 4,
    "evo": 2,
    "fullName": "Kalevan, the Forest Green II"
  },
  "11122": {
    "name": "Tannin",
    "stats": {
      "HP": 13669,
      "ATK": 15500,
      "DEF": 12683,
      "WIS": 19541,
      "AGI": 17894
    },
    "skills": [
      298
    ],
    "img": "24a",
    "rarity": 5,
    "evo": 2,
    "fullName": "Tannin, Sea Dragon II"
  },
  "11124": {
    "name": "Ushabti",
    "stats": {
      "HP": 12434,
      "ATK": 16475,
      "DEF": 14655,
      "WIS": 10062,
      "AGI": 14027
    },
    "skills": [
      317
    ],
    "img": "21d",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ushabti II"
  },
  "11125": {
    "name": "Kekro",
    "stats": {
      "HP": 17992,
      "ATK": 12001,
      "DEF": 15002,
      "WIS": 19660,
      "AGI": 16302
    },
    "skills": [
      379
    ],
    "autoAttack": 10007,
    "img": "33b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kekro, Demiwyrm Magus II"
  },
  "11126": {
    "name": "Magdal M",
    "stats": {
      "HP": 18728,
      "ATK": 20917,
      "DEF": 21491,
      "WIS": 23235,
      "AGI": 15998
    },
    "skills": [
      336
    ],
    "img": "346",
    "rarity": 6,
    "evo": 2,
    "fullName": "Magdal, Dragonmaster II"
  },
  "11128": {
    "name": "Leupold",
    "stats": {
      "HP": 17585,
      "ATK": 11038,
      "DEF": 12963,
      "WIS": 9794,
      "AGI": 16510
    },
    "skills": [
      378
    ],
    "img": "4ca",
    "rarity": 4,
    "evo": 4,
    "fullName": "Leupold, Wyvern Knight II"
  },
  "11129": {
    "name": "Caassimolar",
    "stats": {
      "HP": 16009,
      "ATK": 24979,
      "DEF": 15587,
      "WIS": 10625,
      "AGI": 12521
    },
    "skills": [
      371
    ],
    "img": "1c7",
    "rarity": 5,
    "evo": 2,
    "fullName": "Caassimolar, the Chimera II"
  },
  "11131": {
    "name": "Gregory",
    "stats": {
      "HP": 16192,
      "ATK": 16121,
      "DEF": 15558,
      "WIS": 9794,
      "AGI": 10294
    },
    "skills": [
      372
    ],
    "img": "248",
    "rarity": 4,
    "evo": 4,
    "fullName": "Gregory, the Masked Slayer II"
  },
  "11134": {
    "name": "Minerva",
    "stats": {
      "HP": 14590,
      "ATK": 18024,
      "DEF": 14438,
      "WIS": 15435,
      "AGI": 18013
    },
    "skills": [
      357
    ],
    "img": "2a2",
    "rarity": 5,
    "evo": 2,
    "fullName": "Minerva, Goddess of War II"
  },
  "11136": {
    "name": "Marcus",
    "stats": {
      "HP": 12317,
      "ATK": 16534,
      "DEF": 14255,
      "WIS": 8991,
      "AGI": 15438
    },
    "skills": [
      358
    ],
    "img": "353",
    "rarity": 4,
    "evo": 4,
    "fullName": "Marcus, Brave of Liberation II"
  },
  "11137": {
    "name": "Venusia",
    "stats": {
      "HP": 14514,
      "ATK": 18273,
      "DEF": 13333,
      "WIS": 10831,
      "AGI": 11492
    },
    "skills": [
      361
    ],
    "img": "403",
    "rarity": 4,
    "evo": 2,
    "fullName": "Venusia, the Grace II"
  },
  "11141": {
    "name": "Lynx",
    "stats": {
      "HP": 14207,
      "ATK": 14062,
      "DEF": 12500,
      "WIS": 10014,
      "AGI": 17147
    },
    "skills": [
      493
    ],
    "img": "321",
    "rarity": 4,
    "evo": 4,
    "fullName": "Madprowl Lynx II"
  },
  "11143": {
    "name": "TBB",
    "stats": {
      "HP": 12001,
      "ATK": 9905,
      "DEF": 12207,
      "WIS": 17000,
      "AGI": 16803
    },
    "skills": [
      366
    ],
    "autoAttack": 10007,
    "img": "115",
    "rarity": 4,
    "evo": 4,
    "fullName": "Tormented Bone Beast II"
  },
  "11144": {
    "name": "Infested Cyclops",
    "stats": {
      "HP": 19508,
      "ATK": 19508,
      "DEF": 15392,
      "WIS": 9997,
      "AGI": 15348
    },
    "skills": [
      364
    ],
    "img": "3db",
    "rarity": 5,
    "evo": 2,
    "fullName": "Infested Cyclops II"
  },
  "11157": {
    "name": "Ausguss",
    "stats": {
      "HP": 14937,
      "ATK": 9087,
      "DEF": 12304,
      "WIS": 16952,
      "AGI": 14308
    },
    "skills": [
      708
    ],
    "autoAttack": 10007,
    "img": "3ce",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ausguss, Jailer II"
  },
  "11168": {
    "name": "Badalisc",
    "stats": {
      "HP": 14092,
      "ATK": 16107,
      "DEF": 11882,
      "WIS": 11297,
      "AGI": 15218
    },
    "skills": [
      315
    ],
    "img": "26c",
    "rarity": 4,
    "evo": 2,
    "fullName": "Badalisc, the Gourmet II"
  },
  "11169": {
    "name": "Jack",
    "stats": {
      "HP": 13507,
      "ATK": 9000,
      "DEF": 12196,
      "WIS": 16204,
      "AGI": 16995
    },
    "skills": [
      333
    ],
    "autoAttack": 10009,
    "img": "10b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Jack o' Frost II"
  },
  "11170": {
    "name": "Gryla",
    "stats": {
      "HP": 16529,
      "ATK": 11622,
      "DEF": 15868,
      "WIS": 15294,
      "AGI": 8740
    },
    "skills": [
      308,
      316
    ],
    "isMounted": true,
    "img": "2c3",
    "rarity": 4,
    "evo": 2,
    "fullName": "Gryla, the Lullaby II"
  },
  "11171": {
    "name": "Hyena",
    "stats": {
      "HP": 14644,
      "ATK": 10766,
      "DEF": 11860,
      "WIS": 18923,
      "AGI": 12228
    },
    "skills": [
      321
    ],
    "autoAttack": 10008,
    "img": "2fc",
    "rarity": 4,
    "evo": 2,
    "fullName": "Bronzeclad Hyena II"
  },
  "11172": {
    "name": "Galatea",
    "stats": {
      "HP": 19833,
      "ATK": 10062,
      "DEF": 15825,
      "WIS": 18566,
      "AGI": 15218
    },
    "skills": [
      533
    ],
    "autoAttack": 10007,
    "img": "48a",
    "rarity": 5,
    "evo": 2,
    "fullName": "Galatea, Nereid II"
  },
  "11175": {
    "name": "Taotie",
    "stats": {
      "HP": 14850,
      "ATK": 15803,
      "DEF": 13106,
      "WIS": 9141,
      "AGI": 14720
    },
    "skills": [
      949
    ],
    "autoAttack": 10005,
    "img": "2ef",
    "rarity": 4,
    "evo": 2,
    "fullName": "Taotie, the Gluttonous II"
  },
  "11177": {
    "name": "CSW",
    "stats": {
      "HP": 15804,
      "ATK": 16768,
      "DEF": 14000,
      "WIS": 5334,
      "AGI": 16707
    },
    "skills": [
      637
    ],
    "autoAttack": 10048,
    "img": "1d8",
    "rarity": 4,
    "evo": 4,
    "fullName": "Cat Sith Warlord II"
  },
  "11185": {
    "name": "Michael",
    "stats": {
      "HP": 24513,
      "ATK": 24004,
      "DEF": 22379,
      "WIS": 12640,
      "AGI": 18284
    },
    "skills": [
      1223
    ],
    "autoAttack": 10157,
    "img": "3a7",
    "rarity": 6,
    "evo": 2,
    "fullName": "Michael, Steelclad Angel II"
  },
  "11189": {
    "name": "Surtr",
    "stats": {
      "HP": 15440,
      "ATK": 17106,
      "DEF": 15085,
      "WIS": 7016,
      "AGI": 12890
    },
    "skills": [
      383
    ],
    "img": "15b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Surtr the Fervent II"
  },
  "11190": {
    "name": "Freyr",
    "stats": {
      "HP": 16562,
      "ATK": 19909,
      "DEF": 15370,
      "WIS": 12943,
      "AGI": 15998
    },
    "skills": [
      385,
      386
    ],
    "img": "151",
    "rarity": 5,
    "evo": 2,
    "fullName": "Freyr, God of the Harvest II"
  },
  "11191": {
    "name": "Freyja",
    "stats": {
      "HP": 14709,
      "ATK": 17125,
      "DEF": 14027,
      "WIS": 10213,
      "AGI": 12380
    },
    "skills": [
      387
    ],
    "img": "3c8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Freyja, Earth Goddess II"
  },
  "11194": {
    "name": "Tarantula",
    "stats": {
      "HP": 19324,
      "ATK": 14568,
      "DEF": 18024,
      "WIS": 15695,
      "AGI": 12120
    },
    "skills": [
      396,
      397
    ],
    "autoAttack": 10005,
    "img": "271",
    "rarity": 5,
    "evo": 2,
    "fullName": "Brass Tarantula II"
  },
  "11196": {
    "name": "Brass Gorilla",
    "stats": {
      "HP": 18996,
      "ATK": 9760,
      "DEF": 18096,
      "WIS": 12684,
      "AGI": 8319
    },
    "skills": [
      398
    ],
    "img": "26b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Reinforced Brass Gorilla II"
  },
  "11199": {
    "name": "High Priestess",
    "stats": {
      "HP": 17233,
      "ATK": 8350,
      "DEF": 20256,
      "WIS": 19086,
      "AGI": 14839
    },
    "skills": [
      388,
      389
    ],
    "autoAttack": 10007,
    "img": "458",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan High Priestess II"
  },
  "11200": {
    "name": "Libra",
    "stats": {
      "HP": 14178,
      "ATK": 16172,
      "DEF": 14698,
      "WIS": 9845,
      "AGI": 13669
    },
    "skills": [
      390
    ],
    "img": "486",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Libra II"
  },
  "11202": {
    "name": "Hereward",
    "stats": {
      "HP": 14927,
      "ATK": 14000,
      "DEF": 12524,
      "WIS": 10951,
      "AGI": 15498
    },
    "skills": [
      391
    ],
    "img": "105",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hereward, Storm of Arrows II"
  },
  "11203": {
    "name": "Prismatic",
    "stats": {
      "HP": 24004,
      "ATK": 14438,
      "DEF": 20982,
      "WIS": 23300,
      "AGI": 18024
    },
    "skills": [
      432
    ],
    "autoAttack": 10007,
    "img": "4fe",
    "rarity": 6,
    "evo": 2,
    "fullName": "Prismatic Wyvern"
  },
  "11204": {
    "name": "Seismo",
    "stats": {
      "HP": 18999,
      "ATK": 19097,
      "DEF": 15056,
      "WIS": 11015,
      "AGI": 16800
    },
    "skills": [
      433
    ],
    "img": "188",
    "rarity": 5,
    "evo": 2,
    "fullName": "Seismo Worm"
  },
  "11206": {
    "name": "Aeneas",
    "stats": {
      "HP": 14590,
      "ATK": 15630,
      "DEF": 13561,
      "WIS": 10311,
      "AGI": 13561
    },
    "skills": [
      400,
      401
    ],
    "img": "25c",
    "rarity": 4,
    "evo": 2,
    "fullName": "Aeneas, Fallen Hero II"
  },
  "11207": {
    "name": "Silver Dragon",
    "stats": {
      "HP": 19714,
      "ATK": 14601,
      "DEF": 15067,
      "WIS": 16215,
      "AGI": 18154
    },
    "skills": [
      522,
      523
    ],
    "autoAttack": 10024,
    "img": "48e",
    "rarity": 5,
    "evo": 2,
    "fullName": "Silver Dragon II"
  },
  "11208": {
    "name": "Magus",
    "stats": {
      "HP": 15186,
      "ATK": 12131,
      "DEF": 17688,
      "WIS": 19010,
      "AGI": 15641
    },
    "skills": [
      402,
      403
    ],
    "img": "1bb",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Magus II"
  },
  "11209": {
    "name": "Rabbit",
    "stats": {
      "HP": 18999,
      "ATK": 13951,
      "DEF": 20007,
      "WIS": 9986,
      "AGI": 18035
    },
    "skills": [
      435,
      436
    ],
    "img": "26e",
    "rarity": 5,
    "evo": 2,
    "fullName": "Brass Rabbit"
  },
  "11210": {
    "name": "Aries",
    "stats": {
      "HP": 14395,
      "ATK": 15543,
      "DEF": 16854,
      "WIS": 9011,
      "AGI": 12813
    },
    "skills": [
      392,
      393
    ],
    "img": "337",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Aries II"
  },
  "11211": {
    "name": "Empress",
    "stats": {
      "HP": 15197,
      "ATK": 12380,
      "DEF": 15348,
      "WIS": 19422,
      "AGI": 17168
    },
    "skills": [
      394,
      395
    ],
    "img": "104",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Empress II"
  },
  "11212": {
    "name": "Millarca",
    "stats": {
      "HP": 15305,
      "ATK": 10668,
      "DEF": 15565,
      "WIS": 21393,
      "AGI": 18046
    },
    "skills": [
      407,
      408
    ],
    "autoAttack": 10007,
    "img": "2ff",
    "rarity": 5,
    "evo": 2,
    "fullName": "Millarca, Lady of Thorns II"
  },
  "11213": {
    "name": "Cegila",
    "stats": {
      "HP": 13149,
      "ATK": 11492,
      "DEF": 9498,
      "WIS": 17504,
      "AGI": 16995
    },
    "skills": [
      354
    ],
    "img": "2a5",
    "rarity": 4,
    "evo": 2,
    "fullName": "Cegila, Dragonian Incantator II"
  },
  "11214": {
    "name": "Melek",
    "stats": {
      "HP": 19097,
      "ATK": 16107,
      "DEF": 21545,
      "WIS": 12792,
      "AGI": 10094
    },
    "skills": [
      374,
      375
    ],
    "img": "219",
    "rarity": 5,
    "evo": 2,
    "fullName": "Melek, the Black Peacock II"
  },
  "11215": {
    "name": "Rohde",
    "stats": {
      "HP": 17591,
      "ATK": 8101,
      "DEF": 16042,
      "WIS": 15305,
      "AGI": 10582
    },
    "skills": [
      376,
      377
    ],
    "autoAttack": 10007,
    "img": "23b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Rohde, the Rose Thorn II"
  },
  "11218": {
    "name": "Xaphan",
    "stats": {
      "HP": 13013,
      "ATK": 9415,
      "DEF": 12573,
      "WIS": 17000,
      "AGI": 15537
    },
    "skills": [
      412
    ],
    "img": "47f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Xaphan, the Foul Flame II"
  },
  "11219": {
    "name": "Sigiled Corpse Beast",
    "stats": {
      "HP": 17006,
      "ATK": 12954,
      "DEF": 14926,
      "WIS": 19855,
      "AGI": 16042
    },
    "skills": [
      414,
      415
    ],
    "autoAttack": 10007,
    "img": "1f6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Sigiled Corpse Beast II"
  },
  "11220": {
    "name": "Sigiled Axeman",
    "stats": {
      "HP": 14644,
      "ATK": 9076,
      "DEF": 12987,
      "WIS": 18338,
      "AGI": 13409
    },
    "skills": [
      416
    ],
    "autoAttack": 10007,
    "img": "39e",
    "rarity": 4,
    "evo": 2,
    "fullName": "Sigiled Skeleton Axeman II"
  },
  "11223": {
    "name": "Brang",
    "stats": {
      "HP": 18826,
      "ATK": 18544,
      "DEF": 14027,
      "WIS": 18208,
      "AGI": 10105
    },
    "skills": [
      423
    ],
    "autoAttack": 10010,
    "img": "4f3",
    "rarity": 5,
    "evo": 2,
    "fullName": "Brang Two-Heads II"
  },
  "11225": {
    "name": "Dein",
    "stats": {
      "HP": 14000,
      "ATK": 16768,
      "DEF": 11098,
      "WIS": 11683,
      "AGI": 14417
    },
    "skills": [
      424
    ],
    "img": "48e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Dein, Silent Bomber II"
  },
  "11229": {
    "name": "Pisces",
    "stats": {
      "HP": 13041,
      "ATK": 8621,
      "DEF": 14796,
      "WIS": 17114,
      "AGI": 14991
    },
    "skills": [
      419
    ],
    "autoAttack": 10007,
    "img": "122",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Pisces II"
  },
  "11231": {
    "name": "Palna",
    "stats": {
      "HP": 14999,
      "ATK": 15509,
      "DEF": 14606,
      "WIS": 8991,
      "AGI": 13807
    },
    "skills": [
      420
    ],
    "img": "3fb",
    "rarity": 4,
    "evo": 4,
    "fullName": "Palna, the Vanguard II"
  },
  "11233": {
    "name": "Musashi",
    "stats": {
      "HP": 20592,
      "ATK": 24752,
      "DEF": 19151,
      "WIS": 17981,
      "AGI": 18024
    },
    "skills": [
      404
    ],
    "img": "11f",
    "rarity": 6,
    "evo": 2,
    "fullName": "Musashi, the Twinblade II"
  },
  "11234": {
    "name": "Saizo",
    "stats": {
      "HP": 16128,
      "ATK": 12055,
      "DEF": 16367,
      "WIS": 19422,
      "AGI": 16995
    },
    "skills": [
      405
    ],
    "autoAttack": 10007,
    "img": "241",
    "rarity": 5,
    "evo": 2,
    "fullName": "Saizo, Phantom Ninja II"
  },
  "11236": {
    "name": "Tomoe",
    "stats": {
      "HP": 13889,
      "ATK": 16010,
      "DEF": 13110,
      "WIS": 8285,
      "AGI": 16622
    },
    "skills": [
      406
    ],
    "img": "2b5",
    "rarity": 4,
    "evo": 4,
    "fullName": "Tomoe, the Lightning Arrow II"
  },
  "11237": {
    "name": "Pollux",
    "stats": {
      "HP": 13290,
      "ATK": 18631,
      "DEF": 11654,
      "WIS": 10311,
      "AGI": 13756
    },
    "skills": [
      427,
      428
    ],
    "img": "1a2",
    "rarity": 4,
    "evo": 2,
    "fullName": "Pollux, Fallen Hero II"
  },
  "11239": {
    "name": "Emperor",
    "stats": {
      "HP": 18577,
      "ATK": 17916,
      "DEF": 17786,
      "WIS": 10809,
      "AGI": 14590
    },
    "skills": [
      425,
      426
    ],
    "img": "102",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Emperor II"
  },
  "11241": {
    "name": "Virgo",
    "stats": {
      "HP": 15500,
      "ATK": 6118,
      "DEF": 12380,
      "WIS": 17797,
      "AGI": 16822
    },
    "skills": [
      421,
      422
    ],
    "autoAttack": 10007,
    "img": "4cf",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Virgo II"
  },
  "11242": {
    "name": "Lovers",
    "stats": {
      "HP": 16908,
      "ATK": 13875,
      "DEF": 12705,
      "WIS": 19021,
      "AGI": 17006
    },
    "skills": [
      430,
      431
    ],
    "autoAttack": 10007,
    "img": "3fb",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Lovers II"
  },
  "11243": {
    "name": "Bandersnatch",
    "stats": {
      "HP": 21805,
      "ATK": 8047,
      "DEF": 14200,
      "WIS": 19183,
      "AGI": 17786
    },
    "skills": [
      635
    ],
    "autoAttack": 10046,
    "img": "1bc",
    "rarity": 5,
    "evo": 2,
    "fullName": "Bandersnatch, Beast Divine II"
  },
  "11245": {
    "name": "Anneberg",
    "stats": {
      "HP": 19097,
      "ATK": 18241,
      "DEF": 17038,
      "WIS": 8794,
      "AGI": 16518
    },
    "skills": [
      489,
      490
    ],
    "img": "1e1",
    "rarity": 5,
    "evo": 2,
    "fullName": "Anneberg, Steel Steed II"
  },
  "11256": {
    "name": "Baba",
    "stats": {
      "HP": 14698,
      "ATK": 5013,
      "DEF": 13799,
      "WIS": 19097,
      "AGI": 17396
    },
    "skills": [
      1150,
      1151
    ],
    "autoAttack": 10175,
    "img": "24b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Baba Yaga II"
  },
  "11258": {
    "name": "Amazon",
    "stats": {
      "HP": 15034,
      "ATK": 16670,
      "DEF": 14048,
      "WIS": 8025,
      "AGI": 16107
    },
    "skills": [
      875
    ],
    "autoAttack": 10116,
    "img": "2a8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Amazon Berserker II"
  },
  "11259": {
    "name": "Peg Powler",
    "stats": {
      "HP": 15500,
      "ATK": 7353,
      "DEF": 12499,
      "WIS": 17049,
      "AGI": 16204
    },
    "skills": [
      636
    ],
    "autoAttack": 10047,
    "img": "30c",
    "rarity": 4,
    "evo": 2,
    "fullName": "Peg Powler II"
  },
  "11261": {
    "name": "Rahab",
    "stats": {
      "HP": 14073,
      "ATK": 12597,
      "DEF": 15498,
      "WIS": 9004,
      "AGI": 16754
    },
    "skills": [
      434
    ],
    "img": "21c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Abyssal Rahab II"
  },
  "11266": {
    "name": "Jormungandr",
    "stats": {
      "HP": 13024,
      "ATK": 16768,
      "DEF": 11756,
      "WIS": 10112,
      "AGI": 15889
    },
    "skills": [
      438
    ],
    "autoAttack": 10012,
    "img": "397",
    "rarity": 4,
    "evo": 4,
    "fullName": "Jormungandr, World Serpent II"
  },
  "11267": {
    "name": "Odin L",
    "stats": {
      "HP": 15110,
      "ATK": 16562,
      "DEF": 13875,
      "WIS": 17363,
      "AGI": 18057
    },
    "skills": [
      440,
      441
    ],
    "isMounted": true,
    "img": "365",
    "rarity": 5,
    "evo": 2,
    "fullName": "Odin, God of Victory II"
  },
  "11268": {
    "name": "Vafthruthnir",
    "stats": {
      "HP": 15500,
      "ATK": 17732,
      "DEF": 13008,
      "WIS": 9997,
      "AGI": 12228
    },
    "skills": [
      442
    ],
    "img": "22b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Vafthruthnir, Elder Giant II"
  },
  "11271": {
    "name": "Ghislandi L",
    "stats": {
      "HP": 18533,
      "ATK": 20234,
      "DEF": 14590,
      "WIS": 10235,
      "AGI": 16204
    },
    "skills": [
      455,
      456
    ],
    "autoAttack": 10015,
    "img": "391",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ghislandi, the Unchained II"
  },
  "11273": {
    "name": "Slagh",
    "stats": {
      "HP": 12978,
      "ATK": 16561,
      "DEF": 11098,
      "WIS": 11683,
      "AGI": 15631
    },
    "skills": [
      457
    ],
    "img": "13c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Slagh, Carnage Incarnate II"
  },
  "11277": {
    "name": "Leo",
    "stats": {
      "HP": 15121,
      "ATK": 15002,
      "DEF": 14200,
      "WIS": 7440,
      "AGI": 16811
    },
    "skills": [
      448
    ],
    "autoAttack": 10014,
    "img": "491",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Leo II"
  },
  "11279": {
    "name": "Kobold",
    "stats": {
      "HP": 14207,
      "ATK": 14462,
      "DEF": 15804,
      "WIS": 8442,
      "AGI": 14999
    },
    "skills": [
      449
    ],
    "img": "16e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Kobold Guard Captain II"
  },
  "11280": {
    "name": "Managarmr M",
    "stats": {
      "HP": 20007,
      "ATK": 21599,
      "DEF": 17396,
      "WIS": 23907,
      "AGI": 18100
    },
    "skills": [
      463
    ],
    "autoAttack": 10007,
    "img": "42b",
    "rarity": 6,
    "evo": 2,
    "fullName": "Managarmr, the Frost Moon II"
  },
  "11281": {
    "name": "Chariot",
    "stats": {
      "HP": 17342,
      "ATK": 19346,
      "DEF": 16453,
      "WIS": 10376,
      "AGI": 17472
    },
    "skills": [
      464
    ],
    "img": "3da",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Chariot II"
  },
  "11282": {
    "name": "Achilles",
    "stats": {
      "HP": 13593,
      "ATK": 15630,
      "DEF": 11362,
      "WIS": 10603,
      "AGI": 16562
    },
    "skills": [
      459,
      460
    ],
    "img": "1c7",
    "rarity": 4,
    "evo": 2,
    "fullName": "Achilles, Fallen Hero II"
  },
  "11284": {
    "name": "Might",
    "stats": {
      "HP": 18598,
      "ATK": 19227,
      "DEF": 10766,
      "WIS": 13301,
      "AGI": 17948
    },
    "skills": [
      461,
      462
    ],
    "isMounted": true,
    "img": "2a4",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Might II"
  },
  "11286": {
    "name": "Aquarius",
    "stats": {
      "HP": 16323,
      "ATK": 7494,
      "DEF": 11448,
      "WIS": 17363,
      "AGI": 16009
    },
    "skills": [
      450,
      451
    ],
    "autoAttack": 10007,
    "img": "2b9",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Aquarius II"
  },
  "11287": {
    "name": "Hermit",
    "stats": {
      "HP": 19205,
      "ATK": 12066,
      "DEF": 12586,
      "WIS": 20722,
      "AGI": 15002
    },
    "skills": [
      453,
      454
    ],
    "autoAttack": 10007,
    "img": "3c5",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Hermit II"
  },
  "11290": {
    "name": "Set",
    "stats": {
      "HP": 13097,
      "ATK": 16364,
      "DEF": 10990,
      "WIS": 10001,
      "AGI": 17133
    },
    "skills": [
      469
    ],
    "img": "2c6",
    "rarity": 4,
    "evo": 4,
    "fullName": "Set, God of the Sands II"
  },
  "11292": {
    "name": "Anubis",
    "stats": {
      "HP": 14330,
      "ATK": 17006,
      "DEF": 12510,
      "WIS": 10625,
      "AGI": 14005
    },
    "skills": [
      473,
      474
    ],
    "img": "247",
    "rarity": 4,
    "evo": 2,
    "fullName": "Anubis, Keeper of the Dead II"
  },
  "11295": {
    "name": "Ryaum",
    "stats": {
      "HP": 19454,
      "ATK": 13561,
      "DEF": 17667,
      "WIS": 11221,
      "AGI": 17602
    },
    "skills": [
      482,
      483
    ],
    "img": "237",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ryaum, Hussar Captain II"
  },
  "11297": {
    "name": "Hoska",
    "stats": {
      "HP": 18996,
      "ATK": 7906,
      "DEF": 15096,
      "WIS": 17023,
      "AGI": 8881
    },
    "skills": [
      484,
      485
    ],
    "autoAttack": 10016,
    "img": "26c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hoska, the Firestroke II"
  },
  "11301": {
    "name": "Capricorn",
    "stats": {
      "HP": 14937,
      "ATK": 8491,
      "DEF": 13507,
      "WIS": 16551,
      "AGI": 15099
    },
    "skills": [
      476
    ],
    "autoAttack": 10007,
    "img": "2f4",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Capricorn II"
  },
  "11303": {
    "name": "Dunkleosteus",
    "stats": {
      "HP": 14000,
      "ATK": 8394,
      "DEF": 13110,
      "WIS": 16620,
      "AGI": 15804
    },
    "skills": [
      477
    ],
    "autoAttack": 10007,
    "img": "222",
    "rarity": 4,
    "evo": 4,
    "fullName": "Dunkleosteus, the Rendmaw II"
  },
  "11304": {
    "name": "Gigantopithecus",
    "stats": {
      "HP": 24210,
      "ATK": 25055,
      "DEF": 21946,
      "WIS": 13994,
      "AGI": 15998
    },
    "skills": [
      491
    ],
    "img": "3e5",
    "rarity": 6,
    "evo": 2,
    "fullName": "Gigantopithecus II"
  },
  "11305": {
    "name": "Microraptor",
    "stats": {
      "HP": 16172,
      "ATK": 18577,
      "DEF": 14406,
      "WIS": 14092,
      "AGI": 17753
    },
    "skills": [
      492
    ],
    "img": "414",
    "rarity": 5,
    "evo": 2,
    "fullName": "Microraptor II"
  },
  "11306": {
    "name": "Circe",
    "stats": {
      "HP": 15002,
      "ATK": 7776,
      "DEF": 11947,
      "WIS": 17017,
      "AGI": 16009
    },
    "skills": [
      487,
      488
    ],
    "autoAttack": 10007,
    "img": "20f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Circe, Fallen Heroine II"
  },
  "11310": {
    "name": "Cancer",
    "stats": {
      "HP": 16627,
      "ATK": 17201,
      "DEF": 10408,
      "WIS": 7494,
      "AGI": 16908
    },
    "skills": [
      478,
      479
    ],
    "img": "24e",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Cancer II"
  },
  "11311": {
    "name": "Hanged Man",
    "stats": {
      "HP": 20505,
      "ATK": 15002,
      "DEF": 13008,
      "WIS": 13030,
      "AGI": 18024
    },
    "skills": [
      480,
      481
    ],
    "img": "489",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Hanged Man II"
  },
  "11314": {
    "name": "Kua Fu",
    "stats": {
      "HP": 16510,
      "ATK": 16561,
      "DEF": 12207,
      "WIS": 9174,
      "AGI": 13476
    },
    "skills": [
      497
    ],
    "img": "3e3",
    "rarity": 4,
    "evo": 4,
    "fullName": "Kua Fu, Sun Chaser II"
  },
  "11315": {
    "name": "Xuan Wu",
    "stats": {
      "HP": 18013,
      "ATK": 18609,
      "DEF": 17038,
      "WIS": 13821,
      "AGI": 13507
    },
    "skills": [
      499,
      500
    ],
    "autoAttack": 10020,
    "img": "325",
    "rarity": 5,
    "evo": 2,
    "fullName": "Xuan Wu II"
  },
  "11316": {
    "name": "Long Feng",
    "stats": {
      "HP": 15164,
      "ATK": 17125,
      "DEF": 13539,
      "WIS": 10452,
      "AGI": 12207
    },
    "skills": [
      501
    ],
    "img": "2ad",
    "rarity": 4,
    "evo": 2,
    "fullName": "Long Feng, the Dragon Fist II"
  },
  "11319": {
    "name": "Manannan",
    "stats": {
      "HP": 16551,
      "ATK": 10668,
      "DEF": 16464,
      "WIS": 19227,
      "AGI": 16605
    },
    "skills": [
      513,
      514
    ],
    "autoAttack": 10007,
    "img": "4a4",
    "rarity": 5,
    "evo": 2,
    "fullName": "Manannan mac Lir II"
  },
  "11321": {
    "name": "Selkie",
    "stats": {
      "HP": 15804,
      "ATK": 8442,
      "DEF": 14049,
      "WIS": 16024,
      "AGI": 13586
    },
    "skills": [
      515,
      516
    ],
    "autoAttack": 10007,
    "img": "431",
    "rarity": 4,
    "evo": 4,
    "fullName": "Selkie, Lady of the Shore II"
  },
  "11324": {
    "name": "Death",
    "stats": {
      "HP": 20234,
      "ATK": 19508,
      "DEF": 13008,
      "WIS": 13019,
      "AGI": 18111
    },
    "skills": [
      546,
      547
    ],
    "autoAttack": 10028,
    "isMounted": true,
    "img": "25b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Death II"
  },
  "11325": {
    "name": "Gemini",
    "stats": {
      "HP": 15197,
      "ATK": 15641,
      "DEF": 10343,
      "WIS": 10148,
      "AGI": 17147
    },
    "skills": [
      511,
      512
    ],
    "isMounted": true,
    "img": "240",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Gemini II"
  },
  "11328": {
    "name": "Dagon",
    "stats": {
      "HP": 23343,
      "ATK": 22065,
      "DEF": 18035,
      "WIS": 19703,
      "AGI": 18208
    },
    "skills": [
      519
    ],
    "img": "36a",
    "rarity": 6,
    "evo": 2,
    "fullName": "Dagon II"
  },
  "11329": {
    "name": "Archbishop",
    "stats": {
      "HP": 19064,
      "ATK": 20191,
      "DEF": 16009,
      "WIS": 10744,
      "AGI": 15002
    },
    "skills": [
      520
    ],
    "autoAttack": 10025,
    "img": "39a",
    "rarity": 5,
    "evo": 2,
    "fullName": "Archbishop of the Deep II"
  },
  "11330": {
    "name": "Orpheus",
    "stats": {
      "HP": 17212,
      "ATK": 6162,
      "DEF": 13658,
      "WIS": 16529,
      "AGI": 16497
    },
    "skills": [
      1282
    ],
    "autoAttack": 10186,
    "img": "3bb",
    "rarity": 4,
    "evo": 2,
    "fullName": "Orpheus, Fallen Hero II"
  },
  "11332": {
    "name": "Temperance",
    "stats": {
      "HP": 19183,
      "ATK": 3800,
      "DEF": 20007,
      "WIS": 19985,
      "AGI": 18046
    },
    "skills": [
      543
    ],
    "autoAttack": 10027,
    "img": "38d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Temperance II"
  },
  "11333": {
    "name": "Kidd",
    "stats": {
      "HP": 18403,
      "ATK": 18046,
      "DEF": 12781,
      "WIS": 14395,
      "AGI": 16085
    },
    "skills": [
      518,
      157
    ],
    "img": "442",
    "rarity": 5,
    "evo": 2,
    "fullName": "Captain Kidd II"
  },
  "11334": {
    "name": "Sagittarius",
    "stats": {
      "HP": 15587,
      "ATK": 15218,
      "DEF": 12163,
      "WIS": 8415,
      "AGI": 17255
    },
    "skills": [
      507,
      508
    ],
    "img": "3c0",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Sagittarius II"
  },
  "11335": {
    "name": "Daemon",
    "stats": {
      "HP": 18252,
      "ATK": 20700,
      "DEF": 12510,
      "WIS": 13117,
      "AGI": 15023
    },
    "skills": [
      509,
      510
    ],
    "img": "249",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Daemon II"
  },
  "11342": {
    "name": "Ghost Ship",
    "stats": {
      "HP": 15365,
      "ATK": 12879,
      "DEF": 11928,
      "WIS": 10951,
      "AGI": 16803
    },
    "skills": [
      525
    ],
    "img": "20f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Inhabited Ghost Ship II"
  },
  "11343": {
    "name": "Sachiel",
    "stats": {
      "HP": 19357,
      "ATK": 14059,
      "DEF": 13052,
      "WIS": 17017,
      "AGI": 17526
    },
    "skills": [
      527,
      528
    ],
    "img": "42b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Sachiel, Angel of Water II"
  },
  "11344": {
    "name": "Afanc",
    "stats": {
      "HP": 16518,
      "ATK": 8610,
      "DEF": 14124,
      "WIS": 16020,
      "AGI": 13214
    },
    "skills": [
      529,
      530
    ],
    "autoAttack": 10003,
    "img": "4a1",
    "rarity": 4,
    "evo": 2,
    "fullName": "Afanc, Beast of the Deep II"
  },
  "11347": {
    "name": "Lava Dragon",
    "stats": {
      "HP": 19021,
      "ATK": 8881,
      "DEF": 16237,
      "WIS": 18891,
      "AGI": 16497
    },
    "skills": [
      534,
      535
    ],
    "autoAttack": 10019,
    "img": "3de",
    "rarity": 5,
    "evo": 2,
    "fullName": "Lava Dragon II"
  },
  "11351": {
    "name": "Ivy",
    "stats": {
      "HP": 16341,
      "ATK": 3882,
      "DEF": 13803,
      "WIS": 15889,
      "AGI": 17998
    },
    "skills": [
      536
    ],
    "autoAttack": 10026,
    "img": "373",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ivy the Verdant II"
  },
  "11353": {
    "name": "Scorpio",
    "stats": {
      "HP": 14146,
      "ATK": 15998,
      "DEF": 13117,
      "WIS": 8350,
      "AGI": 16995
    },
    "skills": [
      544
    ],
    "img": "4fe",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Scorpio II"
  },
  "11355": {
    "name": "Dong",
    "stats": {
      "HP": 13489,
      "ATK": 17000,
      "DEF": 13196,
      "WIS": 8150,
      "AGI": 16110
    },
    "skills": [
      545
    ],
    "img": "48b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Dong, the Bloody Claw II"
  },
  "11356": {
    "name": "Ulfhe",
    "stats": {
      "HP": 24102,
      "ATK": 22921,
      "DEF": 18447,
      "WIS": 18057,
      "AGI": 18219
    },
    "skills": [
      702
    ],
    "autoAttack": 10062,
    "img": "268",
    "rarity": 6,
    "evo": 2,
    "fullName": "Ulfhe, Sword-Shield Master II"
  },
  "11358": {
    "name": "Europa",
    "stats": {
      "HP": 14731,
      "ATK": 8296,
      "DEF": 12207,
      "WIS": 16735,
      "AGI": 16518
    },
    "skills": [
      538,
      539
    ],
    "autoAttack": 10007,
    "img": "425",
    "rarity": 4,
    "evo": 2,
    "fullName": "Europa, Fallen Heroine II"
  },
  "11360": {
    "name": "Star",
    "stats": {
      "HP": 20223,
      "ATK": 7548,
      "DEF": 18035,
      "WIS": 18208,
      "AGI": 15803
    },
    "skills": [
      540,
      541
    ],
    "autoAttack": 10007,
    "img": "475",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Star II"
  },
  "11361": {
    "name": "Ashlee",
    "stats": {
      "HP": 17038,
      "ATK": 16042,
      "DEF": 15045,
      "WIS": 13431,
      "AGI": 17992
    },
    "skills": [
      623
    ],
    "autoAttack": 10029,
    "img": "3f7",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ashlee Steamsaw II"
  },
  "11362": {
    "name": "Taurus",
    "stats": {
      "HP": 15608,
      "ATK": 18598,
      "DEF": 10105,
      "WIS": 7007,
      "AGI": 17363
    },
    "skills": [
      553,
      554
    ],
    "img": "2d3",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paladin of Taurus II"
  },
  "11363": {
    "name": "Moon",
    "stats": {
      "HP": 18273,
      "ATK": 18046,
      "DEF": 13279,
      "WIS": 12467,
      "AGI": 17948
    },
    "skills": [
      551,
      552
    ],
    "autoAttack": 10030,
    "img": "3b8",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Moon II"
  },
  "11364": {
    "name": "Apsara",
    "stats": {
      "HP": 15717,
      "ATK": 4992,
      "DEF": 14113,
      "WIS": 17179,
      "AGI": 17006
    },
    "skills": [
      630,
      631
    ],
    "autoAttack": 10007,
    "img": "152",
    "rarity": 4,
    "evo": 2,
    "fullName": "Apsara, Spirit of Water II"
  },
  "11366": {
    "name": "CSS",
    "stats": {
      "HP": 15034,
      "ATK": 16518,
      "DEF": 13052,
      "WIS": 7202,
      "AGI": 16811
    },
    "skills": [
      549
    ],
    "img": "17b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Cat Sith Swordswoman II"
  },
  "11369": {
    "name": "Nin-Ridu",
    "stats": {
      "HP": 16529,
      "ATK": 16215,
      "DEF": 11351,
      "WIS": 10495,
      "AGI": 14005
    },
    "skills": [
      505
    ],
    "autoAttack": 10022,
    "img": "239",
    "rarity": 4,
    "evo": 2,
    "fullName": "Nin-Ridu"
  },
  "11371": {
    "name": "Bayam",
    "stats": {
      "HP": 13269,
      "ATK": 7966,
      "DEF": 12804,
      "WIS": 17106,
      "AGI": 16779
    },
    "skills": [
      506
    ],
    "autoAttack": 10023,
    "img": "171",
    "rarity": 4,
    "evo": 4,
    "fullName": "Bayam II"
  },
  "11374": {
    "name": "Pazuzu",
    "stats": {
      "HP": 15121,
      "ATK": 17182,
      "DEF": 14988,
      "WIS": 5640,
      "AGI": 14999
    },
    "skills": [
      556
    ],
    "img": "24d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Pazuzu, the Whirling Jinn II"
  },
  "11375": {
    "name": "Gilgamesh",
    "stats": {
      "HP": 20115,
      "ATK": 19053,
      "DEF": 18013,
      "WIS": 8220,
      "AGI": 16096
    },
    "skills": [
      558,
      559
    ],
    "img": "1e1",
    "rarity": 5,
    "evo": 2,
    "fullName": "Gilgamesh the Bold II"
  },
  "11376": {
    "name": "Ishtar",
    "stats": {
      "HP": 16009,
      "ATK": 16074,
      "DEF": 13106,
      "WIS": 9022,
      "AGI": 14265
    },
    "skills": [
      560,
      561
    ],
    "img": "24d",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ishtar, Goddess of Love II"
  },
  "11379": {
    "name": "Seimei",
    "stats": {
      "HP": 19963,
      "ATK": 6389,
      "DEF": 17038,
      "WIS": 19053,
      "AGI": 17103
    },
    "skills": [
      564,
      565
    ],
    "autoAttack": 10007,
    "img": "4b7",
    "rarity": 5,
    "evo": 2,
    "fullName": "Seimei, Onmyoji II"
  },
  "11381": {
    "name": "Kijin",
    "stats": {
      "HP": 17047,
      "ATK": 3323,
      "DEF": 14038,
      "WIS": 17402,
      "AGI": 16110
    },
    "skills": [
      566
    ],
    "autoAttack": 10031,
    "img": "23a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Kijin, Heavenly Maiden II"
  },
  "11385": {
    "name": "Aeshma",
    "stats": {
      "HP": 17558,
      "ATK": 17212,
      "DEF": 15034,
      "WIS": 5804,
      "AGI": 13019
    },
    "skills": [
      579
    ],
    "autoAttack": 10035,
    "img": "243",
    "rarity": 4,
    "evo": 2,
    "fullName": "Aeshma, the Tyrant II"
  },
  "11387": {
    "name": "Simurgh",
    "stats": {
      "HP": 15524,
      "ATK": 6956,
      "DEF": 12145,
      "WIS": 17206,
      "AGI": 16110
    },
    "skills": [
      580
    ],
    "autoAttack": 10007,
    "img": "2a2",
    "rarity": 4,
    "evo": 4,
    "fullName": "Simurgh, Bird Divine II"
  },
  "11388": {
    "name": "Azi",
    "stats": {
      "HP": 20375,
      "ATK": 20202,
      "DEF": 20104,
      "WIS": 22899,
      "AGI": 18057
    },
    "skills": [
      572
    ],
    "autoAttack": 10033,
    "img": "25b",
    "rarity": 6,
    "evo": 2,
    "fullName": "Azi Dahaka II"
  },
  "11389": {
    "name": "Ophiuchus",
    "stats": {
      "HP": 19508,
      "ATK": 9000,
      "DEF": 15002,
      "WIS": 19541,
      "AGI": 17504
    },
    "skills": [
      583,
      584
    ],
    "autoAttack": 10007,
    "img": "13d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Paladin of Ophiuchus II"
  },
  "11390": {
    "name": "Suzhen",
    "stats": {
      "HP": 15998,
      "ATK": 3096,
      "DEF": 15002,
      "WIS": 17504,
      "AGI": 17006
    },
    "skills": [
      81
    ],
    "autoAttack": 10031,
    "img": "105",
    "rarity": 4,
    "evo": 2,
    "fullName": "Bai Suzhen, Lady of Scales II"
  },
  "11392": {
    "name": "Viper",
    "stats": {
      "HP": 14999,
      "ATK": 12999,
      "DEF": 14999,
      "WIS": 7808,
      "AGI": 17133
    },
    "skills": [
      574
    ],
    "img": "338",
    "rarity": 4,
    "evo": 4,
    "fullName": "Clockwork Viper II"
  },
  "11393": {
    "name": "Icarus",
    "stats": {
      "HP": 15186,
      "ATK": 14796,
      "DEF": 14005,
      "WIS": 7137,
      "AGI": 17363
    },
    "skills": [
      568,
      569
    ],
    "img": "194",
    "rarity": 4,
    "evo": 2,
    "fullName": "Icarus, Fallen Hero II"
  },
  "11394": {
    "name": "Sun",
    "stats": {
      "HP": 20299,
      "ATK": 7982,
      "DEF": 16356,
      "WIS": 18013,
      "AGI": 17916
    },
    "skills": [
      570,
      571
    ],
    "autoAttack": 10032,
    "img": "10a",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Sun II"
  },
  "11395": {
    "name": "Judgment",
    "stats": {
      "HP": 19996,
      "ATK": 7754,
      "DEF": 16009,
      "WIS": 19508,
      "AGI": 17753
    },
    "skills": [
      573
    ],
    "autoAttack": 10003,
    "img": "172",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Judgment II"
  },
  "11396": {
    "name": "Wicker Man",
    "stats": {
      "HP": 16605,
      "ATK": 6833,
      "DEF": 11654,
      "WIS": 16670,
      "AGI": 16930
    },
    "skills": [
      581,
      582
    ],
    "autoAttack": 10036,
    "img": "2d2",
    "rarity": 4,
    "evo": 2,
    "fullName": "Wicker Man II"
  },
  "11397": {
    "name": "Terra",
    "stats": {
      "HP": 19053,
      "ATK": 7267,
      "DEF": 17006,
      "WIS": 22498,
      "AGI": 18100
    },
    "skills": [
      575,
      576
    ],
    "autoAttack": 10007,
    "img": "325",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Terra II"
  },
  "11400": {
    "name": "Ales",
    "stats": {
      "HP": 18119,
      "ATK": 18009,
      "DEF": 16024,
      "WIS": 10101,
      "AGI": 5884
    },
    "skills": [
      562,
      563
    ],
    "img": "4d5",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ales Darkblood II"
  },
  "11401": {
    "name": "Huan",
    "stats": {
      "HP": 14005,
      "ATK": 14406,
      "DEF": 13106,
      "WIS": 9997,
      "AGI": 16096
    },
    "skills": [
      577
    ],
    "img": "1d4",
    "rarity": 4,
    "evo": 2,
    "fullName": "Huan, Doomcaller II"
  },
  "11406": {
    "name": "Vucub",
    "stats": {
      "HP": 16123,
      "ATK": 13110,
      "DEF": 14732,
      "WIS": 6967,
      "AGI": 17000
    },
    "skills": [
      586
    ],
    "img": "2aa",
    "rarity": 4,
    "evo": 4,
    "fullName": "Vucub Caquix, the Barbarian II"
  },
  "11407": {
    "name": "Ixtab",
    "stats": {
      "HP": 20007,
      "ATK": 8502,
      "DEF": 17472,
      "WIS": 17504,
      "AGI": 18013
    },
    "skills": [
      588,
      589
    ],
    "autoAttack": 10031,
    "img": "294",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ixtab, Guardian of the Dead II"
  },
  "11408": {
    "name": "Pakal",
    "stats": {
      "HP": 15435,
      "ATK": 15175,
      "DEF": 10777,
      "WIS": 10018,
      "AGI": 17103
    },
    "skills": [
      590,
      591
    ],
    "img": "168",
    "rarity": 4,
    "evo": 2,
    "fullName": "Pakal, Jade King II"
  },
  "11413": {
    "name": "Sera",
    "stats": {
      "HP": 14293,
      "ATK": 17023,
      "DEF": 13306,
      "WIS": 7406,
      "AGI": 15903
    },
    "skills": [
      594,
      595
    ],
    "img": "284",
    "rarity": 4,
    "evo": 4,
    "fullName": "Sera, Exorcist II"
  },
  "11415": {
    "name": "Dantalion",
    "stats": {
      "HP": 15193,
      "ATK": 5298,
      "DEF": 10990,
      "WIS": 14207,
      "AGI": 11098
    },
    "skills": [
      596
    ],
    "autoAttack": 10007,
    "img": "18e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Dantalion, Duke of Hell II"
  },
  "11425": {
    "name": "Pelops",
    "stats": {
      "HP": 15056,
      "ATK": 14113,
      "DEF": 10018,
      "WIS": 12055,
      "AGI": 17266
    },
    "skills": [
      597,
      598
    ],
    "img": "3ee",
    "rarity": 4,
    "evo": 2,
    "fullName": "Pelops, Fallen Hero II"
  },
  "11427": {
    "name": "Fool",
    "stats": {
      "HP": 20613,
      "ATK": 20104,
      "DEF": 18057,
      "WIS": 13182,
      "AGI": 11102
    },
    "skills": [
      632,
      633
    ],
    "isMounted": true,
    "img": "3f3",
    "rarity": 5,
    "evo": 2,
    "fullName": "Arcanan Fool II"
  },
  "11428": {
    "name": "Hash",
    "stats": {
      "HP": 15034,
      "ATK": 13485,
      "DEF": 12532,
      "WIS": 10441,
      "AGI": 17147
    },
    "skills": [
      641
    ],
    "img": "112",
    "rarity": 4,
    "evo": 2,
    "fullName": "Hash, Lizardman Cannoneer II"
  },
  "11429": {
    "name": "Maisie",
    "stats": {
      "HP": 19194,
      "ATK": 19097,
      "DEF": 16258,
      "WIS": 8101,
      "AGI": 17905
    },
    "skills": [
      599,
      600
    ],
    "autoAttack": 10037,
    "img": "1da",
    "rarity": 5,
    "evo": 2,
    "fullName": "Maisie, Grimoire Keeper II"
  },
  "11435": {
    "name": "Figgo",
    "stats": {
      "HP": 15509,
      "ATK": 16377,
      "DEF": 13451,
      "WIS": 6051,
      "AGI": 16534
    },
    "skills": [
      614
    ],
    "img": "275",
    "rarity": 4,
    "evo": 4,
    "fullName": "Figgo, Executioner II"
  },
  "11436": {
    "name": "Alyssa",
    "stats": {
      "HP": 17883,
      "ATK": 8718,
      "DEF": 16594,
      "WIS": 20516,
      "AGI": 17786
    },
    "skills": [
      616,
      617
    ],
    "autoAttack": 10007,
    "img": "41d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Alyssa, Black Cat Witch II"
  },
  "11437": {
    "name": "Pumpkin",
    "stats": {
      "HP": 16497,
      "ATK": 7061,
      "DEF": 12423,
      "WIS": 17060,
      "AGI": 15457
    },
    "skills": [
      618,
      619
    ],
    "autoAttack": 10007,
    "img": "46b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Clockwork Pumpkin II"
  },
  "11440": {
    "name": "Lucan",
    "stats": {
      "HP": 25304,
      "ATK": 22011,
      "DEF": 18349,
      "WIS": 17916,
      "AGI": 18154
    },
    "skills": [
      634
    ],
    "autoAttack": 10049,
    "img": "419",
    "rarity": 6,
    "evo": 2,
    "fullName": "Lucan, Eagle Knight II"
  },
  "11441": {
    "name": "Ausra",
    "stats": {
      "HP": 21913,
      "ATK": 9596,
      "DEF": 15998,
      "WIS": 18403,
      "AGI": 18154
    },
    "skills": [
      638,
      639
    ],
    "autoAttack": 10023,
    "img": "2c8",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ausra, the Fall Breeze II"
  },
  "11443": {
    "name": "Zorg",
    "stats": {
      "HP": 14073,
      "ATK": 15196,
      "DEF": 11331,
      "WIS": 5395,
      "AGI": 10805
    },
    "skills": [
      629
    ],
    "img": "1e0",
    "rarity": 4,
    "evo": 4,
    "fullName": "Zorg, the Cruncher II"
  },
  "11446": {
    "name": "Olan",
    "stats": {
      "HP": 16497,
      "ATK": 14048,
      "DEF": 14113,
      "WIS": 6779,
      "AGI": 17255
    },
    "skills": [
      610,
      611
    ],
    "img": "36b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Olan, Tricky Succubus II"
  },
  "11448": {
    "name": "Jack Rusty",
    "stats": {
      "HP": 17021,
      "ATK": 16123,
      "DEF": 10148,
      "WIS": 9539,
      "AGI": 15121
    },
    "skills": [
      609
    ],
    "autoAttack": 10044,
    "img": "46a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Jack, the Rusty II"
  },
  "11449": {
    "name": "Camazo",
    "stats": {
      "HP": 22628,
      "ATK": 22585,
      "DEF": 22173,
      "WIS": 16139,
      "AGI": 18208
    },
    "skills": [
      601,
      445
    ],
    "autoAttack": 10038,
    "img": "26c",
    "rarity": 6,
    "evo": 2,
    "fullName": "Camazo, Knight of Bats II"
  },
  "11450": {
    "name": "Elsa",
    "stats": {
      "HP": 19010,
      "ATK": 19021,
      "DEF": 15132,
      "WIS": 10018,
      "AGI": 17851
    },
    "skills": [
      602
    ],
    "autoAttack": 10039,
    "img": "2fe",
    "rarity": 5,
    "evo": 2,
    "fullName": "Elsa, Undead Bride II"
  },
  "11451": {
    "name": "Hatshepsut",
    "stats": {
      "HP": 17049,
      "ATK": 16334,
      "DEF": 13041,
      "WIS": 6097,
      "AGI": 16096
    },
    "skills": [
      603
    ],
    "autoAttack": 10040,
    "img": "2bd",
    "rarity": 4,
    "evo": 2,
    "fullName": "Hatshepsut, Mummy Queen II"
  },
  "11453": {
    "name": "GCE",
    "stats": {
      "HP": 15100,
      "ATK": 7564,
      "DEF": 11403,
      "WIS": 17254,
      "AGI": 16609
    },
    "skills": [
      604
    ],
    "autoAttack": 10007,
    "img": "333",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ghost Carriage Express II"
  },
  "11454": {
    "name": "Bella",
    "stats": {
      "HP": 16009,
      "ATK": 16627,
      "DEF": 13052,
      "WIS": 5631,
      "AGI": 17374
    },
    "skills": [
      643,
      644
    ],
    "img": "314",
    "rarity": 4,
    "evo": 2,
    "fullName": "Bella, the Dazzling Flower II"
  },
  "11455": {
    "name": "Skeleton King",
    "stats": {
      "HP": 19714,
      "ATK": 19064,
      "DEF": 20982,
      "WIS": 6097,
      "AGI": 18143
    },
    "skills": [
      605,
      606
    ],
    "autoAttack": 10041,
    "img": "3b5",
    "rarity": 5,
    "evo": 2,
    "fullName": "Skeleton King II"
  },
  "11456": {
    "name": "Chimaera",
    "stats": {
      "HP": 19519,
      "ATK": 9986,
      "DEF": 16009,
      "WIS": 17038,
      "AGI": 18013
    },
    "skills": [
      612,
      134
    ],
    "autoAttack": 10043,
    "img": "4a7",
    "rarity": 5,
    "evo": 2,
    "fullName": "Maleficent Chimaera II"
  },
  "11457": {
    "name": "Asena",
    "stats": {
      "HP": 15121,
      "ATK": 17385,
      "DEF": 11622,
      "WIS": 7505,
      "AGI": 16995
    },
    "skills": [
      608
    ],
    "img": "3f8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Asena, Wolfwoman II"
  },
  "11458": {
    "name": "Odoa",
    "stats": {
      "HP": 20364,
      "ATK": 24600,
      "DEF": 16009,
      "WIS": 10040,
      "AGI": 9520
    },
    "skills": [
      645,
      646
    ],
    "img": "1a6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Odoa, the Scarecrow II"
  },
  "11461": {
    "name": "Wang Yi",
    "stats": {
      "HP": 16024,
      "ATK": 6577,
      "DEF": 11855,
      "WIS": 17000,
      "AGI": 16816
    },
    "skills": [
      621,
      622
    ],
    "autoAttack": 10007,
    "img": "1b8",
    "rarity": 4,
    "evo": 4,
    "fullName": "Wang Yi, Lady of Iron II"
  },
  "11465": {
    "name": "Okypete",
    "stats": {
      "HP": 15610,
      "ATK": 13331,
      "DEF": 15158,
      "WIS": 6967,
      "AGI": 16840
    },
    "skills": [
      648
    ],
    "autoAttack": 10051,
    "img": "39d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Okypete, the Swiftwing II"
  },
  "11466": {
    "name": "Calais",
    "stats": {
      "HP": 19812,
      "ATK": 18100,
      "DEF": 16009,
      "WIS": 12792,
      "AGI": 17277
    },
    "skills": [
      650,
      651
    ],
    "img": "379",
    "rarity": 5,
    "evo": 2,
    "fullName": "Calais, the Gale II"
  },
  "11467": {
    "name": "Atalanta",
    "stats": {
      "HP": 16497,
      "ATK": 16302,
      "DEF": 13561,
      "WIS": 7776,
      "AGI": 15576
    },
    "skills": [
      652,
      653
    ],
    "img": "210",
    "rarity": 4,
    "evo": 2,
    "fullName": "Atalanta, Fowler II"
  },
  "11469": {
    "name": "Phineus",
    "stats": {
      "HP": 13597,
      "ATK": 7005,
      "DEF": 9894,
      "WIS": 14561,
      "AGI": 10915
    },
    "skills": [
      654
    ],
    "autoAttack": 10007,
    "img": "37a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Phineus, the Augur King II"
  },
  "11470": {
    "name": "Sedna",
    "stats": {
      "HP": 20321,
      "ATK": 17840,
      "DEF": 19129,
      "WIS": 7072,
      "AGI": 17699
    },
    "skills": [
      657,
      658
    ],
    "autoAttack": 10033,
    "img": "18d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Sedna, the Frozen Sea II"
  },
  "11472": {
    "name": "Tulok",
    "stats": {
      "HP": 15498,
      "ATK": 15047,
      "DEF": 10807,
      "WIS": 5247,
      "AGI": 10198
    },
    "skills": [
      662
    ],
    "img": "3a7",
    "rarity": 4,
    "evo": 4,
    "fullName": "Tulok, Icebreaker II"
  },
  "11474": {
    "name": "Aipaloovik",
    "stats": {
      "HP": 15610,
      "ATK": 7991,
      "DEF": 11807,
      "WIS": 16534,
      "AGI": 15999
    },
    "skills": [
      659
    ],
    "autoAttack": 10007,
    "img": "389",
    "rarity": 4,
    "evo": 4,
    "fullName": "Aipaloovik, the Snowstorm II"
  },
  "11478": {
    "name": "Hecatoncheir",
    "stats": {
      "HP": 15509,
      "ATK": 15158,
      "DEF": 14024,
      "WIS": 8759,
      "AGI": 15706
    },
    "skills": [
      676
    ],
    "img": "2e5",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hecatoncheir Rimetouch II"
  },
  "11479": {
    "name": "Jed",
    "stats": {
      "HP": 24080,
      "ATK": 25066,
      "DEF": 20494,
      "WIS": 14005,
      "AGI": 18100
    },
    "skills": [
      667
    ],
    "autoAttack": 10053,
    "img": "1b7",
    "rarity": 6,
    "evo": 2,
    "fullName": "Captain Jed II"
  },
  "11480": {
    "name": "Snegurochka",
    "stats": {
      "HP": 20007,
      "ATK": 7895,
      "DEF": 16063,
      "WIS": 22000,
      "AGI": 18143
    },
    "skills": [
      672,
      673
    ],
    "autoAttack": 10057,
    "img": "306",
    "rarity": 5,
    "evo": 2,
    "fullName": "Snegurochka II"
  },
  "11483": {
    "name": "Tree Golem",
    "stats": {
      "HP": 17998,
      "ATK": 17106,
      "DEF": 17998,
      "WIS": 12001,
      "AGI": 2907
    },
    "skills": [
      671
    ],
    "autoAttack": 10056,
    "img": "14b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ancient Tree Golem II"
  },
  "11484": {
    "name": "Chione",
    "stats": {
      "HP": 16204,
      "ATK": 13008,
      "DEF": 13561,
      "WIS": 8502,
      "AGI": 17266
    },
    "skills": [
      663,
      664
    ],
    "img": "4d9",
    "rarity": 4,
    "evo": 2,
    "fullName": "Chione, Fallen Heroine II"
  },
  "11485": {
    "name": "Luot",
    "stats": {
      "HP": 18013,
      "ATK": 17992,
      "DEF": 17006,
      "WIS": 9997,
      "AGI": 18035
    },
    "skills": [
      668
    ],
    "autoAttack": 10054,
    "img": "2c3",
    "rarity": 5,
    "evo": 2,
    "fullName": "Luot, Scout II"
  },
  "11486": {
    "name": "Qing Nu",
    "stats": {
      "HP": 19010,
      "ATK": 8957,
      "DEF": 15002,
      "WIS": 19541,
      "AGI": 17992
    },
    "skills": [
      677,
      678
    ],
    "autoAttack": 10007,
    "img": "14f",
    "rarity": 5,
    "evo": 2,
    "fullName": "Qing Nu, Snowweaver II"
  },
  "11488": {
    "name": "Aspidochelone",
    "stats": {
      "HP": 21003,
      "ATK": 17103,
      "DEF": 21003,
      "WIS": 17006,
      "AGI": 4450
    },
    "skills": [
      665,
      666
    ],
    "autoAttack": 10050,
    "img": "26f",
    "rarity": 5,
    "evo": 2,
    "fullName": "Aspidochelone, the Iceberg II"
  },
  "11493": {
    "name": "Hati",
    "stats": {
      "HP": 15002,
      "ATK": 8144,
      "DEF": 10777,
      "WIS": 17721,
      "AGI": 16995
    },
    "skills": [
      675
    ],
    "autoAttack": 10059,
    "img": "230",
    "rarity": 4,
    "evo": 2,
    "fullName": "Hati, Icetail Wolf II"
  },
  "11494": {
    "name": "Bergel",
    "stats": {
      "HP": 16529,
      "ATK": 7321,
      "DEF": 10538,
      "WIS": 17797,
      "AGI": 16811
    },
    "skills": [
      679,
      680
    ],
    "autoAttack": 10007,
    "img": "297",
    "rarity": 4,
    "evo": 2,
    "fullName": "Bergel, Frost Magus II"
  },
  "11501": {
    "name": "Agathos",
    "stats": {
      "HP": 15265,
      "ATK": 7478,
      "DEF": 11442,
      "WIS": 16803,
      "AGI": 16913
    },
    "skills": [
      682
    ],
    "autoAttack": 10036,
    "img": "38e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Agathos, Wyrm of the Harvest II"
  },
  "11502": {
    "name": "Kokopelli",
    "stats": {
      "HP": 19584,
      "ATK": 18858,
      "DEF": 13799,
      "WIS": 11102,
      "AGI": 18187
    },
    "skills": [
      684,
      685
    ],
    "isMounted": true,
    "img": "210",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kokopelli Mana II"
  },
  "11503": {
    "name": "Fenghuang",
    "stats": {
      "HP": 15218,
      "ATK": 7494,
      "DEF": 12261,
      "WIS": 17190,
      "AGI": 16345
    },
    "skills": [
      686,
      687
    ],
    "autoAttack": 10019,
    "img": "1ce",
    "rarity": 4,
    "evo": 2,
    "fullName": "Fenghuang, Bird Divine II"
  },
  "11505": {
    "name": "Bert",
    "stats": {
      "HP": 14107,
      "ATK": 14000,
      "DEF": 11828,
      "WIS": 6577,
      "AGI": 9453
    },
    "skills": [
      688
    ],
    "img": "404",
    "rarity": 4,
    "evo": 4,
    "fullName": "Bert, Foe Sweep II"
  },
  "11506": {
    "name": "Lachesis",
    "stats": {
      "HP": 17992,
      "ATK": 9596,
      "DEF": 15002,
      "WIS": 19205,
      "AGI": 17753
    },
    "skills": [
      689,
      690
    ],
    "autoAttack": 10003,
    "img": "2ba",
    "rarity": 5,
    "evo": 2,
    "fullName": "Lachesis, the Measurer II"
  },
  "11508": {
    "name": "Pixiu",
    "stats": {
      "HP": 15706,
      "ATK": 15999,
      "DEF": 12999,
      "WIS": 8005,
      "AGI": 16489
    },
    "skills": [
      691,
      701
    ],
    "autoAttack": 10060,
    "img": "443",
    "rarity": 4,
    "evo": 4,
    "fullName": "Pixiu, the Wealthy II"
  },
  "11510": {
    "name": "Botis",
    "stats": {
      "HP": 14096,
      "ATK": 14000,
      "DEF": 10001,
      "WIS": 5506,
      "AGI": 13196
    },
    "skills": [
      694
    ],
    "img": "2e7",
    "rarity": 4,
    "evo": 4,
    "fullName": "Botis, Earl of Hell II"
  },
  "11512": {
    "name": "Deimos",
    "stats": {
      "HP": 16497,
      "ATK": 17753,
      "DEF": 11188,
      "WIS": 6996,
      "AGI": 17363
    },
    "skills": [
      695,
      696
    ],
    "autoAttack": 10061,
    "img": "1b8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Deimos, Terror Spear II"
  },
  "11514": {
    "name": "Eros",
    "stats": {
      "HP": 15438,
      "ATK": 16292,
      "DEF": 14486,
      "WIS": 6284,
      "AGI": 16668
    },
    "skills": [
      801
    ],
    "img": "38c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Eros, the Golden Arrow II"
  },
  "11516": {
    "name": "Kotyangwuti",
    "stats": {
      "HP": 18512,
      "ATK": 9509,
      "DEF": 15023,
      "WIS": 20028,
      "AGI": 17992
    },
    "skills": [
      703
    ],
    "autoAttack": 10063,
    "img": "3ce",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kotyangwuti, Spider Spirit II"
  },
  "11517": {
    "name": "Etain",
    "stats": {
      "HP": 15511,
      "ATK": 7873,
      "DEF": 11015,
      "WIS": 17038,
      "AGI": 17201
    },
    "skills": [
      704
    ],
    "autoAttack": 10064,
    "img": "147",
    "rarity": 4,
    "evo": 2,
    "fullName": "Etain, Butterfly Tamer II"
  },
  "11519": {
    "name": "Koroku",
    "stats": {
      "HP": 15341,
      "ATK": 16561,
      "DEF": 13013,
      "WIS": 7492,
      "AGI": 16853
    },
    "skills": [
      705
    ],
    "autoAttack": 10065,
    "img": "11f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Koroku, the Death Stinger II"
  },
  "11520": {
    "name": "Oliver",
    "stats": {
      "HP": 15912,
      "ATK": 14980,
      "DEF": 13702,
      "WIS": 8014,
      "AGI": 17266
    },
    "skills": [
      800
    ],
    "autoAttack": 10067,
    "img": "3e5",
    "rarity": 4,
    "evo": 2,
    "fullName": "Sir Oliver, the Golden Sword II"
  },
  "11521": {
    "name": "Wrath",
    "stats": {
      "HP": 19010,
      "ATK": 21101,
      "DEF": 16410,
      "WIS": 11936,
      "AGI": 18154
    },
    "skills": [
      706,
      707
    ],
    "img": "279",
    "rarity": 5,
    "evo": 2,
    "fullName": "Wrath, Beast of Sin II"
  },
  "11522": {
    "name": "Briar",
    "stats": {
      "HP": 18988,
      "ATK": 9000,
      "DEF": 20028,
      "WIS": 19519,
      "AGI": 12987
    },
    "skills": [
      804,
      805
    ],
    "autoAttack": 10007,
    "img": "36b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Briar, Grimoire Keeper II"
  },
  "11523": {
    "name": "Jason",
    "stats": {
      "HP": 15348,
      "ATK": 18024,
      "DEF": 11015,
      "WIS": 8978,
      "AGI": 16876
    },
    "skills": [
      802,
      803
    ],
    "img": "4c7",
    "rarity": 4,
    "evo": 2,
    "fullName": "Jason, Fallen Hero II"
  },
  "11524": {
    "name": "Kokuanten",
    "stats": {
      "HP": 18999,
      "ATK": 7050,
      "DEF": 19996,
      "WIS": 20505,
      "AGI": 13994
    },
    "skills": [
      697,
      698
    ],
    "autoAttack": 10003,
    "img": "2ad",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kokuanten, the Ominous II"
  },
  "11526": {
    "name": "Yae",
    "stats": {
      "HP": 15317,
      "ATK": 7271,
      "DEF": 13258,
      "WIS": 15365,
      "AGI": 17133
    },
    "skills": [
      699,
      700
    ],
    "autoAttack": 10007,
    "img": "2a6",
    "rarity": 4,
    "evo": 4,
    "fullName": "Yae, the Night Flower II"
  },
  "11531": {
    "name": "Onra",
    "stats": {
      "HP": 15719,
      "ATK": 16416,
      "DEF": 15147,
      "WIS": 6710,
      "AGI": 15147
    },
    "skills": [
      807
    ],
    "img": "155",
    "rarity": 4,
    "evo": 4,
    "fullName": "Onra, Ogre Lord II"
  },
  "11532": {
    "name": "Kibitsuhiko",
    "stats": {
      "HP": 19335,
      "ATK": 18642,
      "DEF": 16562,
      "WIS": 11448,
      "AGI": 17981
    },
    "skills": [
      809,
      810
    ],
    "img": "38d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kibitsuhiko, Ogre Slayer II"
  },
  "11533": {
    "name": "Momoso",
    "stats": {
      "HP": 15034,
      "ATK": 16973,
      "DEF": 11925,
      "WIS": 9997,
      "AGI": 15836
    },
    "skills": [
      811,
      812
    ],
    "img": "39b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Momoso, Pheasant Tamer II"
  },
  "11535": {
    "name": "Macaca",
    "stats": {
      "HP": 13962,
      "ATK": 13671,
      "DEF": 12025,
      "WIS": 6368,
      "AGI": 9453
    },
    "skills": [
      813
    ],
    "img": "2a6",
    "rarity": 4,
    "evo": 4,
    "fullName": "Macaca, the Headlong II"
  },
  "11536": {
    "name": "Juno",
    "stats": {
      "HP": 19552,
      "ATK": 18501,
      "DEF": 17006,
      "WIS": 17992,
      "AGI": 9000
    },
    "skills": [
      817,
      818
    ],
    "img": "489",
    "rarity": 5,
    "evo": 2,
    "fullName": "Juno, Goddess of Affection II"
  },
  "11538": {
    "name": "Brangane",
    "stats": {
      "HP": 14610,
      "ATK": 7639,
      "DEF": 12001,
      "WIS": 17899,
      "AGI": 15804
    },
    "skills": [
      819
    ],
    "autoAttack": 10003,
    "img": "2de",
    "rarity": 4,
    "evo": 4,
    "fullName": "Brangane, the Enchanting II"
  },
  "11540": {
    "name": "Houdi",
    "stats": {
      "HP": 13293,
      "ATK": 6006,
      "DEF": 10001,
      "WIS": 15498,
      "AGI": 12001
    },
    "skills": [
      822
    ],
    "autoAttack": 10003,
    "img": "14b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Houdi, the Illusory Flame II"
  },
  "11541": {
    "name": "Aengus",
    "stats": {
      "HP": 15803,
      "ATK": 6996,
      "DEF": 12239,
      "WIS": 17006,
      "AGI": 16497
    },
    "skills": [
      823,
      824
    ],
    "autoAttack": 10003,
    "img": "453",
    "rarity": 4,
    "evo": 2,
    "fullName": "Aengus, the Charitable II"
  },
  "11543": {
    "name": "He Qiong",
    "stats": {
      "HP": 24253,
      "ATK": 14243,
      "DEF": 22206,
      "WIS": 23051,
      "AGI": 17992
    },
    "skills": [
      827
    ],
    "autoAttack": 10007,
    "img": "359",
    "rarity": 6,
    "evo": 2,
    "fullName": "He Qiong, the Transcendent II"
  },
  "11544": {
    "name": "Karna",
    "stats": {
      "HP": 19324,
      "ATK": 20310,
      "DEF": 15478,
      "WIS": 11004,
      "AGI": 17461
    },
    "skills": [
      828
    ],
    "autoAttack": 10103,
    "img": "365",
    "rarity": 5,
    "evo": 2,
    "fullName": "Karna, the Red Eye II"
  },
  "11545": {
    "name": "Chi-Hu",
    "stats": {
      "HP": 16529,
      "ATK": 17071,
      "DEF": 13106,
      "WIS": 7440,
      "AGI": 15738
    },
    "skills": [
      829
    ],
    "autoAttack": 10104,
    "img": "381",
    "rarity": 4,
    "evo": 2,
    "fullName": "Chi-Hu II"
  },
  "11547": {
    "name": "Barometz",
    "stats": {
      "HP": 16961,
      "ATK": 15121,
      "DEF": 14145,
      "WIS": 14000,
      "AGI": 9052
    },
    "skills": [
      830
    ],
    "autoAttack": 10105,
    "img": "184",
    "rarity": 4,
    "evo": 4,
    "fullName": "Caparisoned Barometz II"
  },
  "11548": {
    "name": "Zepar",
    "stats": {
      "HP": 24557,
      "ATK": 23029,
      "DEF": 20050,
      "WIS": 18111,
      "AGI": 18533
    },
    "skills": [
      831,
      832
    ],
    "passiveSkills": [
      9001
    ],
    "img": "1ba",
    "rarity": 6,
    "evo": 2,
    "fullName": "Zepar, Blood-Annointed II"
  },
  "11550": {
    "name": "Medea",
    "stats": {
      "HP": 17493,
      "ATK": 18598,
      "DEF": 17493,
      "WIS": 10300,
      "AGI": 5999
    },
    "skills": [
      834
    ],
    "autoAttack": 10107,
    "img": "37b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Medea, Vengeful Queen II"
  },
  "11552": {
    "name": "Druj",
    "stats": {
      "HP": 15024,
      "ATK": 12999,
      "DEF": 15647,
      "WIS": 7005,
      "AGI": 17241
    },
    "skills": [
      835
    ],
    "img": "460",
    "rarity": 4,
    "evo": 4,
    "fullName": "Druj Nasu, the Impure II"
  },
  "11553": {
    "name": "Rapunzel",
    "stats": {
      "HP": 18349,
      "ATK": 20223,
      "DEF": 17428,
      "WIS": 8805,
      "AGI": 18208
    },
    "skills": [
      836,
      837
    ],
    "autoAttack": 10108,
    "img": "391",
    "rarity": 5,
    "evo": 2,
    "fullName": "Rapunzel, Grimoire Keeper II"
  },
  "11554": {
    "name": "Pandora",
    "stats": {
      "HP": 15023,
      "ATK": 7028,
      "DEF": 13528,
      "WIS": 16887,
      "AGI": 16529
    },
    "skills": [
      838,
      839
    ],
    "autoAttack": 10003,
    "img": "12a",
    "rarity": 4,
    "evo": 2,
    "fullName": "Pandora, Fallen Heroine II"
  },
  "11557": {
    "name": "Peony",
    "stats": {
      "HP": 17298,
      "ATK": 17797,
      "DEF": 12250,
      "WIS": 7505,
      "AGI": 16399
    },
    "skills": [
      820,
      821
    ],
    "autoAttack": 10101,
    "img": "394",
    "rarity": 4,
    "evo": 2,
    "fullName": "Peony, the Jiang Shi II"
  },
  "11560": {
    "name": "Bheara",
    "stats": {
      "HP": 14975,
      "ATK": 6502,
      "DEF": 12207,
      "WIS": 17475,
      "AGI": 16754
    },
    "skills": [
      842
    ],
    "autoAttack": 10007,
    "img": "387",
    "rarity": 4,
    "evo": 4,
    "fullName": "Bheara, Wastestrider II"
  },
  "11561": {
    "name": "Persephone",
    "stats": {
      "HP": 18793,
      "ATK": 8686,
      "DEF": 13929,
      "WIS": 21957,
      "AGI": 18154
    },
    "skills": [
      844,
      845
    ],
    "autoAttack": 10007,
    "img": "4b1",
    "rarity": 5,
    "evo": 2,
    "fullName": "Persephone, Spring Goddess II"
  },
  "11562": {
    "name": "Oka",
    "stats": {
      "HP": 16042,
      "ATK": 5122,
      "DEF": 12120,
      "WIS": 17959,
      "AGI": 17244
    },
    "skills": [
      846,
      847
    ],
    "autoAttack": 10007,
    "img": "275",
    "rarity": 4,
    "evo": 2,
    "fullName": "Oka, Kunoichi II"
  },
  "11564": {
    "name": "Ninurta",
    "stats": {
      "HP": 13465,
      "ATK": 6954,
      "DEF": 10332,
      "WIS": 14120,
      "AGI": 11077
    },
    "skills": [
      848
    ],
    "autoAttack": 10007,
    "img": "292",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ninurta, the Thunderclap II"
  },
  "11565": {
    "name": "Sigurd",
    "stats": {
      "HP": 19996,
      "ATK": 19053,
      "DEF": 14005,
      "WIS": 11004,
      "AGI": 17992
    },
    "skills": [
      864,
      865
    ],
    "img": "25e",
    "rarity": 5,
    "evo": 2,
    "fullName": "Sigurd, Dragonslayer II"
  },
  "11567": {
    "name": "Phlox",
    "stats": {
      "HP": 15047,
      "ATK": 5298,
      "DEF": 13489,
      "WIS": 17499,
      "AGI": 16607
    },
    "skills": [
      863
    ],
    "autoAttack": 10003,
    "img": "23d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Phlox, Avern Witch II"
  },
  "11569": {
    "name": "Icemelt",
    "stats": {
      "HP": 11794,
      "ATK": 8101,
      "DEF": 8502,
      "WIS": 14402,
      "AGI": 14000
    },
    "skills": [
      858
    ],
    "img": "408",
    "rarity": 4,
    "evo": 4,
    "fullName": "Icemelt Dragon II"
  },
  "11570": {
    "name": "War Bear",
    "stats": {
      "HP": 18999,
      "ATK": 17504,
      "DEF": 15500,
      "WIS": 11492,
      "AGI": 6292
    },
    "skills": [
      859,
      860
    ],
    "img": "1b8",
    "rarity": 4,
    "evo": 2,
    "fullName": "War Bear II"
  },
  "11572": {
    "name": "Banshee",
    "stats": {
      "HP": 23560,
      "ATK": 16009,
      "DEF": 21480,
      "WIS": 24708,
      "AGI": 18533
    },
    "skills": [
      872,
      873
    ],
    "passiveSkills": [
      9002
    ],
    "autoAttack": 10114,
    "img": "42d",
    "rarity": 6,
    "evo": 2,
    "fullName": "Banshee Rider II"
  },
  "11574": {
    "name": "Dryad",
    "stats": {
      "HP": 15186,
      "ATK": 17060,
      "DEF": 14449,
      "WIS": 6487,
      "AGI": 16670
    },
    "skills": [
      870
    ],
    "autoAttack": 10103,
    "img": "3c2",
    "rarity": 4,
    "evo": 2,
    "fullName": "Dryad Archer II"
  },
  "11576": {
    "name": "Lubberkin",
    "stats": {
      "HP": 15793,
      "ATK": 12965,
      "DEF": 16144,
      "WIS": 6078,
      "AGI": 17060
    },
    "skills": [
      871
    ],
    "autoAttack": 10061,
    "img": "1fc",
    "rarity": 4,
    "evo": 4,
    "fullName": "Lubberkin, Four Leaf Clover II"
  },
  "11577": {
    "name": "Emerald",
    "stats": {
      "HP": 25012,
      "ATK": 24383,
      "DEF": 21881,
      "WIS": 12272,
      "AGI": 18208
    },
    "skills": [
      868
    ],
    "autoAttack": 10112,
    "img": "342",
    "rarity": 6,
    "evo": 2,
    "fullName": "Emerald Dragon II"
  },
  "11579": {
    "name": "Gryla",
    "stats": {
      "HP": 17049,
      "ATK": 4363,
      "DEF": 15489,
      "WIS": 16594,
      "AGI": 15500
    },
    "skills": [
      879,
      880
    ],
    "autoAttack": 10007,
    "isMounted": true,
    "img": "3a9",
    "rarity": 4,
    "evo": 2,
    "fullName": "Gryla, Swap II"
  },
  "11581": {
    "name": "Adara",
    "stats": {
      "HP": 14424,
      "ATK": 17805,
      "DEF": 13013,
      "WIS": 7005,
      "AGI": 16902
    },
    "skills": [
      876
    ],
    "autoAttack": 10117,
    "img": "205",
    "rarity": 4,
    "evo": 4,
    "fullName": "Adara Luck Shot, Swap II"
  },
  "11582": {
    "name": "Vlad",
    "stats": {
      "HP": 18934,
      "ATK": 8491,
      "DEF": 15240,
      "WIS": 19812,
      "AGI": 18024
    },
    "skills": [
      877,
      878
    ],
    "autoAttack": 10007,
    "img": "187",
    "rarity": 5,
    "evo": 2,
    "fullName": "Vlad, Swap II"
  },
  "11583": {
    "name": "Kalevan",
    "stats": {
      "HP": 15153,
      "ATK": 15803,
      "DEF": 14222,
      "WIS": 6855,
      "AGI": 17006
    },
    "skills": [
      947,
      948
    ],
    "autoAttack": 10051,
    "img": "187",
    "rarity": 4,
    "evo": 2,
    "fullName": "Kalevan, Swap II"
  },
  "11590": {
    "name": "Lenore",
    "stats": {
      "HP": 15903,
      "ATK": 12280,
      "DEF": 13745,
      "WIS": 8709,
      "AGI": 17292
    },
    "skills": [
      883
    ],
    "autoAttack": 10061,
    "img": "1e6",
    "rarity": 4,
    "evo": 4,
    "fullName": "Lenore, the Sly Fox II"
  },
  "11591": {
    "name": "Aletheia",
    "stats": {
      "HP": 18674,
      "ATK": 19010,
      "DEF": 17277,
      "WIS": 10939,
      "AGI": 18165
    },
    "skills": [
      885,
      886
    ],
    "img": "2be",
    "rarity": 5,
    "evo": 2,
    "fullName": "Aletheia, Knight Templar II"
  },
  "11592": {
    "name": "Andromalius",
    "stats": {
      "HP": 15749,
      "ATK": 16172,
      "DEF": 12564,
      "WIS": 7960,
      "AGI": 17277
    },
    "skills": [
      887,
      888
    ],
    "img": "2b6",
    "rarity": 4,
    "evo": 2,
    "fullName": "Andromalius, Eater of Lies II"
  },
  "11594": {
    "name": "Isegrim",
    "stats": {
      "HP": 12573,
      "ATK": 12928,
      "DEF": 12049,
      "WIS": 8491,
      "AGI": 9515
    },
    "skills": [
      889
    ],
    "img": "31d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Isegrim, the Lone Wolf II"
  },
  "11595": {
    "name": "Blazing",
    "stats": {
      "HP": 18891,
      "ATK": 9141,
      "DEF": 14005,
      "WIS": 19963,
      "AGI": 17992
    },
    "skills": [
      893,
      894
    ],
    "autoAttack": 10019,
    "img": "1c6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Blazing Drake"
  },
  "11597": {
    "name": "Telluric",
    "stats": {
      "HP": 16696,
      "ATK": 17499,
      "DEF": 17693,
      "WIS": 11770,
      "AGI": 5506
    },
    "skills": [
      895
    ],
    "autoAttack": 10122,
    "img": "3d5",
    "rarity": 4,
    "evo": 4,
    "fullName": "Telluric Drake II"
  },
  "11599": {
    "name": "Mammi EP4",
    "stats": {
      "HP": 13293,
      "ATK": 7699,
      "DEF": 8505,
      "WIS": 14806,
      "AGI": 12500
    },
    "skills": [
      892
    ],
    "autoAttack": 10016,
    "img": "46a",
    "rarity": 4,
    "evo": 4,
    "fullName": "Mammi, Spiritmancer II"
  },
  "11600": {
    "name": "Feathered",
    "stats": {
      "HP": 17006,
      "ATK": 13008,
      "DEF": 15998,
      "WIS": 6248,
      "AGI": 17992
    },
    "skills": [
      896,
      897
    ],
    "autoAttack": 10120,
    "img": "25d",
    "rarity": 4,
    "evo": 2,
    "fullName": "Feathered Drake"
  },
  "11601": {
    "name": "Brine",
    "stats": {
      "HP": 18501,
      "ATK": 6898,
      "DEF": 16009,
      "WIS": 22000,
      "AGI": 17591
    },
    "skills": [
      900,
      901
    ],
    "autoAttack": 10121,
    "img": "217",
    "rarity": 5,
    "evo": 2,
    "fullName": "Brine Drake"
  },
  "11602": {
    "name": "Lancelot",
    "stats": {
      "HP": 23127,
      "ATK": 25098,
      "DEF": 20093,
      "WIS": 17461,
      "AGI": 18533
    },
    "skills": [
      909,
      910
    ],
    "passiveSkills": [
      9003
    ],
    "autoAttack": 10108,
    "img": "28e",
    "rarity": 6,
    "evo": 2,
    "fullName": "Lancelot of the Lake II"
  },
  "11603": {
    "name": "Nimue",
    "stats": {
      "HP": 19086,
      "ATK": 8599,
      "DEF": 14384,
      "WIS": 20982,
      "AGI": 17992
    },
    "skills": [
      906
    ],
    "autoAttack": 10124,
    "img": "2c6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Nimue, Lady of the Lake II"
  },
  "11604": {
    "name": "Hellawes",
    "stats": {
      "HP": 15327,
      "ATK": 7559,
      "DEF": 12272,
      "WIS": 16659,
      "AGI": 16800
    },
    "skills": [
      907
    ],
    "autoAttack": 10007,
    "img": "431",
    "rarity": 4,
    "evo": 2,
    "fullName": "Hellawes, Fetter Witch II"
  },
  "11606": {
    "name": "Palamedes",
    "stats": {
      "HP": 15376,
      "ATK": 16217,
      "DEF": 14561,
      "WIS": 6650,
      "AGI": 17071
    },
    "skills": [
      908
    ],
    "autoAttack": 10103,
    "img": "21e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Palamedes, the Hawk's Eye II"
  },
  "11607": {
    "name": "Shackled Red Wyrm",
    "stats": {
      "HP": 25521,
      "ATK": 14092,
      "DEF": 20386,
      "WIS": 23538,
      "AGI": 18219
    },
    "skills": [
      905
    ],
    "autoAttack": 10123,
    "img": "281",
    "rarity": 6,
    "evo": 2,
    "fullName": "Shackled Red Wyrm II"
  },
  "11611": {
    "name": "Chariot Hippocamp",
    "stats": {
      "HP": 14402,
      "ATK": 14792,
      "DEF": 13024,
      "WIS": 7980,
      "AGI": 17706
    },
    "skills": [
      913
    ],
    "img": "281",
    "rarity": 4,
    "evo": 4,
    "fullName": "Chariot Hippocamp II"
  },
  "11612": {
    "name": "Belle",
    "stats": {
      "HP": 16009,
      "ATK": 17006,
      "DEF": 14980,
      "WIS": 14807,
      "AGI": 18208
    },
    "skills": [
      914,
      915
    ],
    "autoAttack": 10125,
    "isMounted": true,
    "img": "31f",
    "rarity": 5,
    "evo": 2,
    "fullName": "Belle, Grimoire Keeper II"
  },
  "11613": {
    "name": "Amphitrite",
    "stats": {
      "HP": 16226,
      "ATK": 7418,
      "DEF": 19638,
      "WIS": 20158,
      "AGI": 17569
    },
    "skills": [
      916,
      917
    ],
    "autoAttack": 10001,
    "img": "2ed",
    "rarity": 5,
    "evo": 2,
    "fullName": "Amphitrite, Nereid Queen II"
  },
  "11621": {
    "name": "Sabnock",
    "stats": {
      "HP": 25261,
      "ATK": 24643,
      "DEF": 21112,
      "WIS": 14926,
      "AGI": 18544
    },
    "skills": [
      1123,
      1124
    ],
    "passiveSkills": [
      9008
    ],
    "autoAttack": 10061,
    "img": "14e",
    "rarity": 6,
    "evo": 2,
    "fullName": "Sabnock, Marquis of Hell II"
  },
  "11624": {
    "name": "Kapoonis",
    "stats": {
      "HP": 21935,
      "ATK": 11719,
      "DEF": 13160,
      "WIS": 16139,
      "AGI": 18295
    },
    "skills": [
      1280,
      1281
    ],
    "autoAttack": 10003,
    "img": "378",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kapoonis, Thunder Magus II"
  },
  "11627": {
    "name": "Charon",
    "stats": {
      "HP": 16681,
      "ATK": 6689,
      "DEF": 10525,
      "WIS": 17095,
      "AGI": 16950
    },
    "skills": [
      919
    ],
    "autoAttack": 10007,
    "img": "3ae",
    "rarity": 4,
    "evo": 4,
    "fullName": "Charon, Greedy Ferryman II"
  },
  "11628": {
    "name": "Beatrice",
    "stats": {
      "HP": 18858,
      "ATK": 7895,
      "DEF": 15251,
      "WIS": 21328,
      "AGI": 18165
    },
    "skills": [
      921,
      922
    ],
    "autoAttack": 10007,
    "img": "26a",
    "rarity": 5,
    "evo": 2,
    "fullName": "Beatrice, the Luminescent II"
  },
  "11629": {
    "name": "Midas",
    "stats": {
      "HP": 14048,
      "ATK": 7819,
      "DEF": 11275,
      "WIS": 18013,
      "AGI": 17374
    },
    "skills": [
      923,
      924
    ],
    "autoAttack": 10007,
    "img": "2a6",
    "rarity": 4,
    "evo": 2,
    "fullName": "Midas, the Wailing King II"
  },
  "11631": {
    "name": "Nessus",
    "stats": {
      "HP": 13803,
      "ATK": 7906,
      "DEF": 9635,
      "WIS": 13965,
      "AGI": 10245
    },
    "skills": [
      925
    ],
    "autoAttack": 10007,
    "img": "13e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Nessus, Centaur Gaoler II"
  },
  "11632": {
    "name": "Azazel",
    "stats": {
      "HP": 19010,
      "ATK": 17331,
      "DEF": 15002,
      "WIS": 11492,
      "AGI": 18165
    },
    "skills": [
      937,
      938
    ],
    "autoAttack": 10125,
    "img": "2ef",
    "rarity": 5,
    "evo": 2,
    "fullName": "Azazel, the Temptress II"
  },
  "11634": {
    "name": "Mammon",
    "stats": {
      "HP": 16010,
      "ATK": 7895,
      "DEF": 13010,
      "WIS": 14999,
      "AGI": 15999
    },
    "skills": [
      944
    ],
    "autoAttack": 10129,
    "img": "274",
    "rarity": 4,
    "evo": 4,
    "fullName": "Mammon, Raven Claw II"
  },
  "11636": {
    "name": "Moloch",
    "stats": {
      "HP": 12001,
      "ATK": 6602,
      "DEF": 10001,
      "WIS": 15207,
      "AGI": 12999
    },
    "skills": [
      941
    ],
    "autoAttack": 10016,
    "img": "356",
    "rarity": 4,
    "evo": 4,
    "fullName": "Moloch, the Infernal Axe II"
  },
  "11637": {
    "name": "Minos",
    "stats": {
      "HP": 15511,
      "ATK": 17244,
      "DEF": 15002,
      "WIS": 6292,
      "AGI": 16204
    },
    "skills": [
      939,
      940
    ],
    "img": "399",
    "rarity": 4,
    "evo": 2,
    "fullName": "Minos, Judgment King II"
  },
  "11638": {
    "name": "Pasiphae",
    "stats": {
      "HP": 18501,
      "ATK": 18999,
      "DEF": 15002,
      "WIS": 10192,
      "AGI": 17309
    },
    "skills": [
      945,
      946
    ],
    "autoAttack": 10125,
    "img": "3dc",
    "rarity": 5,
    "evo": 2,
    "fullName": "Pasiphae, the Brass Bull II"
  },
  "11639": {
    "name": "Fafnir",
    "stats": {
      "HP": 25012,
      "ATK": 23538,
      "DEF": 20754,
      "WIS": 14092,
      "AGI": 18349
    },
    "skills": [
      950
    ],
    "autoAttack": 10061,
    "img": "257",
    "rarity": 6,
    "evo": 2,
    "fullName": "Fafnir, Fireclad Dragon II"
  },
  "11640": {
    "name": "Waltraute",
    "stats": {
      "HP": 19552,
      "ATK": 18100,
      "DEF": 16854,
      "WIS": 8480,
      "AGI": 18046
    },
    "skills": [
      951
    ],
    "autoAttack": 10044,
    "img": "24d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Waltraute, Valiant Valkyrie II"
  },
  "11641": {
    "name": "Aslaug",
    "stats": {
      "HP": 15121,
      "ATK": 16486,
      "DEF": 13496,
      "WIS": 6389,
      "AGI": 17103
    },
    "skills": [
      952
    ],
    "autoAttack": 10130,
    "img": "2c8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Aslaug, the Lyre Bow II"
  },
  "11643": {
    "name": "Alberich",
    "stats": {
      "HP": 15964,
      "ATK": 17120,
      "DEF": 16523,
      "WIS": 15427,
      "AGI": 4237
    },
    "skills": [
      953
    ],
    "autoAttack": 10131,
    "img": "376",
    "rarity": 4,
    "evo": 4,
    "fullName": "Alberich, the Ceratophrys II"
  },
  "11644": {
    "name": "Nidhogg",
    "stats": {
      "HP": 24752,
      "ATK": 16128,
      "DEF": 22130,
      "WIS": 23246,
      "AGI": 18035
    },
    "skills": [
      935,
      936
    ],
    "passiveSkills": [
      9004
    ],
    "autoAttack": 10126,
    "img": "151",
    "rarity": 6,
    "evo": 2,
    "fullName": "Nidhogg, Iceclad Dragon II"
  },
  "11646": {
    "name": "Thoth",
    "stats": {
      "HP": 13117,
      "ATK": 8047,
      "DEF": 12694,
      "WIS": 17645,
      "AGI": 17190
    },
    "skills": [
      958
    ],
    "autoAttack": 10003,
    "img": "20b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Thoth, Hieroglypher II"
  },
  "11648": {
    "name": "Fleetfoot",
    "stats": {
      "HP": 13583,
      "ATK": 17924,
      "DEF": 11574,
      "WIS": 7688,
      "AGI": 17144
    },
    "skills": [
      959
    ],
    "autoAttack": 10133,
    "img": "165",
    "rarity": 4,
    "evo": 4,
    "fullName": "Fleetfoot Ornithomimus II"
  },
  "11649": {
    "name": "Pele",
    "stats": {
      "HP": 17840,
      "ATK": 19357,
      "DEF": 17017,
      "WIS": 11080,
      "AGI": 18208
    },
    "skills": [
      960,
      961
    ],
    "autoAttack": 10133,
    "img": "2f1",
    "rarity": 5,
    "evo": 2,
    "fullName": "Pele, Volcano Shamaness II"
  },
  "11650": {
    "name": "Rongo",
    "stats": {
      "HP": 18057,
      "ATK": 16800,
      "DEF": 17342,
      "WIS": 13312,
      "AGI": 17992
    },
    "skills": [
      962,
      963
    ],
    "autoAttack": 10135,
    "img": "198",
    "rarity": 5,
    "evo": 2,
    "fullName": "Rongo, Moai Master II"
  },
  "11653": {
    "name": "Acanthus",
    "stats": {
      "HP": 16616,
      "ATK": 6129,
      "DEF": 10419,
      "WIS": 17493,
      "AGI": 17894
    },
    "skills": [
      1270,
      1271
    ],
    "autoAttack": 10007,
    "img": "148",
    "rarity": 4,
    "evo": 2,
    "fullName": "Acanthus, the Gilded Thorn II"
  },
  "11654": {
    "name": "Ullr",
    "stats": {
      "HP": 19151,
      "ATK": 16692,
      "DEF": 17797,
      "WIS": 9260,
      "AGI": 18349
    },
    "skills": [
      1043,
      1044
    ],
    "autoAttack": 10152,
    "img": "1a6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Ullr, Starshooter II"
  },
  "11655": {
    "name": "Hermine",
    "stats": {
      "HP": 15121,
      "ATK": 4710,
      "DEF": 13138,
      "WIS": 18143,
      "AGI": 17992
    },
    "skills": [
      1203,
      1204
    ],
    "autoAttack": 10183,
    "img": "14f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Hermine, High Priestess II"
  },
  "11662": {
    "name": "Feng",
    "stats": {
      "HP": 16215,
      "ATK": 17569,
      "DEF": 13637,
      "WIS": 6227,
      "AGI": 16399
    },
    "skills": [
      1033
    ],
    "autoAttack": 10147,
    "img": "342",
    "rarity": 4,
    "evo": 2,
    "fullName": "Feng, Sanjiegun Master II"
  },
  "11664": {
    "name": "Wyvern Gemwarden",
    "stats": {
      "HP": 15164,
      "ATK": 5512,
      "DEF": 14027,
      "WIS": 16583,
      "AGI": 17461
    },
    "skills": [
      1039
    ],
    "autoAttack": 10149,
    "img": "229",
    "rarity": 4,
    "evo": 2,
    "fullName": "Wyvern Gemwarden II"
  },
  "11673": {
    "name": "Amethyst",
    "stats": {
      "HP": 20169,
      "ATK": 16291,
      "DEF": 13788,
      "WIS": 13030,
      "AGI": 18241
    },
    "skills": [
      967,
      968
    ],
    "autoAttack": 10108,
    "img": "4a6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Amethyst Dragon II"
  },
  "11674": {
    "name": "Agate",
    "stats": {
      "HP": 16497,
      "ATK": 14308,
      "DEF": 12077,
      "WIS": 7852,
      "AGI": 17764
    },
    "skills": [
      969,
      970
    ],
    "autoAttack": 10108,
    "img": "205",
    "rarity": 4,
    "evo": 2,
    "fullName": "Agate, Gem Tamer II"
  },
  "11676": {
    "name": "Fionn",
    "stats": {
      "HP": 12597,
      "ATK": 11514,
      "DEF": 10027,
      "WIS": 7819,
      "AGI": 13597
    },
    "skills": [
      971
    ],
    "img": "29f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Fionn, the Meteor Sword II"
  },
  "11677": {
    "name": "Susanoo",
    "stats": {
      "HP": 17797,
      "ATK": 19508,
      "DEF": 15500,
      "WIS": 11784,
      "AGI": 18013
    },
    "skills": [
      974,
      975
    ],
    "autoAttack": 10133,
    "img": "1ab",
    "rarity": 5,
    "evo": 2,
    "fullName": "Susanoo, Rowdy God II"
  },
  "11679": {
    "name": "Ame",
    "stats": {
      "HP": 16803,
      "ATK": 10001,
      "DEF": 14500,
      "WIS": 17499,
      "AGI": 9209
    },
    "skills": [
      976
    ],
    "autoAttack": 10003,
    "img": "152",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ame no Uzume, the Lure II"
  },
  "11681": {
    "name": "Mizuchi",
    "stats": {
      "HP": 13010,
      "ATK": 7991,
      "DEF": 10305,
      "WIS": 12001,
      "AGI": 13489
    },
    "skills": [
      979
    ],
    "autoAttack": 10136,
    "img": "1bd",
    "rarity": 4,
    "evo": 4,
    "fullName": "Mizuchi, the Maelstrom II"
  },
  "11682": {
    "name": "Takemikazuchi",
    "stats": {
      "HP": 17201,
      "ATK": 16995,
      "DEF": 17006,
      "WIS": 11004,
      "AGI": 8144
    },
    "skills": [
      980,
      981
    ],
    "autoAttack": 10137,
    "img": "24a",
    "rarity": 4,
    "evo": 2,
    "fullName": "Takemikazuchi, the Lightning II"
  },
  "11683": {
    "name": "Kushinada",
    "stats": {
      "HP": 17992,
      "ATK": 10549,
      "DEF": 15511,
      "WIS": 18999,
      "AGI": 18046
    },
    "skills": [
      982,
      983
    ],
    "autoAttack": 10138,
    "isMounted": true,
    "img": "411",
    "rarity": 5,
    "evo": 2,
    "fullName": "Kushinada, Shamaness II"
  },
  "11684": {
    "name": "Heracles",
    "stats": {
      "HP": 24849,
      "ATK": 25499,
      "DEF": 20061,
      "WIS": 13203,
      "AGI": 18154
    },
    "skills": [
      985
    ],
    "autoAttack": 10061,
    "img": "3cd",
    "rarity": 6,
    "evo": 2,
    "fullName": "Heracles, Mightiest of Men II"
  },
  "11685": {
    "name": "Hippolyta",
    "stats": {
      "HP": 20429,
      "ATK": 19389,
      "DEF": 17862,
      "WIS": 7971,
      "AGI": 17992
    },
    "skills": [
      986
    ],
    "autoAttack": 10103,
    "img": "247",
    "rarity": 5,
    "evo": 2,
    "fullName": "Hippolyta, Amazon Queen II"
  },
  "11686": {
    "name": "Antaeus",
    "stats": {
      "HP": 15652,
      "ATK": 17439,
      "DEF": 14048,
      "WIS": 6010,
      "AGI": 16800
    },
    "skills": [
      987
    ],
    "autoAttack": 10113,
    "img": "2f2",
    "rarity": 4,
    "evo": 2,
    "fullName": "Antaeus, Giant II"
  },
  "11688": {
    "name": "Autolycus",
    "stats": {
      "HP": 16144,
      "ATK": 16696,
      "DEF": 13538,
      "WIS": 6712,
      "AGI": 16902
    },
    "skills": [
      988
    ],
    "autoAttack": 10139,
    "img": "3e2",
    "rarity": 4,
    "evo": 4,
    "fullName": "Autolycus, Shrewd Warrior II"
  },
  "11689": {
    "name": "Thanatos",
    "stats": {
      "HP": 25532,
      "ATK": 23452,
      "DEF": 22834,
      "WIS": 14016,
      "AGI": 18544
    },
    "skills": [
      989,
      990
    ],
    "passiveSkills": [
      9005
    ],
    "autoAttack": 10061,
    "img": "362",
    "rarity": 6,
    "evo": 2,
    "fullName": "Thanatos, Death Incarnate II"
  },
  "11691": {
    "name": "Laola",
    "stats": {
      "HP": 18382,
      "ATK": 8068,
      "DEF": 17439,
      "WIS": 19129,
      "AGI": 18241
    },
    "skills": [
      996,
      997
    ],
    "autoAttack": 10142,
    "img": "3a1",
    "rarity": 5,
    "evo": 2,
    "fullName": "Laola, Demiwyrm Spearbearer II"
  },
  "11693": {
    "name": "Hina",
    "stats": {
      "HP": 16097,
      "ATK": 6736,
      "DEF": 10001,
      "WIS": 17875,
      "AGI": 17254
    },
    "skills": [
      993
    ],
    "autoAttack": 10019,
    "img": "4f1",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hina, Flame Serpent II"
  },
  "11694": {
    "name": "A'shi",
    "stats": {
      "HP": 18208,
      "ATK": 7039,
      "DEF": 16919,
      "WIS": 20689,
      "AGI": 18403
    },
    "skills": [
      994,
      995
    ],
    "autoAttack": 10143,
    "img": "13a",
    "rarity": 5,
    "evo": 2,
    "fullName": "A'shi, Pterorider II"
  },
  "11695": {
    "name": "Azan",
    "stats": {
      "HP": 14861,
      "ATK": 15478,
      "DEF": 14308,
      "WIS": 7982,
      "AGI": 17309
    },
    "skills": [
      992
    ],
    "autoAttack": 10141,
    "img": "18a",
    "rarity": 4,
    "evo": 2,
    "fullName": "Azan, the Dragon Bone II"
  },
  "11697": {
    "name": "Aegir",
    "stats": {
      "HP": 25098,
      "ATK": 15088,
      "DEF": 21675,
      "WIS": 24069,
      "AGI": 18544
    },
    "skills": [
      1035,
      1036
    ],
    "passiveSkills": [
      9006
    ],
    "autoAttack": 10007,
    "isMounted": true,
    "img": "2e8",
    "rarity": 6,
    "evo": 2,
    "fullName": "Aegir, the Roaring Sea II"
  },
  "11705": {
    "name": "Dryas",
    "stats": {
      "HP": 21025,
      "ATK": 21632,
      "DEF": 15901,
      "WIS": 7072,
      "AGI": 18100
    },
    "skills": [
      1032
    ],
    "autoAttack": 10113,
    "img": "15c",
    "rarity": 5,
    "evo": 2,
    "fullName": "Dryas, Centaur Knight II"
  },
  "11706": {
    "name": "Cat Sith Whipmaster",
    "stats": {
      "HP": 16215,
      "ATK": 16518,
      "DEF": 14211,
      "WIS": 5609,
      "AGI": 17504
    },
    "skills": [
      1210
    ],
    "autoAttack": 10185,
    "img": "41f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Cat Sith Whipmaster II"
  },
  "11710": {
    "name": "Muspell",
    "stats": {
      "HP": 22130,
      "ATK": 16063,
      "DEF": 17201,
      "WIS": 15305,
      "AGI": 13052
    },
    "skills": [
      1248,
      1249
    ],
    "autoAttack": 10192,
    "img": "1bc",
    "rarity": 5,
    "evo": 2,
    "fullName": "Muspell, Giant Knight II"
  },
  "11712": {
    "name": "Vesta",
    "stats": {
      "HP": 19552,
      "ATK": 20397,
      "DEF": 15500,
      "WIS": 7581,
      "AGI": 18219
    },
    "skills": [
      1260,
      1261
    ],
    "autoAttack": 10196,
    "img": "10b",
    "rarity": 5,
    "evo": 2,
    "fullName": "Vesta, Flame Witch II"
  },
  "11717": {
    "name": "Rex",
    "stats": {
      "HP": 15002,
      "ATK": 6097,
      "DEF": 14005,
      "WIS": 16009,
      "AGI": 17992
    },
    "skills": [
      1024,
      1025
    ],
    "autoAttack": 10001,
    "img": "3a3",
    "rarity": 4,
    "evo": 2,
    "fullName": "Crystallus Rex II"
  },
  "11720": {
    "name": "Laned",
    "stats": {
      "HP": 24578,
      "ATK": 23549,
      "DEF": 21523,
      "WIS": 13853,
      "AGI": 18349
    },
    "skills": [
      1031
    ],
    "autoAttack": 10153,
    "img": "22f",
    "rarity": 6,
    "evo": 2,
    "fullName": "Laned, the Piercing Fist II"
  },
  "11723": {
    "name": "Veigr",
    "stats": {
      "HP": 17220,
      "ATK": 17678,
      "DEF": 15024,
      "WIS": 11038,
      "AGI": 9101
    },
    "skills": [
      1251
    ],
    "autoAttack": 10191,
    "img": "298",
    "rarity": 4,
    "evo": 4,
    "fullName": "Veigr, Under-watch Captain II"
  },
  "11728": {
    "name": "Louise",
    "stats": {
      "HP": 16529,
      "ATK": 15912,
      "DEF": 14709,
      "WIS": 5111,
      "AGI": 17797
    },
    "skills": [
      1162
    ],
    "autoAttack": 10044,
    "img": "131",
    "rarity": 4,
    "evo": 2,
    "fullName": "Louise, Twilight Swordswoman II"
  },
  "11731": {
    "name": "Siege Horse",
    "stats": {
      "HP": 16013,
      "ATK": 13269,
      "DEF": 12049,
      "WIS": 9246,
      "AGI": 17340
    },
    "skills": [
      999
    ],
    "autoAttack": 10021,
    "img": "11e",
    "rarity": 4,
    "evo": 4,
    "fullName": "Vengeful Siege Horse II"
  },
  "11732": {
    "name": "Helen",
    "stats": {
      "HP": 19660,
      "ATK": 7039,
      "DEF": 15186,
      "WIS": 21404,
      "AGI": 18208
    },
    "skills": [
      1001,
      1002
    ],
    "autoAttack": 10007,
    "img": "10d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Helen, Swan Queen II"
  },
  "11733": {
    "name": "Paris",
    "stats": {
      "HP": 16854,
      "ATK": 16356,
      "DEF": 11318,
      "WIS": 7895,
      "AGI": 17363
    },
    "skills": [
      1003,
      1004
    ],
    "autoAttack": 10061,
    "img": "4ce",
    "rarity": 4,
    "evo": 2,
    "fullName": "Paris, Trueshot II"
  },
  "11735": {
    "name": "Cassandra",
    "stats": {
      "HP": 13013,
      "ATK": 7492,
      "DEF": 10087,
      "WIS": 13889,
      "AGI": 11063
    },
    "skills": [
      1005
    ],
    "autoAttack": 10007,
    "img": "39b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Cassandra, the Tragic II"
  },
  "11736": {
    "name": "Gunhild",
    "stats": {
      "HP": 20007,
      "ATK": 19508,
      "DEF": 16540,
      "WIS": 8491,
      "AGI": 18057
    },
    "skills": [
      1018,
      1019
    ],
    "autoAttack": 10145,
    "img": "494",
    "rarity": 5,
    "evo": 2,
    "fullName": "Gunhild, Brass Pincers II"
  },
  "11738": {
    "name": "Eric",
    "stats": {
      "HP": 15999,
      "ATK": 16303,
      "DEF": 14096,
      "WIS": 6051,
      "AGI": 16803
    },
    "skills": [
      1020
    ],
    "autoAttack": 10146,
    "img": "35d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Eric, Bloodaxe King II"
  },
  "11740": {
    "name": "Eviscerating Hafgufa",
    "stats": {
      "HP": 13013,
      "ATK": 12999,
      "DEF": 12001,
      "WIS": 7003,
      "AGI": 11770
    },
    "skills": [
      1023
    ],
    "autoAttack": 10005,
    "img": "407",
    "rarity": 4,
    "evo": 4,
    "fullName": "Eviscerating Hafgufa II"
  },
  "11743": {
    "name": "Vepar",
    "stats": {
      "HP": 16668,
      "ATK": 16196,
      "DEF": 13438,
      "WIS": 5847,
      "AGI": 17047
    },
    "skills": [
      1047
    ],
    "autoAttack": 10145,
    "img": "451",
    "rarity": 4,
    "evo": 4,
    "fullName": "Vepar, the Roiling Sea II"
  },
  "11744": {
    "name": "Triton",
    "stats": {
      "HP": 20386,
      "ATK": 18414,
      "DEF": 16735,
      "WIS": 10289,
      "AGI": 18176
    },
    "skills": [
      1049,
      1050
    ],
    "autoAttack": 10151,
    "img": "299",
    "rarity": 5,
    "evo": 2,
    "fullName": "Triton, Lord of the Sea II"
  },
  "11745": {
    "name": "Rusalka",
    "stats": {
      "HP": 16713,
      "ATK": 5988,
      "DEF": 11015,
      "WIS": 17439,
      "AGI": 17374
    },
    "skills": [
      1051,
      1052
    ],
    "autoAttack": 10003,
    "img": "4a8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Rusalka, Spirit of Water II"
  },
  "11747": {
    "name": "Kuki",
    "stats": {
      "HP": 13196,
      "ATK": 13049,
      "DEF": 11880,
      "WIS": 7223,
      "AGI": 10256
    },
    "skills": [
      1053
    ],
    "img": "46d",
    "rarity": 4,
    "evo": 4,
    "fullName": "Kuki, Pirate Busho II"
  },
  "11750": {
    "name": "Khepri",
    "stats": {
      "HP": 14792,
      "ATK": 7602,
      "DEF": 12999,
      "WIS": 16010,
      "AGI": 16707
    },
    "skills": [
      1060
    ],
    "autoAttack": 10155,
    "img": "108",
    "rarity": 4,
    "evo": 4,
    "fullName": "Khepri, the Morning Sun II"
  },
  "11752": {
    "name": "Petsuchos",
    "stats": {
      "HP": 12513,
      "ATK": 13510,
      "DEF": 11001,
      "WIS": 6006,
      "AGI": 13769
    },
    "skills": [
      1063
    ],
    "autoAttack": 10011,
    "img": "1c1",
    "rarity": 4,
    "evo": 4,
    "fullName": "Petsuchos Executioner II"
  },
  "11753": {
    "name": "Pagos",
    "stats": {
      "HP": 16995,
      "ATK": 11297,
      "DEF": 15002,
      "WIS": 8101,
      "AGI": 17699
    },
    "skills": [
      1064,
      1065
    ],
    "autoAttack": 10137,
    "img": "255",
    "rarity": 4,
    "evo": 2,
    "fullName": "Pagos, Camel Cavalryman II"
  },
  "11754": {
    "name": "Horus",
    "stats": {
      "HP": 21003,
      "ATK": 17797,
      "DEF": 16497,
      "WIS": 7440,
      "AGI": 18360
    },
    "skills": [
      1066,
      1067
    ],
    "autoAttack": 10021,
    "img": "3e4",
    "rarity": 5,
    "evo": 2,
    "fullName": "Horus, the Falcon God II"
  },
  "11755": {
    "name": "Spartacus",
    "stats": {
      "HP": 25770,
      "ATK": 24275,
      "DEF": 24871,
      "WIS": 15381,
      "AGI": 11557
    },
    "skills": [
      1070
    ],
    "autoAttack": 10156,
    "img": "4ca",
    "rarity": 6,
    "evo": 2,
    "fullName": "Spartacus, Rebel Gladiator II"
  },
  "11756": {
    "name": "Julia",
    "stats": {
      "HP": 21870,
      "ATK": 20353,
      "DEF": 14178,
      "WIS": 6725,
      "AGI": 18154
    },
    "skills": [
      1071
    ],
    "img": "1da",
    "rarity": 5,
    "evo": 2,
    "fullName": "Julia, Centaur Eques II"
  },
  "11759": {
    "name": "Gaiuz",
    "stats": {
      "HP": 17109,
      "ATK": 18023,
      "DEF": 12134,
      "WIS": 5978,
      "AGI": 16803
    },
    "skills": [
      1073
    ],
    "autoAttack": 10104,
    "img": "461",
    "rarity": 4,
    "evo": 4,
    "fullName": "Gaiuz, Crashing Wave II"
  },
  "11760": {
    "name": "Crassus",
    "stats": {
      "HP": 24253,
      "ATK": 25131,
      "DEF": 22087,
      "WIS": 14482,
      "AGI": 18544
    },
    "skills": [
      1074,
      1075
    ],
    "passiveSkills": [
      9007
    ],
    "autoAttack": 10106,
    "img": "3be",
    "rarity": 6,
    "evo": 2,
    "fullName": "Crassus, the Lion General II"
  },
  "11762": {
    "name": "Frostscale",
    "stats": {
      "HP": 17038,
      "ATK": 4396,
      "DEF": 12391,
      "WIS": 17721,
      "AGI": 17201
    },
    "skills": [
      1079
    ],
    "autoAttack": 10158,
    "img": "138",
    "rarity": 4,
    "evo": 2,
    "fullName": "Frostscale Plesiosaur II"
  },
  "11764": {
    "name": "Thundering",
    "stats": {
      "HP": 12686,
      "ATK": 5029,
      "DEF": 14927,
      "WIS": 17899,
      "AGI": 17424
    },
    "skills": [
      1080
    ],
    "autoAttack": 10159,
    "img": "2a2",
    "rarity": 4,
    "evo": 4,
    "fullName": "Thundering Pterosaur II"
  },
  "11765": {
    "name": "Huitzilopochtli",
    "stats": {
      "HP": 20050,
      "ATK": 18555,
      "DEF": 18100,
      "WIS": 8827,
      "AGI": 18219
    },
    "skills": [
      1081,
      1082
    ],
    "autoAttack": 10160,
    "img": "282",
    "rarity": 5,
    "evo": 2,
    "fullName": "Huitzilopochtli, God of War II"
  },
  "11766": {
    "name": "Mielikki",
    "stats": {
      "HP": 19053,
      "ATK": 18566,
      "DEF": 16681,
      "WIS": 11340,
      "AGI": 18111
    },
    "skills": [
      1083,
      1084
    ],
    "autoAttack": 10161,
    "img": "1b5",
    "rarity": 5,
    "evo": 2,
    "fullName": "Mielikki, Bear Rider II"
  },
  "11767": {
    "name": "Ortlinde",
    "stats": {
      "HP": 25716,
      "ATK": 23625,
      "DEF": 21328,
      "WIS": 15218,
      "AGI": 18598
    },
    "skills": [
      1252,
      1253
    ],
    "passiveSkills": [
      9011
    ],
    "autoAttack": 10103,
    "isMounted": true,
    "img": "10e",
    "rarity": 6,
    "evo": 2,
    "fullName": "Ortlinde, Silent Valkyrie II"
  },
  "11774": {
    "name": "Gog",
    "stats": {
      "HP": 16724,
      "ATK": 15197,
      "DEF": 13052,
      "WIS": 7884,
      "AGI": 17201
    },
    "skills": [
      1250
    ],
    "autoAttack": 10034,
    "img": "48e",
    "rarity": 4,
    "evo": 2,
    "fullName": "Gog, Giant II"
  },
  "11775": {
    "name": "Magog",
    "stats": {
      "HP": 17049,
      "ATK": 16648,
      "DEF": 16161,
      "WIS": 12380,
      "AGI": 9000
    },
    "skills": [
      1256
    ],
    "autoAttack": 10193,
    "img": "315",
    "rarity": 4,
    "evo": 2,
    "fullName": "Magog, Giant II"
  },
  "11779": {
    "name": "Hagen",
    "stats": {
      "HP": 16779,
      "ATK": 15281,
      "DEF": 15438,
      "WIS": 4785,
      "AGI": 16889
    },
    "skills": [
      1087
    ],
    "autoAttack": 10162,
    "img": "1c4",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hagen, Dueling King II"
  },
  "11780": {
    "name": "Uscias",
    "stats": {
      "HP": 20234,
      "ATK": 6790,
      "DEF": 14991,
      "WIS": 21458,
      "AGI": 18154
    },
    "skills": [
      1089,
      1090
    ],
    "autoAttack": 10163,
    "img": "26f",
    "rarity": 5,
    "evo": 2,
    "fullName": "Uscias, the Claiomh Solais II"
  },
  "11781": {
    "name": "Muramasa",
    "stats": {
      "HP": 17049,
      "ATK": 16042,
      "DEF": 13160,
      "WIS": 5923,
      "AGI": 17569
    },
    "skills": [
      1091,
      1092
    ],
    "autoAttack": 10108,
    "img": "45e",
    "rarity": 4,
    "evo": 2,
    "fullName": "Muramasa, the Cursed Katana II"
  },
  "11783": {
    "name": "Paracelsus",
    "stats": {
      "HP": 12806,
      "ATK": 7806,
      "DEF": 8198,
      "WIS": 15950,
      "AGI": 11111
    },
    "skills": [
      1093
    ],
    "autoAttack": 10007,
    "img": "15f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Paracelsus, Venomdagger II"
  },
  "11786": {
    "name": "Ruyi Zhenxian",
    "stats": {
      "HP": 14500,
      "ATK": 16489,
      "DEF": 14500,
      "WIS": 6103,
      "AGI": 16510
    },
    "skills": [
      1109
    ],
    "autoAttack": 10165,
    "img": "308",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ruyi Zhenxian, the Ferocious II"
  },
  "11788": {
    "name": "Qiong Qi",
    "stats": {
      "HP": 12806,
      "ATK": 11903,
      "DEF": 10904,
      "WIS": 6689,
      "AGI": 14500
    },
    "skills": [
      1112
    ],
    "autoAttack": 10011,
    "img": "435",
    "rarity": 4,
    "evo": 4,
    "fullName": "Qiong Qi, Man Eater II"
  },
  "11789": {
    "name": "Chanchu",
    "stats": {
      "HP": 15002,
      "ATK": 4396,
      "DEF": 13008,
      "WIS": 19097,
      "AGI": 17602
    },
    "skills": [
      1113,
      1114
    ],
    "autoAttack": 10001,
    "img": "209",
    "rarity": 4,
    "evo": 2,
    "fullName": "Chanchu, Hermit II"
  },
  "11791": {
    "name": "Gawain",
    "stats": {
      "HP": 23798,
      "ATK": 22509,
      "DEF": 23224,
      "WIS": 14113,
      "AGI": 18208
    },
    "skills": [
      1118
    ],
    "autoAttack": 10166,
    "img": "4a3",
    "rarity": 6,
    "evo": 2,
    "fullName": "Sir Gawain, Sun Knight II"
  },
  "11792": {
    "name": "Bercilak",
    "stats": {
      "HP": 21588,
      "ATK": 22856,
      "DEF": 15034,
      "WIS": 6487,
      "AGI": 17797
    },
    "skills": [
      1119,
      1120
    ],
    "autoAttack": 10044,
    "img": "111",
    "rarity": 5,
    "evo": 2,
    "fullName": "Bercilak, Green Knight II"
  },
  "11793": {
    "name": "Carl",
    "stats": {
      "HP": 16009,
      "ATK": 16952,
      "DEF": 14482,
      "WIS": 5403,
      "AGI": 17201
    },
    "skills": [
      1121
    ],
    "autoAttack": 10167,
    "img": "1b8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Carl, Giant Knight II"
  },
  "11795": {
    "name": "Ragnelle",
    "stats": {
      "HP": 16681,
      "ATK": 6064,
      "DEF": 11662,
      "WIS": 17106,
      "AGI": 17292
    },
    "skills": [
      1122
    ],
    "autoAttack": 10129,
    "img": "32b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ragnelle, the Moonlight II"
  },
  "11798": {
    "name": "Ollpheist",
    "stats": {
      "HP": 17569,
      "ATK": 3594,
      "DEF": 13507,
      "WIS": 16876,
      "AGI": 17201
    },
    "skills": [
      1127
    ],
    "autoAttack": 10169,
    "img": "372",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ollpheist II"
  },
  "11800": {
    "name": "Fergus",
    "stats": {
      "HP": 14879,
      "ATK": 14792,
      "DEF": 14413,
      "WIS": 10418,
      "AGI": 15487
    },
    "skills": [
      1128
    ],
    "autoAttack": 10170,
    "img": "1df",
    "rarity": 4,
    "evo": 4,
    "fullName": "Fergus, Bold King II"
  },
  "11801": {
    "name": "Scathach",
    "stats": {
      "HP": 19140,
      "ATK": 6844,
      "DEF": 18100,
      "WIS": 19151,
      "AGI": 18013
    },
    "skills": [
      1129,
      1130
    ],
    "autoAttack": 10171,
    "img": "4a6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Scathach, Shadow Goddess II"
  },
  "11802": {
    "name": "Medb",
    "stats": {
      "HP": 21231,
      "ATK": 5999,
      "DEF": 14893,
      "WIS": 20906,
      "AGI": 18219
    },
    "skills": [
      1131,
      1132
    ],
    "autoAttack": 10001,
    "img": "494",
    "rarity": 5,
    "evo": 2,
    "fullName": "Medb, Jealous Queen II"
  },
  "11803": {
    "name": "Andras",
    "stats": {
      "HP": 18501,
      "ATK": 20007,
      "DEF": 16009,
      "WIS": 8144,
      "AGI": 18447
    },
    "skills": [
      1116,
      1117
    ],
    "autoAttack": 10011,
    "isMounted": true,
    "img": "2c8",
    "rarity": 5,
    "evo": 2,
    "fullName": "Andras, the Slayer II"
  },
  "11807": {
    "name": "Gabrielle",
    "stats": {
      "HP": 19974,
      "ATK": 17775,
      "DEF": 17255,
      "WIS": 8101,
      "AGI": 18143
    },
    "skills": [
      1245,
      1246
    ],
    "img": "372",
    "rarity": 5,
    "evo": 2,
    "fullName": "Gabrielle, Angel of Sky II"
  },
  "11816": {
    "name": "Pallas",
    "stats": {
      "HP": 18501,
      "ATK": 24990,
      "DEF": 15998,
      "WIS": 5858,
      "AGI": 18252
    },
    "skills": [
      1026,
      1027
    ],
    "autoAttack": 10133,
    "img": "1b1",
    "rarity": 5,
    "evo": 2,
    "fullName": "Pallas, Goddess of Protection II"
  },
  "11819": {
    "name": "Shisen",
    "stats": {
      "HP": 16657,
      "ATK": 15498,
      "DEF": 13052,
      "WIS": 6092,
      "AGI": 17499
    },
    "skills": [
      1034
    ],
    "autoAttack": 10137,
    "img": "1c6",
    "rarity": 4,
    "evo": 4,
    "fullName": "Shisen, the Flitting Bolt II"
  },
  "11821": {
    "name": "Jarn",
    "stats": {
      "HP": 13038,
      "ATK": 17461,
      "DEF": 14269,
      "WIS": 7319,
      "AGI": 17120
    },
    "skills": [
      1040
    ],
    "autoAttack": 10150,
    "img": "1d2",
    "rarity": 4,
    "evo": 4,
    "fullName": "Jarn, the Bladed Wolf II"
  },
  "11824": {
    "name": "Pumpkin Hangman",
    "stats": {
      "HP": 15462,
      "ATK": 6723,
      "DEF": 11770,
      "WIS": 16900,
      "AGI": 17071
    },
    "skills": [
      1135
    ],
    "autoAttack": 10172,
    "img": "2e2",
    "rarity": 4,
    "evo": 4,
    "fullName": "Pumpkin Hangman II"
  },
  "11825": {
    "name": "Pomona",
    "stats": {
      "HP": 20559,
      "ATK": 17894,
      "DEF": 13528,
      "WIS": 11275,
      "AGI": 18349
    },
    "skills": [
      1137,
      1138
    ],
    "autoAttack": 10151,
    "img": "2e6",
    "rarity": 5,
    "evo": 2,
    "fullName": "Pomona, Grove Goddess II"
  },
  "11826": {
    "name": "Tricia",
    "stats": {
      "HP": 16356,
      "ATK": 15348,
      "DEF": 12965,
      "WIS": 6064,
      "AGI": 17797
    },
    "skills": [
      1139,
      1140
    ],
    "autoAttack": 10151,
    "img": "393",
    "rarity": 4,
    "evo": 2,
    "fullName": "Tricia, Cauldron Witch II"
  },
  "11828": {
    "name": "Leopard Queen",
    "stats": {
      "HP": 12487,
      "ATK": 11428,
      "DEF": 10564,
      "WIS": 8026,
      "AGI": 13256
    },
    "skills": [
      1142
    ],
    "img": "4d5",
    "rarity": 4,
    "evo": 4,
    "fullName": "Cat Sith Leopard Queen II"
  },
  "11831": {
    "name": "Samedi",
    "stats": {
      "HP": 17292,
      "ATK": 7102,
      "DEF": 16609,
      "WIS": 16596,
      "AGI": 10511
    },
    "skills": [
      1157
    ],
    "autoAttack": 10176,
    "img": "4fb",
    "rarity": 4,
    "evo": 4,
    "fullName": "Samedi, Dark Necromancer II"
  },
  "11833": {
    "name": "Twar",
    "stats": {
      "HP": 12904,
      "ATK": 7785,
      "DEF": 10001,
      "WIS": 12097,
      "AGI": 14000
    },
    "skills": [
      1154
    ],
    "autoAttack": 10001,
    "img": "12c",
    "rarity": 4,
    "evo": 4,
    "fullName": "Twar, Ghost Archmage II"
  },
  "11834": {
    "name": "Joro-gumo",
    "stats": {
      "HP": 14807,
      "ATK": 4537,
      "DEF": 13008,
      "WIS": 18902,
      "AGI": 17851
    },
    "skills": [
      1148,
      1149
    ],
    "autoAttack": 10174,
    "img": "2b3",
    "rarity": 4,
    "evo": 2,
    "fullName": "Joro-gumo II"
  },
  "11835": {
    "name": "Peri",
    "stats": {
      "HP": 17699,
      "ATK": 12293,
      "DEF": 15002,
      "WIS": 17797,
      "AGI": 18403
    },
    "skills": [
      1152,
      1153
    ],
    "autoAttack": 10001,
    "img": "1bf",
    "rarity": 5,
    "evo": 2,
    "fullName": "Peri, Spirit of Fire II"
  },
  "11836": {
    "name": "Strigoi",
    "stats": {
      "HP": 25120,
      "ATK": 18512,
      "DEF": 24102,
      "WIS": 15803,
      "AGI": 18306
    },
    "skills": [
      1159
    ],
    "autoAttack": 10034,
    "img": "43a",
    "rarity": 6,
    "evo": 2,
    "fullName": "Strigoi, Undying Warrior II"
  },
  "11837": {
    "name": "Mormo",
    "stats": {
      "HP": 21047,
      "ATK": 22022,
      "DEF": 14005,
      "WIS": 6064,
      "AGI": 18100
    },
    "skills": [
      1160,
      1161
    ],
    "autoAttack": 10177,
    "isMounted": true,
    "img": "150",
    "rarity": 5,
    "evo": 2,
    "fullName": "Mormo, Nightmare II"
  },
  "11840": {
    "name": "Gilles",
    "stats": {
      "HP": 16596,
      "ATK": 5871,
      "DEF": 12025,
      "WIS": 17426,
      "AGI": 16902
    },
    "skills": [
      1163
    ],
    "autoAttack": 10163,
    "img": "2cc",
    "rarity": 4,
    "evo": 4,
    "fullName": "Gilles, Mad Knight II"
  },
  "11841": {
    "name": "Van",
    "stats": {
      "HP": 25662,
      "ATK": 25521,
      "DEF": 20126,
      "WIS": 14633,
      "AGI": 18555
    },
    "skills": [
      1164,
      1165
    ],
    "passiveSkills": [
      9009
    ],
    "autoAttack": 10103,
    "img": "425",
    "rarity": 6,
    "evo": 2,
    "fullName": "Van, Shadow Hunter II"
  },
  "11843": {
    "name": "Rustom",
    "stats": {
      "HP": 16096,
      "ATK": 15847,
      "DEF": 16215,
      "WIS": 5035,
      "AGI": 16800
    },
    "skills": [
      1168
    ],
    "autoAttack": 10178,
    "img": "197",
    "rarity": 4,
    "evo": 2,
    "fullName": "Rustom, Zombie Ape II"
  },
  "11845": {
    "name": "Latona",
    "stats": {
      "HP": 14952,
      "ATK": 15147,
      "DEF": 14585,
      "WIS": 5506,
      "AGI": 16803
    },
    "skills": [
      1169
    ],
    "autoAttack": 10179,
    "img": "267",
    "rarity": 4,
    "evo": 4,
    "fullName": "Latona, Wolfwoman II"
  },
  "11846": {
    "name": "Lippy",
    "stats": {
      "HP": 22466,
      "ATK": 21209,
      "DEF": 14536,
      "WIS": 7321,
      "AGI": 18219
    },
    "skills": [
      1170,
      1171
    ],
    "autoAttack": 10029,
    "img": "1fc",
    "rarity": 5,
    "evo": 2,
    "fullName": "Lippy, Candymancer II"
  },
  "11847": {
    "name": "Urom",
    "stats": {
      "HP": 20971,
      "ATK": 21209,
      "DEF": 15056,
      "WIS": 8415,
      "AGI": 18100
    },
    "skills": [
      1172,
      1173
    ],
    "autoAttack": 10180,
    "img": "172",
    "rarity": 5,
    "evo": 2,
    "fullName": "Urom, Mummy Lizardman II"
  },
  "11849": {
    "name": "Beelzebub",
    "stats": {
      "HP": 22509,
      "ATK": 22910,
      "DEF": 21242,
      "WIS": 17049,
      "AGI": 18143
    },
    "skills": [
      1174
    ],
    "autoAttack": 10181,
    "img": "3df",
    "rarity": 6,
    "evo": 2,
    "fullName": "Beelzebub, Glutton King II"
  },
  "11852": {
    "name": "Nicola",
    "stats": {
      "HP": 16048,
      "ATK": 14606,
      "DEF": 12597,
      "WIS": 7528,
      "AGI": 17147
    },
    "skills": [
      1178
    ],
    "autoAttack": 10182,
    "img": "208",
    "rarity": 4,
    "evo": 4,
    "fullName": "Nicola, the Poison Fly II"
  },
  "11853": {
    "name": "Tatsuta",
    "stats": {
      "HP": 20104,
      "ATK": 6768,
      "DEF": 16421,
      "WIS": 20126,
      "AGI": 18176
    },
    "skills": [
      1180,
      1181
    ],
    "autoAttack": 10007,
    "img": "25d",
    "rarity": 5,
    "evo": 2,
    "fullName": "Lady Tatsuta, the Mapleleaf II"
  },
  "11854": {
    "name": "Domini",
    "stats": {
      "HP": 15847,
      "ATK": 5880,
      "DEF": 12358,
      "WIS": 17049,
      "AGI": 17385
    },
    "skills": [
      1182,
      1183
    ],
    "autoAttack": 10007,
    "img": "2ad",
    "rarity": 4,
    "evo": 2,
    "fullName": "Domini, Pest Controller II"
  },
  "11856": {
    "name": "Bare-Branch",
    "stats": {
      "HP": 12355,
      "ATK": 8698,
      "DEF": 9063,
      "WIS": 13451,
      "AGI": 11390
    },
    "skills": [
      1184
    ],
    "autoAttack": 10007,
    "img": "26f",
    "rarity": 4,
    "evo": 4,
    "fullName": "Bare-Branch Treant II"
  },
  "11859": {
    "name": "Bennu",
    "stats": {
      "HP": 15134,
      "ATK": 5040,
      "DEF": 13245,
      "WIS": 17864,
      "AGI": 16803
    },
    "skills": [
      1199
    ],
    "autoAttack": 10110,
    "img": "275",
    "rarity": 4,
    "evo": 4,
    "fullName": "Bennu, the Sun Bird II"
  },
  "11861": {
    "name": "Caim",
    "stats": {
      "HP": 12720,
      "ATK": 12293,
      "DEF": 11124,
      "WIS": 6516,
      "AGI": 14134
    },
    "skills": [
      1202
    ],
    "img": "37b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Caim, the Dark Plume II"
  },
  "11864": {
    "name": "Tyr",
    "stats": {
      "HP": 25109,
      "ATK": 25012,
      "DEF": 24578,
      "WIS": 9000,
      "AGI": 18154
    },
    "skills": [
      1207
    ],
    "autoAttack": 10184,
    "img": "42b",
    "rarity": 6,
    "evo": 2,
    "fullName": "Tyr, God of War II"
  },
  "11865": {
    "name": "Garmr",
    "stats": {
      "HP": 21339,
      "ATK": 19205,
      "DEF": 14417,
      "WIS": 8025,
      "AGI": 18252
    },
    "skills": [
      1208,
      1209
    ],
    "autoAttack": 10061,
    "img": "2af",
    "rarity": 5,
    "evo": 2,
    "fullName": "Garmr, Watchhound II"
  },
  "11869": {
    "name": "Hel",
    "stats": {
      "HP": 25824,
      "ATK": 12315,
      "DEF": 22249,
      "WIS": 25553,
      "AGI": 18555
    },
    "skills": [
      1212,
      1213
    ],
    "passiveSkills": [
      9010
    ],
    "autoAttack": 10129,
    "img": "135",
    "rarity": 6,
    "evo": 2,
    "fullName": "Hel, Goddess of Woe II"
  },
  "11870": {
    "name": "Chicomecoatl",
    "stats": {
      "HP": 21112,
      "ATK": 10192,
      "DEF": 17363,
      "WIS": 19530,
      "AGI": 13008
    },
    "skills": [
      1205,
      1206
    ],
    "autoAttack": 10001,
    "img": "1ba",
    "rarity": 5,
    "evo": 2,
    "fullName": "Chicomecoatl, the Bountiful II"
  },
  "11871": {
    "name": "Ain",
    "stats": {
      "HP": 15652,
      "ATK": 4472,
      "DEF": 15099,
      "WIS": 16020,
      "AGI": 17504
    },
    "skills": [
      1216
    ],
    "autoAttack": 10186,
    "img": "400",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ain, Squirrel-back Faerie II"
  },
  "11873": {
    "name": "Iridescent Chalchiuhtotolin",
    "stats": {
      "HP": 16510,
      "ATK": 13476,
      "DEF": 15778,
      "WIS": 5687,
      "AGI": 17292
    },
    "skills": [
      1217
    ],
    "img": "4fb",
    "rarity": 4,
    "evo": 4,
    "fullName": "Iridescent Chalchiuhtotolin II"
  },
  "11874": {
    "name": "Idun",
    "stats": {
      "HP": 20722,
      "ATK": 6270,
      "DEF": 15706,
      "WIS": 20299,
      "AGI": 18252
    },
    "skills": [
      1218,
      1219
    ],
    "autoAttack": 10003,
    "img": "467",
    "rarity": 5,
    "evo": 2,
    "fullName": "Idun, the Golden Apple II"
  },
  "11875": {
    "name": "Guardian of the Grove",
    "stats": {
      "HP": 20104,
      "ATK": 19086,
      "DEF": 14807,
      "WIS": 8935,
      "AGI": 18317
    },
    "skills": [
      1221,
      1222
    ],
    "autoAttack": 10187,
    "img": "3fa",
    "rarity": 5,
    "evo": 2,
    "fullName": "Guardian of the Grove II"
  },
  "11880": {
    "name": "Yule",
    "stats": {
      "HP": 15024,
      "ATK": 17353,
      "DEF": 13256,
      "WIS": 6530,
      "AGI": 17034
    },
    "skills": [
      1226
    ],
    "autoAttack": 10157,
    "img": "2fc",
    "rarity": 4,
    "evo": 4,
    "fullName": "Yule Goat, Death Bringer II"
  },
  "11881": {
    "name": "Vidar",
    "stats": {
      "HP": 22141,
      "ATK": 21859,
      "DEF": 15587,
      "WIS": 6183,
      "AGI": 18306
    },
    "skills": [
      1228,
      1229
    ],
    "img": "155",
    "rarity": 5,
    "evo": 2,
    "fullName": "Vidar, the Iron Heel II"
  },
  "11882": {
    "name": "Negafok",
    "stats": {
      "HP": 17103,
      "ATK": 16258,
      "DEF": 13431,
      "WIS": 5468,
      "AGI": 17493
    },
    "skills": [
      1230,
      1231
    ],
    "autoAttack": 10103,
    "img": "192",
    "rarity": 4,
    "evo": 2,
    "fullName": "Negafok, Reindeer Rider II"
  },
  "11884": {
    "name": "Brass Snow-Leopard",
    "stats": {
      "HP": 13256,
      "ATK": 13024,
      "DEF": 10074,
      "WIS": 8209,
      "AGI": 10564
    },
    "skills": [
      1232
    ],
    "img": "4d8",
    "rarity": 4,
    "evo": 4,
    "fullName": "Brass Snow-Leopard II"
  },
  "11888": {
    "name": "Kosuke",
    "stats": {
      "HP": 15755,
      "ATK": 16913,
      "DEF": 14003,
      "WIS": 5664,
      "AGI": 17023
    },
    "skills": [
      1237
    ],
    "autoAttack": 10188,
    "img": "454",
    "rarity": 4,
    "evo": 4,
    "fullName": "Kosuke, Master Ninja II"
  },
  "11890": {
    "name": "Chuchunya",
    "stats": {
      "HP": 12659,
      "ATK": 13245,
      "DEF": 11756,
      "WIS": 6564,
      "AGI": 12586
    },
    "skills": [
      1240
    ],
    "img": "344",
    "rarity": 4,
    "evo": 4,
    "fullName": "Chuchunya, Iceberg Breaker II"
  },
  "11891": {
    "name": "Gorynich",
    "stats": {
      "HP": 18826,
      "ATK": 14579,
      "DEF": 17082,
      "WIS": 12185,
      "AGI": 18533
    },
    "skills": [
      1243,
      1244
    ],
    "autoAttack": 10190,
    "img": "395",
    "rarity": 5,
    "evo": 2,
    "fullName": "Gorynich, Snow Dragon II"
  },
  "11892": {
    "name": "Nyx",
    "stats": {
      "HP": 16280,
      "ATK": 14113,
      "DEF": 16172,
      "WIS": 4558,
      "AGI": 17981
    },
    "skills": [
      1241,
      1242
    ],
    "autoAttack": 10189,
    "img": "32f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Nyx, the Dark Wing II"
  },
  "11904": {
    "name": "Marsyas",
    "stats": {
      "HP": 15889,
      "ATK": 6430,
      "DEF": 10346,
      "WIS": 17950,
      "AGI": 17315
    },
    "skills": [
      1266
    ],
    "autoAttack": 10007,
    "img": "3d2",
    "rarity": 4,
    "evo": 4,
    "fullName": "Marsyas, the Cursed Flute II"
  },
  "11905": {
    "name": "Apollo",
    "stats": {
      "HP": 22043,
      "ATK": 15901,
      "DEF": 14514,
      "WIS": 10809,
      "AGI": 18360
    },
    "skills": [
      1268,
      1269
    ],
    "autoAttack": 10061,
    "img": "307",
    "rarity": 5,
    "evo": 2,
    "fullName": "Apollo, God of the Sun II"
  },
  "11908": {
    "name": "Hyacinth",
    "stats": {
      "HP": 13476,
      "ATK": 10391,
      "DEF": 10321,
      "WIS": 7966,
      "AGI": 12366
    },
    "skills": [
      1272
    ],
    "autoAttack": 10005,
    "img": "182",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hyacinth, the Death Dealer II"
  },
  "11909": {
    "name": "Amaterasu",
    "stats": {
      "HP": 24102,
      "ATK": 12553,
      "DEF": 23387,
      "WIS": 23387,
      "AGI": 18403
    },
    "skills": [
      1262,
      1263
    ],
    "autoAttack": 10016,
    "img": "3d1",
    "rarity": 6,
    "evo": 2,
    "fullName": "Amaterasu, Light of the Sun II"
  },
  "11914": {
    "name": "Taromaiti",
    "stats": {
      "HP": 12890,
      "ATK": 12999,
      "DEF": 10538,
      "WIS": 7419,
      "AGI": 12734
    },
    "skills": [
      1278
    ],
    "img": "152",
    "rarity": 4,
    "evo": 4,
    "fullName": "Taromaiti, Depraved Queen II"
  },
  "11915": {
    "name": "Champion of Aquarius",
    "stats": {
      "HP": 19974,
      "ATK": 4732,
      "DEF": 18761,
      "WIS": 19378,
      "AGI": 18403
    },
    "skills": [
      1258,
      1259
    ],
    "autoAttack": 10195,
    "img": "202",
    "rarity": 5,
    "evo": 2,
    "fullName": "Champion of Aquarius II"
  },
  "11917": {
    "name": "Sir",
    "stats": {
      "HP": 25748,
      "ATK": 24578,
      "DEF": 22455,
      "WIS": 10571,
      "AGI": 18501
    },
    "skills": [
      1279
    ],
    "img": "44e",
    "rarity": 6,
    "evo": 2,
    "fullName": "Sir Galahad, Knight Champion II"
  },
  "11922": {
    "name": "Ritho",
    "stats": {
      "HP": 25402,
      "ATK": 24188,
      "DEF": 24123,
      "WIS": 10127,
      "AGI": 18013
    },
    "skills": [
      1247
    ],
    "autoAttack": 10145,
    "img": "4c0",
    "rarity": 6,
    "evo": 2,
    "fullName": "Ritho, King of the Giants II"
  },
  "11930": {
    "name": "Ljung",
    "stats": {
      "HP": 16974,
      "ATK": 17133,
      "DEF": 13710,
      "WIS": 5029,
      "AGI": 17206
    },
    "skills": [
      1211
    ],
    "autoAttack": 10105,
    "img": "441",
    "rarity": 4,
    "evo": 4,
    "fullName": "Ljung, the Wrecker II"
  },
  "11932": {
    "name": "Halphas",
    "stats": {
      "HP": 17058,
      "ATK": 16913,
      "DEF": 12745,
      "WIS": 5371,
      "AGI": 17913
    },
    "skills": [
      1257
    ],
    "autoAttack": 10194,
    "img": "496",
    "rarity": 4,
    "evo": 4,
    "fullName": "Halphas, Earl of Hell II"
  },
  "11934": {
    "name": "Wepwawet",
    "stats": {
      "HP": 16230,
      "ATK": 15851,
      "DEF": 11634,
      "WIS": 6881,
      "AGI": 17533
    },
    "skills": [
      1275
    ],
    "img": "17b",
    "rarity": 4,
    "evo": 4,
    "fullName": "Wepwawet, the Vanguard II"
  },
  "11936": {
    "name": "Hervor",
    "stats": {
      "HP": 17751,
      "ATK": 18157,
      "DEF": 14535,
      "WIS": 14120,
      "AGI": 5506
    },
    "skills": [
      1283
    ],
    "autoAttack": 10041,
    "img": "385",
    "rarity": 4,
    "evo": 4,
    "fullName": "Hervor, the Cursed Blade II"
  },
  "21104": {
    "name": "IIG",
    "stats": {
      "HP": 23155,
      "ATK": 19935,
      "DEF": 21027,
      "WIS": 8440,
      "AGI": 17505
    },
    "skills": [
      444,
      445
    ],
    "img": "15f",
    "rarity": 5,
    "evo": 3,
    "fullName": "Impregnable Iron Golem"
  },
  "21187": {
    "name": "Loki",
    "stats": {
      "HP": 19202,
      "ATK": 21231,
      "DEF": 16192,
      "WIS": 15119,
      "AGI": 15806
    },
    "skills": [
      382
    ],
    "img": "47b",
    "rarity": 5,
    "evo": 3,
    "fullName": "Loki, God of Cunning"
  },
  "21216": {
    "name": "Gremory",
    "stats": {
      "HP": 18466,
      "ATK": 12819,
      "DEF": 18945,
      "WIS": 20426,
      "AGI": 17009
    },
    "skills": [
      411
    ],
    "autoAttack": 10007,
    "img": "20b",
    "rarity": 5,
    "evo": 3,
    "fullName": "Gremory, the Vermilion Moon"
  },
  "21228": {
    "name": "Hierophant",
    "stats": {
      "HP": 19681,
      "ATK": 13391,
      "DEF": 17534,
      "WIS": 20112,
      "AGI": 16950
    },
    "skills": [
      418
    ],
    "autoAttack": 10007,
    "img": "1b1",
    "rarity": 5,
    "evo": 3,
    "fullName": "Scathing Hierophant"
  },
  "21264": {
    "name": "Thor L",
    "stats": {
      "HP": 20007,
      "ATK": 22002,
      "DEF": 19063,
      "WIS": 10334,
      "AGI": 16518
    },
    "skills": [
      437
    ],
    "autoAttack": 10011,
    "img": "323",
    "rarity": 5,
    "evo": 3,
    "fullName": "Thor, the Roaring Thunder"
  },
  "21276": {
    "name": "Empusa",
    "stats": {
      "HP": 20706,
      "ATK": 12623,
      "DEF": 16110,
      "WIS": 20999,
      "AGI": 17510
    },
    "skills": [
      447
    ],
    "autoAttack": 10016,
    "img": "30a",
    "rarity": 5,
    "evo": 3,
    "fullName": "Empusa, the Death Scythe"
  },
  "21285": {
    "name": "Guillaume",
    "stats": {
      "HP": 21515,
      "ATK": 20887,
      "DEF": 16308,
      "WIS": 12948,
      "AGI": 18505
    },
    "skills": [
      466,
      467
    ],
    "img": "122",
    "rarity": 5,
    "evo": 3,
    "fullName": "Guillaume, Fanatic"
  },
  "21288": {
    "name": "Apep",
    "stats": {
      "HP": 20543,
      "ATK": 20975,
      "DEF": 15503,
      "WIS": 14302,
      "AGI": 16729
    },
    "skills": [
      468
    ],
    "autoAttack": 10017,
    "img": "179",
    "rarity": 5,
    "evo": 3,
    "fullName": "Apep the Chaotic"
  },
  "21291": {
    "name": "Nephthys",
    "stats": {
      "HP": 21015,
      "ATK": 11985,
      "DEF": 18202,
      "WIS": 22005,
      "AGI": 16912
    },
    "skills": [
      471,
      472
    ],
    "autoAttack": 10007,
    "img": "116",
    "rarity": 5,
    "evo": 3,
    "fullName": "Nephthys, Ruler of Death"
  },
  "21300": {
    "name": "Fate",
    "stats": {
      "HP": 20706,
      "ATK": 17848,
      "DEF": 13181,
      "WIS": 18794,
      "AGI": 17522
    },
    "skills": [
      475
    ],
    "autoAttack": 10007,
    "img": "3ee",
    "rarity": 5,
    "evo": 3,
    "fullName": "Arcanan Circle of Fate"
  },
  "21308": {
    "name": "Justice",
    "stats": {
      "HP": 20795,
      "ATK": 11717,
      "DEF": 17470,
      "WIS": 22225,
      "AGI": 18005
    },
    "skills": [
      494,
      495
    ],
    "autoAttack": 10007,
    "img": "27c",
    "rarity": 5,
    "evo": 3,
    "fullName": "Dauntless Justice"
  },
  "21312": {
    "name": "Hei Long",
    "stats": {
      "HP": 20486,
      "ATK": 13485,
      "DEF": 16192,
      "WIS": 20881,
      "AGI": 17113
    },
    "skills": [
      496
    ],
    "autoAttack": 10019,
    "img": "1bd",
    "rarity": 5,
    "evo": 3,
    "fullName": "Hei Long, the New Moon"
  },
  "21340": {
    "name": "Cetus",
    "stats": {
      "HP": 22316,
      "ATK": 20624,
      "DEF": 17579,
      "WIS": 11013,
      "AGI": 16729
    },
    "skills": [
      524
    ],
    "autoAttack": 10021,
    "img": "30a",
    "rarity": 5,
    "evo": 3,
    "fullName": "Raging Cetus"
  },
  "21352": {
    "name": "Siege Tower",
    "stats": {
      "HP": 20007,
      "ATK": 19750,
      "DEF": 16915,
      "WIS": 14021,
      "AGI": 17567
    },
    "skills": [
      548
    ],
    "autoAttack": 10029,
    "img": "293",
    "rarity": 5,
    "evo": 3,
    "fullName": "Ferocious Siege Tower"
  },
  "21368": {
    "name": "Perendon",
    "stats": {
      "HP": 19202,
      "ATK": 17300,
      "DEF": 17055,
      "WIS": 17009,
      "AGI": 17604
    },
    "skills": [
      504
    ],
    "autoAttack": 10021,
    "img": "124",
    "rarity": 5,
    "evo": 3,
    "fullName": "Perendon the Pure"
  },
  "21372": {
    "name": "Lamashtu",
    "stats": {
      "HP": 20579,
      "ATK": 17977,
      "DEF": 20007,
      "WIS": 12062,
      "AGI": 17685
    },
    "skills": [
      555
    ],
    "img": "2e5",
    "rarity": 5,
    "evo": 3,
    "fullName": "Lamashtu, Fell Goddess"
  },
  "21384": {
    "name": "Garshasp",
    "stats": {
      "HP": 22002,
      "ATK": 18058,
      "DEF": 20019,
      "WIS": 20007,
      "AGI": 8223
    },
    "skills": [
      578
    ],
    "autoAttack": 10034,
    "img": "225",
    "rarity": 5,
    "evo": 3,
    "fullName": "Garshasp, the Juggernaut"
  },
  "21404": {
    "name": "Ah Puch",
    "stats": {
      "HP": 22515,
      "ATK": 9134,
      "DEF": 18258,
      "WIS": 20999,
      "AGI": 17486
    },
    "skills": [
      585
    ],
    "autoAttack": 10007,
    "img": "460",
    "rarity": 5,
    "evo": 3,
    "fullName": "Ah Puch, Lord of Death"
  },
  "21416": {
    "name": "Mercury",
    "stats": {
      "HP": 22700,
      "ATK": 20970,
      "DEF": 18517,
      "WIS": 12020,
      "AGI": 18005
    },
    "skills": [
      814,
      815
    ],
    "img": "3d8",
    "rarity": 5,
    "evo": 3,
    "fullName": "Intrepid Hand of Mercury"
  },
  "21430": {
    "name": "Bijan",
    "stats": {
      "HP": 22189,
      "ATK": 20473,
      "DEF": 18945,
      "WIS": 11176,
      "AGI": 18083
    },
    "skills": [
      874
    ],
    "autoAttack": 10115,
    "img": "16a",
    "rarity": 5,
    "evo": 3,
    "fullName": "Bijan, the Comet"
  },
  "21433": {
    "name": "Liza",
    "stats": {
      "HP": 22491,
      "ATK": 9517,
      "DEF": 16542,
      "WIS": 21861,
      "AGI": 18011
    },
    "skills": [
      613
    ],
    "autoAttack": 10045,
    "img": "4ff",
    "rarity": 5,
    "evo": 3,
    "fullName": "Liza, Blood-Anointed"
  },
  "21445": {
    "name": "Darkwind Wyvern",
    "stats": {
      "HP": 22211,
      "ATK": 8270,
      "DEF": 19352,
      "WIS": 20917,
      "AGI": 17649
    },
    "skills": [
      607
    ],
    "autoAttack": 10042,
    "img": "4dd",
    "rarity": 5,
    "evo": 3,
    "fullName": "Darkwind Wyvern"
  },
  "21459": {
    "name": "Benjamina",
    "stats": {
      "HP": 21022,
      "ATK": 16379,
      "DEF": 20007,
      "WIS": 13006,
      "AGI": 18011
    },
    "skills": [
      640
    ],
    "img": "46a",
    "rarity": 5,
    "evo": 3,
    "fullName": "Benjamina, Wild Turkey"
  },
  "21463": {
    "name": "Kaikias",
    "stats": {
      "HP": 22014,
      "ATK": 20007,
      "DEF": 18560,
      "WIS": 12611,
      "AGI": 17742
    },
    "skills": [
      647
    ],
    "autoAttack": 10050,
    "img": "350",
    "rarity": 5,
    "evo": 3,
    "fullName": "Kaikias, the Hail God"
  },
  "21465": {
    "name": "Okypete Shd.",
    "stats": {
      "HP": 12889,
      "ATK": 10506,
      "DEF": 13084,
      "WIS": 6313,
      "AGI": 13214
    },
    "skills": [
      649
    ],
    "img": "203",
    "rarity": 4,
    "evo": 2,
    "fullName": "Okypete, the Night Breeze II"
  },
  "21474": {
    "name": "Aipaloovik",
    "stats": {
      "HP": 17006,
      "ATK": 7397,
      "DEF": 11481,
      "WIS": 17526,
      "AGI": 16605
    },
    "skills": [
      660,
      661
    ],
    "autoAttack": 10052,
    "img": "46f",
    "rarity": 4,
    "evo": 2,
    "fullName": "Aipaloovik, Sacred Dragon II"
  },
  "21475": {
    "name": "Uranus",
    "stats": {
      "HP": 21943,
      "ATK": 9529,
      "DEF": 18525,
      "WIS": 20649,
      "AGI": 17742
    },
    "skills": [
      674
    ],
    "autoAttack": 10058,
    "img": "3d5",
    "rarity": 5,
    "evo": 3,
    "fullName": "Intrepid Hand of Uranus"
  },
  "21489": {
    "name": "Poliahu",
    "stats": {
      "HP": 23572,
      "ATK": 8648,
      "DEF": 17482,
      "WIS": 22365,
      "AGI": 18202
    },
    "skills": [
      655,
      656
    ],
    "autoAttack": 10007,
    "img": "17d",
    "rarity": 5,
    "evo": 3,
    "fullName": "Poliahu, the Mauna Kea"
  },
  "21499": {
    "name": "Tyche",
    "stats": {
      "HP": 22409,
      "ATK": 9752,
      "DEF": 17534,
      "WIS": 20836,
      "AGI": 17942
    },
    "skills": [
      681
    ],
    "autoAttack": 10052,
    "img": "1b7",
    "rarity": 5,
    "evo": 3,
    "fullName": "Tyche, Goddess of Glory"
  },
  "21501": {
    "name": "Agathos",
    "stats": {
      "HP": 12163,
      "ATK": 8220,
      "DEF": 10224,
      "WIS": 13095,
      "AGI": 12315
    },
    "skills": [
      683
    ],
    "autoAttack": 10007,
    "img": "188",
    "rarity": 4,
    "evo": 2,
    "fullName": "Agathos, the Ruinous II"
  },
  "21510": {
    "name": "Botis",
    "stats": {
      "HP": 16009,
      "ATK": 14742,
      "DEF": 13994,
      "WIS": 8003,
      "AGI": 17255
    },
    "skills": [
      692,
      693
    ],
    "autoAttack": 10060,
    "img": "417",
    "rarity": 4,
    "evo": 2,
    "fullName": "Botis, Dasher of Hopes II"
  },
  "21511": {
    "name": "Venus",
    "stats": {
      "HP": 21967,
      "ATK": 19039,
      "DEF": 19982,
      "WIS": 18011,
      "AGI": 9391
    },
    "skills": [
      709
    ],
    "autoAttack": 10066,
    "img": "21d",
    "rarity": 5,
    "evo": 3,
    "fullName": "Intrepid Hand of Venus"
  },
  "21529": {
    "name": "Aso",
    "stats": {
      "HP": 19587,
      "ATK": 18851,
      "DEF": 18105,
      "WIS": 13823,
      "AGI": 18083
    },
    "skills": [
      806
    ],
    "autoAttack": 10100,
    "img": "170",
    "rarity": 5,
    "evo": 3,
    "fullName": "Aso, the Asura"
  },
  "21531": {
    "name": "Onra",
    "stats": {
      "HP": 13377,
      "ATK": 13496,
      "DEF": 12250,
      "WIS": 7516,
      "AGI": 9368
    },
    "skills": [
      808
    ],
    "img": "3b8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Onra, Ogre of Darkness II"
  },
  "21549": {
    "name": "Discordia",
    "stats": {
      "HP": 20031,
      "ATK": 18525,
      "DEF": 19831,
      "WIS": 12014,
      "AGI": 17989
    },
    "skills": [
      833
    ],
    "autoAttack": 10106,
    "img": "42a",
    "rarity": 5,
    "evo": 3,
    "fullName": "Discordia, Bringer of Ruin"
  },
  "21558": {
    "name": "Jarilo",
    "stats": {
      "HP": 20987,
      "ATK": 21955,
      "DEF": 18023,
      "WIS": 12050,
      "AGI": 17965
    },
    "skills": [
      841
    ],
    "autoAttack": 10109,
    "img": "31b",
    "rarity": 5,
    "evo": 3,
    "fullName": "Jarilo, God of Fertility"
  },
  "21560": {
    "name": "Bheara",
    "stats": {
      "HP": 13572,
      "ATK": 8426,
      "DEF": 9509,
      "WIS": 13301,
      "AGI": 11459
    },
    "skills": [
      843
    ],
    "autoAttack": 10007,
    "img": "141",
    "rarity": 4,
    "evo": 2,
    "fullName": "Bheara, Tree of Death II"
  },
  "21569": {
    "name": "Cocytus",
    "stats": {
      "HP": 16497,
      "ATK": 6248,
      "DEF": 12001,
      "WIS": 18252,
      "AGI": 16995
    },
    "skills": [
      866,
      867
    ],
    "autoAttack": 10111,
    "img": "2f0",
    "rarity": 4,
    "evo": 2,
    "fullName": "Cocytus Dragon II"
  },
  "21571": {
    "name": "Rattlebolt",
    "stats": {
      "HP": 19552,
      "ATK": 11502,
      "DEF": 20007,
      "WIS": 20999,
      "AGI": 18221
    },
    "skills": [
      861,
      862
    ],
    "autoAttack": 10110,
    "img": "35b",
    "rarity": 5,
    "evo": 3,
    "fullName": "Rattlebolt Wyvern"
  },
  "21578": {
    "name": "Zeruel",
    "stats": {
      "HP": 22841,
      "ATK": 21478,
      "DEF": 18303,
      "WIS": 12038,
      "AGI": 18128
    },
    "skills": [
      954,
      955
    ],
    "autoAttack": 10015,
    "img": "3b0",
    "rarity": 5,
    "evo": 3,
    "fullName": "Zeruel Angel of War, Swap"
  },
  "21588": {
    "name": "Apate",
    "stats": {
      "HP": 21266,
      "ATK": 9647,
      "DEF": 18128,
      "WIS": 21466,
      "AGI": 17977
    },
    "skills": [
      882
    ],
    "autoAttack": 10118,
    "img": "111",
    "rarity": 5,
    "evo": 3,
    "fullName": "Apate, Goddess of Deceit"
  },
  "21590": {
    "name": "Lenore",
    "stats": {
      "HP": 13182,
      "ATK": 9455,
      "DEF": 12120,
      "WIS": 8404,
      "AGI": 12900
    },
    "skills": [
      884
    ],
    "img": "271",
    "rarity": 4,
    "evo": 2,
    "fullName": "Lenore, the False II"
  },
  "21599": {
    "name": "Mammi EP2",
    "stats": {
      "HP": 15500,
      "ATK": 5663,
      "DEF": 12987,
      "WIS": 18696,
      "AGI": 17407
    },
    "skills": [
      898,
      899
    ],
    "autoAttack": 10052,
    "img": "46a",
    "rarity": 4,
    "evo": 2,
    "fullName": "Mammi, Hare of the Harvest II"
  },
  "21608": {
    "name": "Neptune",
    "stats": {
      "HP": 20461,
      "ATK": 10404,
      "DEF": 17836,
      "WIS": 21674,
      "AGI": 18023
    },
    "skills": [
      911
    ],
    "autoAttack": 10057,
    "img": "349",
    "rarity": 5,
    "evo": 3,
    "fullName": "Intrepid Hand of Neptune"
  },
  "21615": {
    "name": "Zaphkiel",
    "stats": {
      "HP": 22700,
      "ATK": 7602,
      "DEF": 17657,
      "WIS": 22190,
      "AGI": 18318
    },
    "skills": [
      1273,
      1274
    ],
    "autoAttack": 10007,
    "img": "27e",
    "rarity": 5,
    "evo": 3,
    "fullName": "Zaphkiel, the Blessed Rain"
  },
  "21618": {
    "name": "Isabella",
    "stats": {
      "HP": 21538,
      "ATK": 21062,
      "DEF": 20795,
      "WIS": 11217,
      "AGI": 18098
    },
    "skills": [
      1054,
      1055
    ],
    "img": "46a",
    "rarity": 5,
    "evo": 3,
    "fullName": "Isabella, the Waterbrand"
  },
  "21625": {
    "name": "Belial",
    "stats": {
      "HP": 21873,
      "ATK": 19096,
      "DEF": 20100,
      "WIS": 9309,
      "AGI": 18105
    },
    "skills": [
      918
    ],
    "autoAttack": 10044,
    "img": "3af",
    "rarity": 5,
    "evo": 3,
    "fullName": "Belial, Lord of Vices"
  },
  "21627": {
    "name": "Charon",
    "stats": {
      "HP": 13680,
      "ATK": 8285,
      "DEF": 9585,
      "WIS": 14503,
      "AGI": 9964
    },
    "skills": [
      920
    ],
    "autoAttack": 10007,
    "img": "430",
    "rarity": 4,
    "evo": 2,
    "fullName": "Charon, Darksun Ferryman II"
  },
  "21636": {
    "name": "Moloch",
    "stats": {
      "HP": 15002,
      "ATK": 8003,
      "DEF": 12987,
      "WIS": 16800,
      "AGI": 17201
    },
    "skills": [
      942,
      943
    ],
    "autoAttack": 10128,
    "img": "1e8",
    "rarity": 4,
    "evo": 2,
    "fullName": "Moloch, Soul Reaper II"
  },
  "21645": {
    "name": "Tangata M",
    "stats": {
      "HP": 20031,
      "ATK": 21103,
      "DEF": 21920,
      "WIS": 9729,
      "AGI": 18105
    },
    "skills": [
      957
    ],
    "autoAttack": 10132,
    "img": "284",
    "rarity": 5,
    "evo": 3,
    "fullName": "Tangata Manu, Withering Gale"
  },
  "21658": {
    "name": "Oenone",
    "stats": {
      "HP": 22132,
      "ATK": 9543,
      "DEF": 17878,
      "WIS": 22445,
      "AGI": 18273
    },
    "skills": [
      1006,
      1007
    ],
    "autoAttack": 10121,
    "img": "1a3",
    "rarity": 5,
    "evo": 3,
    "fullName": "Oenone, the Hailstorm"
  },
  "21670": {
    "name": "Urcagu",
    "stats": {
      "HP": 22527,
      "ATK": 22048,
      "DEF": 19668,
      "WIS": 8912,
      "AGI": 17779
    },
    "skills": [
      964
    ],
    "autoAttack": 10108,
    "img": "1bc",
    "rarity": 5,
    "evo": 3,
    "fullName": "Urcagu, the Grinder"
  },
  "21672": {
    "name": "Unbound",
    "stats": {
      "HP": 13788,
      "ATK": 13138,
      "DEF": 10896,
      "WIS": 8003,
      "AGI": 10343
    },
    "skills": [
      966
    ],
    "img": "35b",
    "rarity": 4,
    "evo": 2,
    "fullName": "Unbound Gem Golem II"
  },
  "21681": {
    "name": "Mizuchi",
    "stats": {
      "HP": 14698,
      "ATK": 6097,
      "DEF": 14005,
      "WIS": 17797,
      "AGI": 17407
    },
    "skills": [
      977,
      978
    ],
    "autoAttack": 10136,
    "img": "312",
    "rarity": 4,
    "evo": 2,
    "fullName": "Mizuchi, the Raging Storm II"
  },
  "21690": {
    "name": "Decaying",
    "stats": {
      "HP": 19982,
      "ATK": 9075,
      "DEF": 18969,
      "WIS": 22316,
      "AGI": 18152
    },
    "skills": [
      991
    ],
    "autoAttack": 10140,
    "img": "12e",
    "rarity": 5,
    "evo": 3,
    "fullName": "Decaying Dragon"
  },
  "21696": {
    "name": "Ker",
    "stats": {
      "HP": 21015,
      "ATK": 19040,
      "DEF": 18585,
      "WIS": 13100,
      "AGI": 18517
    },
    "skills": [
      972,
      973
    ],
    "autoAttack": 10134,
    "img": "233",
    "rarity": 5,
    "evo": 3,
    "fullName": "Ker, the Despair Diamond"
  },
  "21698": {
    "name": "Dionysus",
    "stats": {
      "HP": 23893,
      "ATK": 10008,
      "DEF": 23600,
      "WIS": 22982,
      "AGI": 8013
    },
    "skills": [
      1037,
      1038
    ],
    "autoAttack": 10148,
    "img": "1e3",
    "rarity": 5,
    "evo": 3,
    "fullName": "Dionysus, the Reveler"
  },
  "21707": {
    "name": "Erupting Golem",
    "stats": {
      "HP": 20742,
      "ATK": 18258,
      "DEF": 19856,
      "WIS": 12423,
      "AGI": 17218
    },
    "skills": [
      1254,
      1255
    ],
    "img": "234",
    "rarity": 5,
    "evo": 3,
    "fullName": "Erupting Golem"
  },
  "21729": {
    "name": "Menelaus",
    "stats": {
      "HP": 22446,
      "ATK": 17883,
      "DEF": 23414,
      "WIS": 17989,
      "AGI": 6719
    },
    "skills": [
      998
    ],
    "autoAttack": 10144,
    "img": "3f4",
    "rarity": 5,
    "evo": 3,
    "fullName": "Menelaus, Vengeful King"
  },
  "21731": {
    "name": "Siege Horse",
    "stats": {
      "HP": 13442,
      "ATK": 8101,
      "DEF": 9282,
      "WIS": 14200,
      "AGI": 11069
    },
    "skills": [
      1000
    ],
    "img": "20c",
    "rarity": 4,
    "evo": 2,
    "fullName": "Dark-Imbued Siege Horse II"
  },
  "21740": {
    "name": "Ravaging Hafgufa",
    "stats": {
      "HP": 16800,
      "ATK": 17244,
      "DEF": 14005,
      "WIS": 5999,
      "AGI": 17201
    },
    "skills": [
      1021,
      1022
    ],
    "autoAttack": 10133,
    "img": "392",
    "rarity": 4,
    "evo": 2,
    "fullName": "Ravaging Hafgufa II"
  },
  "21741": {
    "name": "Walutahanga",
    "stats": {
      "HP": 22503,
      "ATK": 8235,
      "DEF": 17579,
      "WIS": 22048,
      "AGI": 18105
    },
    "skills": [
      1045,
      1046
    ],
    "autoAttack": 10126,
    "img": "28b",
    "rarity": 5,
    "evo": 3,
    "fullName": "Walutahanga, Guardian Dragon"
  },
  "21743": {
    "name": "Vepar",
    "stats": {
      "HP": 13344,
      "ATK": 13604,
      "DEF": 11026,
      "WIS": 7548,
      "AGI": 10777
    },
    "skills": [
      1048
    ],
    "img": "438",
    "rarity": 4,
    "evo": 2,
    "fullName": "Vepar, the Perpetual Night II"
  },
  "21748": {
    "name": "Bastet",
    "stats": {
      "HP": 23015,
      "ATK": 12008,
      "DEF": 22038,
      "WIS": 23015,
      "AGI": 8405
    },
    "skills": [
      1058,
      1059
    ],
    "autoAttack": 10001,
    "img": "4f4",
    "rarity": 5,
    "evo": 3,
    "fullName": "Bastet, Cat Goddess"
  },
  "21752": {
    "name": "Petsuchos",
    "stats": {
      "HP": 16518,
      "ATK": 18490,
      "DEF": 13994,
      "WIS": 5447,
      "AGI": 16800
    },
    "skills": [
      1061,
      1062
    ],
    "autoAttack": 10011,
    "img": "472",
    "rarity": 4,
    "evo": 2,
    "fullName": "Petsuchos Minister II"
  },
  "21761": {
    "name": "Hellscale",
    "stats": {
      "HP": 20787,
      "ATK": 21384,
      "DEF": 20088,
      "WIS": 11221,
      "AGI": 17510
    },
    "skills": [
      1077,
      1078
    ],
    "autoAttack": 10157,
    "img": "35a",
    "rarity": 5,
    "evo": 3,
    "fullName": "Hellscale Theropod"
  },
  "21777": {
    "name": "Demonblade",
    "stats": {
      "HP": 21454,
      "ATK": 9110,
      "DEF": 18186,
      "WIS": 21674,
      "AGI": 18046
    },
    "skills": [
      1085,
      1086
    ],
    "autoAttack": 10007,
    "img": "1f4",
    "rarity": 5,
    "evo": 3,
    "fullName": "Demonblade Countess"
  },
  "21779": {
    "name": "Hagen",
    "stats": {
      "HP": 12759,
      "ATK": 13615,
      "DEF": 13312,
      "WIS": 6313,
      "AGI": 10311
    },
    "skills": [
      1088
    ],
    "img": "447",
    "rarity": 4,
    "evo": 2,
    "fullName": "Hagen, Mad King II"
  },
  "21784": {
    "name": "Long Nu",
    "stats": {
      "HP": 20492,
      "ATK": 6522,
      "DEF": 21003,
      "WIS": 22480,
      "AGI": 17982
    },
    "skills": [
      1107,
      1108
    ],
    "autoAttack": 10001,
    "img": "11f",
    "rarity": 5,
    "evo": 3,
    "fullName": "Long Nu, Sea Princess"
  },
  "21788": {
    "name": "Qiong Qi",
    "stats": {
      "HP": 17591,
      "ATK": 13398,
      "DEF": 14503,
      "WIS": 8047,
      "AGI": 17710
    },
    "skills": [
      1110,
      1111
    ],
    "autoAttack": 10011,
    "img": "305",
    "rarity": 4,
    "evo": 2,
    "fullName": "Qiong Qi, World Eater II"
  },
  "21790": {
    "name": "Zhu Rong",
    "stats": {
      "HP": 23340,
      "ATK": 7660,
      "DEF": 18190,
      "WIS": 22817,
      "AGI": 18318
    },
    "skills": [
      1094,
      1095
    ],
    "autoAttack": 10164,
    "img": "295",
    "rarity": 5,
    "evo": 3,
    "fullName": "Zhu Rong, the Blazing Storm"
  },
  "21797": {
    "name": "Cu Chulainn",
    "stats": {
      "HP": 20754,
      "ATK": 19681,
      "DEF": 20170,
      "WIS": 12283,
      "AGI": 18105
    },
    "skills": [
      1125,
      1126
    ],
    "autoAttack": 10168,
    "img": "44b",
    "rarity": 5,
    "evo": 3,
    "fullName": "Cu Chulainn, the Thunderbolt"
  },
  "21822": {
    "name": "Samhain",
    "stats": {
      "HP": 21767,
      "ATK": 18770,
      "DEF": 17836,
      "WIS": 14021,
      "AGI": 18105
    },
    "skills": [
      1133,
      1134
    ],
    "autoAttack": 10151,
    "img": "3ae",
    "rarity": 5,
    "evo": 3,
    "fullName": "Samhain, Night Trampler"
  },
  "21824": {
    "name": "Cursed Pumpkin",
    "stats": {
      "HP": 12857,
      "ATK": 8534,
      "DEF": 9054,
      "WIS": 13897,
      "AGI": 11156
    },
    "skills": [
      1136
    ],
    "autoAttack": 10007,
    "img": "27e",
    "rarity": 4,
    "evo": 2,
    "fullName": "Cursed Pumpkin Golem II"
  },
  "21829": {
    "name": "Fell Bonedrake",
    "stats": {
      "HP": 20375,
      "ATK": 24595,
      "DEF": 18505,
      "WIS": 9300,
      "AGI": 18202
    },
    "skills": [
      1155,
      1156
    ],
    "autoAttack": 10011,
    "img": "438",
    "rarity": 5,
    "evo": 3,
    "fullName": "Fell Bonedrake Knight"
  },
  "21833": {
    "name": "Twar",
    "stats": {
      "HP": 15543,
      "ATK": 5143,
      "DEF": 13702,
      "WIS": 18122,
      "AGI": 17493
    },
    "skills": [
      1146,
      1147
    ],
    "autoAttack": 10173,
    "img": "331",
    "rarity": 4,
    "evo": 2,
    "fullName": "Twar, the Moonlit Night II"
  },
  "21842": {
    "name": "Infernal Wyrm Warden",
    "stats": {
      "HP": 21034,
      "ATK": 7687,
      "DEF": 20031,
      "WIS": 21592,
      "AGI": 18152
    },
    "skills": [
      1166,
      1167
    ],
    "autoAttack": 10001,
    "img": "206",
    "rarity": 5,
    "evo": 3,
    "fullName": "Infernal Wyrm Warden"
  },
  "21848": {
    "name": "Fenrir",
    "stats": {
      "HP": 22073,
      "ATK": 19760,
      "DEF": 19400,
      "WIS": 13123,
      "AGI": 18425
    },
    "skills": [
      1143,
      1144
    ],
    "img": "1c3",
    "rarity": 5,
    "evo": 3,
    "fullName": "Fenrir, Vengeful Beast"
  },
  "21850": {
    "name": "Lucifuge",
    "stats": {
      "HP": 21278,
      "ATK": 8712,
      "DEF": 18221,
      "WIS": 22130,
      "AGI": 18140
    },
    "skills": [
      1175,
      1176
    ],
    "autoAttack": 10007,
    "img": "3bb",
    "rarity": 5,
    "evo": 3,
    "fullName": "Lucifuge, Infernal Premier"
  },
  "21852": {
    "name": "Nicola",
    "stats": {
      "HP": 12900,
      "ATK": 11611,
      "DEF": 11459,
      "WIS": 7960,
      "AGI": 12066
    },
    "skills": [
      1179
    ],
    "autoAttack": 10005,
    "img": "298",
    "rarity": 4,
    "evo": 2,
    "fullName": "Nicola, Corpse Handler II"
  },
  "21857": {
    "name": "Jupiter",
    "stats": {
      "HP": 22538,
      "ATK": 24050,
      "DEF": 18017,
      "WIS": 8125,
      "AGI": 18250
    },
    "skills": [
      1197,
      1198
    ],
    "img": "113",
    "rarity": 5,
    "evo": 3,
    "fullName": "Intrepid Hand of Jupiter"
  },
  "21861": {
    "name": "Caim",
    "stats": {
      "HP": 16085,
      "ATK": 16908,
      "DEF": 13799,
      "WIS": 6010,
      "AGI": 17201
    },
    "skills": [
      1200,
      1201
    ],
    "img": "3c3",
    "rarity": 4,
    "evo": 2,
    "fullName": "Caim, Death Seeker II"
  },
  "21862": {
    "name": "Diana",
    "stats": {
      "HP": 20380,
      "ATK": 20194,
      "DEF": 19493,
      "WIS": 10208,
      "AGI": 18221
    },
    "skills": [
      1214,
      1215
    ],
    "autoAttack": 10103,
    "img": "26d",
    "rarity": 5,
    "evo": 3,
    "fullName": "Diana, the Crescent Moon"
  },
  "21877": {
    "name": "Renenet",
    "stats": {
      "HP": 23328,
      "ATK": 10078,
      "DEF": 16343,
      "WIS": 22132,
      "AGI": 18365
    },
    "skills": [
      1185,
      1186
    ],
    "autoAttack": 10007,
    "img": "2cb",
    "rarity": 5,
    "evo": 3,
    "fullName": "Renenet, Goddess of Wealth"
  },
  "21878": {
    "name": "Befana",
    "stats": {
      "HP": 22153,
      "ATK": 8586,
      "DEF": 17498,
      "WIS": 22048,
      "AGI": 18105
    },
    "skills": [
      1224,
      1225
    ],
    "autoAttack": 10007,
    "img": "3d1",
    "rarity": 5,
    "evo": 3,
    "fullName": "Befana, the Moonless Night"
  },
  "21880": {
    "name": "Yule",
    "stats": {
      "HP": 13052,
      "ATK": 13203,
      "DEF": 10733,
      "WIS": 7364,
      "AGI": 11589
    },
    "skills": [
      1227
    ],
    "autoAttack": 10005,
    "img": "1a3",
    "rarity": 4,
    "evo": 2,
    "fullName": "Yule Goat, the Blood-Stained II"
  },
  "21885": {
    "name": "Virginal",
    "stats": {
      "HP": 23688,
      "ATK": 8160,
      "DEF": 17865,
      "WIS": 22340,
      "AGI": 18342
    },
    "skills": [
      1233,
      1234
    ],
    "autoAttack": 10057,
    "img": "2c8",
    "rarity": 5,
    "evo": 3,
    "fullName": "Virginal, Ice Queen"
  },
  "21886": {
    "name": "Skadi",
    "stats": {
      "HP": 21655,
      "ATK": 24515,
      "DEF": 19052,
      "WIS": 7533,
      "AGI": 18225
    },
    "skills": [
      1235,
      1236
    ],
    "img": "274",
    "rarity": 5,
    "evo": 3,
    "fullName": "Skadi, Goddess of Winter"
  },
  "21890": {
    "name": "Chuchunya",
    "stats": {
      "HP": 17309,
      "ATK": 19422,
      "DEF": 11492,
      "WIS": 5533,
      "AGI": 17493
    },
    "skills": [
      1238,
      1239
    ],
    "img": "31d",
    "rarity": 4,
    "evo": 2,
    "fullName": "Chuchunya, Tundra Guardian II"
  },
  "21902": {
    "name": "Amphion",
    "stats": {
      "HP": 23414,
      "ATK": 9122,
      "DEF": 15352,
      "WIS": 22364,
      "AGI": 18186
    },
    "skills": [
      1264,
      1265
    ],
    "autoAttack": 10007,
    "img": "136",
    "rarity": 5,
    "evo": 3,
    "fullName": "Amphion, Hymn of Death"
  },
  "21904": {
    "name": "Marsyas",
    "stats": {
      "HP": 12033,
      "ATK": 8935,
      "DEF": 10582,
      "WIS": 13463,
      "AGI": 11665
    },
    "skills": [
      1267
    ],
    "autoAttack": 10007,
    "img": "348",
    "rarity": 4,
    "evo": 2,
    "fullName": "Marsyas, Calamity Caller II"
  },
  "21914": {
    "name": "Taromaiti",
    "stats": {
      "HP": 17385,
      "ATK": 18241,
      "DEF": 12033,
      "WIS": 6151,
      "AGI": 17309
    },
    "skills": [
      1276,
      1277
    ],
    "img": "1bd",
    "rarity": 4,
    "evo": 2,
    "fullName": "Taromaiti, Fallen Goddess II"
  },
  "41068": {
    "name": "Valafar",
    "stats": {
      "HP": 20005,
      "ATK": 8007,
      "DEF": 13710,
      "WIS": 22024,
      "AGI": 17212
    },
    "skills": [],
    "autoAttack": 10007,
    "img": "168",
    "rarity": 5,
    "evo": 1,
    "fullName": "Valafar, Inferno Vanquisher"
  },
  "41173": {
    "name": "Tarasca",
    "stats": {
      "HP": 22911,
      "ATK": 17998,
      "DEF": 20476,
      "WIS": 8002,
      "AGI": 13503
    },
    "skills": [],
    "img": "49b",
    "rarity": 5,
    "evo": 1,
    "fullName": "Adamant Tarasca"
  },
  "41659": {
    "name": "Ilya",
    "stats": {
      "HP": 19655,
      "ATK": 19943,
      "DEF": 17687,
      "WIS": 8028,
      "AGI": 18186
    },
    "skills": [],
    "img": "260",
    "rarity": 5,
    "evo": 1,
    "fullName": "Ilya, Giant Slayer"
  },
  "41726": {
    "name": "Haagenti",
    "stats": {
      "HP": 21813,
      "ATK": 20148,
      "DEF": 16179,
      "WIS": 6523,
      "AGI": 18386
    },
    "skills": [],
    "img": "4cb",
    "rarity": 5,
    "evo": 1,
    "fullName": "Haagenti, Lord of Beasts"
  },
  "41806": {
    "name": "Crom",
    "stats": {
      "HP": 21213,
      "ATK": 6017,
      "DEF": 16987,
      "WIS": 21505,
      "AGI": 18112
    },
    "skills": [],
    "autoAttack": 10016,
    "img": "3b3",
    "rarity": 5,
    "evo": 1,
    "fullName": "Crom Cruach, the Silver Moon"
  }
}