var Formation = (function () {
    function Formation(type) {
        this.type = type;
    }
    Formation.initialize = function () {
        Formation.FORMATION_CONFIG[ENUM.FormationType.SKEIN_5] = [3, 2, 1, 2, 3];
        Formation.FORMATION_CONFIG[ENUM.FormationType.VALLEY_5] = [1, 2, 3, 2, 1];
        Formation.FORMATION_CONFIG[ENUM.FormationType.TOOTH_5] = [1, 3, 1, 3, 1];
        Formation.FORMATION_CONFIG[ENUM.FormationType.WAVE_5] = [3, 1, 2, 1, 3];
        Formation.FORMATION_CONFIG[ENUM.FormationType.FRONT_5] = [1, 1, 1, 1, 1];
        Formation.FORMATION_CONFIG[ENUM.FormationType.MID_5] = [2, 2, 2, 2, 2];
        Formation.FORMATION_CONFIG[ENUM.FormationType.REAR_5] = [3, 3, 3, 3, 3];
        Formation.FORMATION_CONFIG[ENUM.FormationType.PIKE_5] = [3, 3, 1, 3, 3];
        Formation.FORMATION_CONFIG[ENUM.FormationType.SHIELD_5] = [1, 1, 3, 1, 1];
        Formation.FORMATION_CONFIG[ENUM.FormationType.PINCER_5] = [3, 1, 3, 1, 3];
        Formation.FORMATION_CONFIG[ENUM.FormationType.SAW_5] = [1, 3, 2, 3, 1];
        Formation.FORMATION_CONFIG[ENUM.FormationType.HYDRA_5] = [3, 3, 1, 1, 1];
        Formation.UNI_PROC_ORDER[ENUM.FormationRow.FRONT] = [11, 12, 13, 14, 15];
        Formation.UNI_PROC_ORDER[ENUM.FormationRow.MID] = [6, 7, 8, 9, 10];
        Formation.UNI_PROC_ORDER[ENUM.FormationRow.REAR] = [1, 2, 3, 4, 5];
        return null;
    };
    Formation.getProcIndex = function (row, column) {
        return Formation.UNI_PROC_ORDER[row][column];
    };
    Formation.prototype.getCardRow = function (position) {
        return Formation.FORMATION_CONFIG[this.type][position];
    };
    Formation.prototype.getFormationConfig = function () {
        return Formation.FORMATION_CONFIG[this.type];
    };
    Formation.FORMATION_CONFIG = {};
    Formation.UNI_PROC_ORDER = {};
    Formation.whyfoo = Formation.initialize();
    return Formation;
}());