class Player {
	constructor(name){
		this.id = generateRandomNumber(1);
		this.name = name;
        this.formation = generateRandomNumber(1,13);
        this.cards = {"main" : generateRandomCards(5), "reserve" : generateRandomCards(5)}
	}
}

var p1 = new Player('Player one');

var p2 = new Player('Player two');

const players = {"p1": p1,"p2": p2};


// card manager instantce
const CMI = new CardManager2; 


class BattleModel {

    constructor(players){
        this.players = players;
        this.cardManager = CMI.getInstance();

        this.isFinished = false;
        this.playerWon = null;
        this.p1_mainCards = this.players.p1.cards.main;
        this.p2_mainCards = this.players.p2.cards.main;
        this.p1_originalMainCards = this.players.p1.cards.main;
        this.p2_originalMainCards = this.players.p2.cards.main;
        this.allCurrentMainCards = [...this.players.p1.cards.main,...this.players.p2.cards.main];
        this.allCardsById = {};
        this.onDeathCards = [];
        this.turnOrderBase = ENUM.BattleTurnOrderType.AGI;
        this.turnOrderChangeEffectiveTurns = 0;
        this.turnOrderChanged = false;

        if (BattleModel._instance) {
            throw new Error("Error: Instantiation failed: Use getInstance() instead of new.");
        }
        BattleModel._instance = this;

        
    }

    getInstance = function () {
        if (BattleModel._instance === null) {
            throw new Error("Error: you should not make this object this way");
        }
        return BattleModel._instance;
    };
    resetAll = function () {
        this.removeInstance();
        CMI.removeInstance();
        console.log("battle model was reset");
    };
    removeInstance = function () {
        BattleModel._instance = null;
    };
}

const battle = new BattleModel(players);

function playGame() {
    // prepareField();
    // BattleGraphic.PLAY_MODE = 'AUTO';
    // BattleDebugger.IS_DEBUG_MODE = false;
    // document.getElementById('startButton').onclick = function () {
    //     this.disabled = true;
    //     if (ENUM.Setting.IS_MOBILE) {
    //         this.style.display = "none";
    //     }
    //     BattleGraphic.getInstance().resetInitialField();
    //     BattleGraphic.getInstance().displayMajorEventAnimation(0);
    // };
    // var dataOption = getBattleDataOption();
    // var data = dataOption[0], option = dataOption[1];
    var newGame = new BattleModel(data, option);
    newGame.startBattle();
}



// CardManager.prototype.getPlayerCurrentMainCardsByProcOrder = function (player) {
//     var playerCards = this.getPlayerCurrentMainCards(player);
//     var copy = [];
//     for (var i = 0; i < playerCards.length; i++) {
//         copy.push(playerCards[i]);
//     }
//     copy.sort(function (a, b) { return a.procIndex - b.procIndex; });
//     return copy;
// };

// BattleModel.prototype.performOpeningSkills = function () {
//     var p1cards = this.cardManager.getPlayerCurrentMainCardsByProcOrder(this.player1);
//     var p2cards = this.cardManager.getPlayerCurrentMainCardsByProcOrder(this.player2);
//     for (var i = 0; i < p1cards.length; i++) {
//         var skill1 = p1cards[i].getRandomOpeningSkill();
//         if (skill1) {
//             var data = {
//                 executor: p1cards[i],
//                 skill: skill1
//             };
//             if (skill1.willBeExecuted(data)) {
//                 this.logger.addMajorEvent({
//                     description: p1cards[i].name + " procs " + skill1.name,
//                     executorId: p1cards[i].id,
//                     skillId: skill1.id
//                 });
//                 skill1.execute(data);
//             }
//         }
//     }
//     for (var i = 0; i < p2cards.length; i++) {
//         var skill2 = p2cards[i].getRandomOpeningSkill();
//         if (skill2) {
//             data = {
//                 executor: p2cards[i],
//                 skill: skill2
//             };
//             if (skill2.willBeExecuted(data)) {
//                 this.logger.addMajorEvent({
//                     description: p2cards[i].name + " procs " + skill2.name,
//                     executorId: p2cards[i].id,
//                     skillId: skill2.id
//                 });
//                 skill2.execute(data);
//             }
//         }
//     }
//     this.turnOrderChanged = false;
// };

function getPlayerCurrentMainCardsByProcOrder(player){
    var playersCards = CMI.getPlayersCurrentMainCards(player);
    var copy = [];
    for (var i = 0; i < playersCards.length; i++) {
        copy.push(playersCards[i]);
    }
    copy.sort(function (a, b) { return a.procIndex - b.procIndex; });
    return copy;
}

function generateRandomNumber(amountOfNumbersNeeded, amountOfNumbers){
    if (amountOfNumbers === undefined) {
        amountOfNumbers = 100;
    }
    if (amountOfNumbersNeeded > 1) {
        var numbers = [];
        while(numbers.length < amountOfNumbersNeeded){
            var r = Math.floor(Math.random() * amountOfNumbers) + 1;
            if(numbers.indexOf(r) === -1) numbers.push(r);
        }
    }else{
        var numbers = 0;
        var numbers = Math.floor(Math.random() * amountOfNumbers) + 1;
    }
    return numbers;
}


function generateRandomCards(amountOfCards){
    var cards = [];
    while(cards.length < amountOfCards){
        var keys = Object.keys(cardDatabase);
        var r = cardDatabase[keys[ keys.length * Math.random() << 0]];
        if(cards.indexOf(r) === -1) cards.push(r);
    }
    return cards;
}


// CardManager.prototype.getPlayerCurrentMainCardsByProcOrder = function (player) {
//     var playerCards = this.getPlayerCurrentMainCards(player);
//     var copy = [];
//     for (var i = 0; i < playerCards.length; i++) {
//         copy.push(playerCards[i]);
//     }
//     copy.sort(function (a, b) { return a.procIndex - b.procIndex; });
//     return copy;
// };
	
// var league = new player(2, "Player 2", "mdh", 2);

// console.log(league)

// BattleModel.prototype.startBattle = function () {
//     this.logger.startBattleLog();
//     this.performOpeningSkills();
//     while (!this.isFinished) {
//         this.logger.currentTurn++;
//         if (this.turnOrderChangeEffectiveTurns === 0) {
//             this.turnOrderBase = ENUM.BattleTurnOrderType.AGI;
//         }
//         else {
//             this.turnOrderChangeEffectiveTurns--;
//         }
//         this.cardManager.updateAllCurrentMainCards();
//         this.cardManager.sortAllCurrentMainCards();
//         for (var i = 0; i < 10 && !this.isFinished; i++) {
//             var currentCard = this.allCurrentMainCards[i];
//             this.currentPlayer = currentCard.player;
//             this.currentPlayerMainCards = this.cardManager.getPlayerCurrentMainCards(this.currentPlayer);
//             this.currentPlayerReserveCards = this.cardManager.getPlayerCurrentReserveCards(this.currentPlayer);
//             this.oppositePlayer = this.getOppositePlayer(this.currentPlayer);
//             this.oppositePlayerMainCards = this.cardManager.getPlayerCurrentMainCards(this.oppositePlayer);
//             this.oppositePlayerReserveCards = this.cardManager.getPlayerCurrentReserveCards(this.oppositePlayer);
//             if (currentCard.isDead) {
//                 var column = currentCard.formationColumn;
//                 if (this.isBloodClash && this.currentPlayerReserveCards[column]) {
//                     var reserveCard = this.currentPlayerReserveCards[column];
//                     this.cardManager.switchCardInAllCurrentMainCards(currentCard, reserveCard);
//                     this.currentPlayerMainCards[column] = reserveCard;
//                     this.currentPlayerReserveCards[column] = null;
//                     this.logger.addMajorEvent({
//                         description: currentCard.name + " is switched by " + reserveCard.name,
//                     });
//                     this.logger.addMinorEvent({
//                         description: currentCard.name + " is switched by " + reserveCard.name,
//                         type: ENUM.MinorEventType.RESERVE_SWITCH,
//                         reserveSwitch: {
//                             mainId: currentCard.id,
//                             reserveId: reserveCard.id
//                         }
//                     });
//                     currentCard = reserveCard;
//                     var openingSkill = currentCard.getRandomOpeningSkill();
//                     if (openingSkill) {
//                         var data = {
//                             executor: currentCard,
//                             skill: openingSkill
//                         };
//                         if (openingSkill.willBeExecuted(data)) {
//                             this.logger.addMajorEvent({
//                                 description: currentCard.name + " procs " + openingSkill.name,
//                                 executorId: currentCard.id,
//                                 skillId: openingSkill.id
//                             });
//                             openingSkill.execute(data);
//                         }
//                     }
//                 }
//                 else {
//                     continue;
//                 }
//             }
//             var missTurn = !currentCard.canAttack();
//             if (missTurn) {
//                 this.logger.addMajorEvent({
//                     description: currentCard.name + " missed a turn"
//                 });
//             }
//             this.processActivePhase(currentCard, "FIRST");
//             if (this.isFinished)
//                 break;
//             var passiveSkill = currentCard.getPassiveSkill();
//             if (!currentCard.isDead && currentCard.status.willAttackAgain !== 0) {
//                 this.processActivePhase(currentCard, "FIRST");
//                 currentCard.status.willAttackAgain = 0;
//                 if (this.isFinished)
//                     break;
//             }
//             else if (!currentCard.isDead && passiveSkill && passiveSkill.skillFunc === ENUM.SkillFunc.EXTRA_TURN_PASSIVE) {
//                 if (passiveSkill.willBeExecuted({ executor: currentCard, skill: passiveSkill })) {
//                     this.logger.addMinorEvent({
//                         description: currentCard.name + " gets an extra turn!",
//                         type: ENUM.MinorEventType.TEXT
//                     });
//                     this.processActivePhase(currentCard, "FIRST");
//                     if (this.isFinished)
//                         break;
//                 }
//             }
//             if (!currentCard.isDead) {
//                 if (currentCard.getAfflictionType() !== ENUM.AfflictionType.BURN) {
//                     var cured = currentCard.updateAffliction();
//                 }
//                 if (!currentCard.affliction && cured) {
//                     var desc = currentCard.name + " is cured of affliction!";
//                     this.logger.addMinorEvent({
//                         targetId: currentCard.id,
//                         type: ENUM.MinorEventType.AFFLICTION,
//                         affliction: {
//                             type: currentCard.getAfflictionType(),
//                             isFinished: true,
//                         },
//                         description: desc,
//                     });
//                 }
//                 this.processOnDeathPhase();
//             }
//             this.checkFinish();
//         }
//         if (!this.isFinished) {
//             this.processEndTurn();
//         }
//     }
//     if (BattleDebugger.IS_DEBUG_MODE) {
//         BattleDebugger.getInstance().displayDebugger();
//     }
//     return this;
// };




// function Battle() {

// 	function BattleModel(data) {
//         this.isFinished = false;
//         this.playerWon = null;
//         this.p1_mainCards = [];
//         this.p2_mainCards = [];
//         this.p1_reserveCards = [];
//         this.p2_reserveCards = [];
//         this.allCurrentMainCards = [];

//         if (BattleModel._instance) {
//             throw new Error("Error: Instantiation failed: Use getInstance() instead of new.");
//         }
//         BattleModel._instance = this;
        
//         this.cardManager = CardManager.getInstance();

//         this.cardManager.sortAllCurrentMainCards();
//         if (!BattleDebugger.IS_DEBUG_MODE) {
//             this.logger.displayInfoText();
//             this.logger.displayWarningText();
//         }
//     }

// 	function startBattle () {

//         this.performOpeningSkills();

//         while (!this.isFinished) {
//             this.logger.currentTurn++;

//             // process turn order change
//             if (this.turnOrderChangeEffectiveTurns === 0) {
//                 this.turnOrderBase = ENUM.BattleTurnOrderType.AGI;
//             }
//             else {
//                 this.turnOrderChangeEffectiveTurns--;
//             }

//             this.cardManager.updateAllCurrentMainCards();
//             this.cardManager.sortAllCurrentMainCards();

//             // assuming both have 5 cards
//             for (var i = 0; i < 10 && !this.isFinished; i++) {
//                 var currentCard = this.allCurrentMainCards[i];

//                 this.currentPlayer = currentCard.player;
//                 this.currentPlayerMainCards = this.cardManager.getPlayerCurrentMainCards(this.currentPlayer);
//                 this.currentPlayerReserveCards = this.cardManager.getPlayerCurrentReserveCards(this.currentPlayer);

//                 this.oppositePlayer = this.getOppositePlayer(this.currentPlayer);
//                 this.oppositePlayerMainCards = this.cardManager.getPlayerCurrentMainCards(this.oppositePlayer);
//                 this.oppositePlayerReserveCards = this.cardManager.getPlayerCurrentReserveCards(this.oppositePlayer);

//                 if (currentCard.isDead) {
//                     var column = currentCard.formationColumn;
//                     if (this.isBloodClash && this.currentPlayerReserveCards[column]) {
//                         // swich in reserve
//                         var reserveCard = this.currentPlayerReserveCards[column];

//                         this.cardManager.switchCardInAllCurrentMainCards(currentCard, reserveCard);
//                         this.currentPlayerMainCards[column] = reserveCard;
//                         this.currentPlayerReserveCards[column] = null;

//                         this.logger.addMajorEvent({
//                             description: currentCard.name + " is switched by " + reserveCard.name,
//                         });

//                         this.logger.addMinorEvent({
//                             description: currentCard.name + " is switched by " + reserveCard.name,
//                             type: ENUM.MinorEventType.RESERVE_SWITCH,
//                             reserveSwitch: {
//                                 mainId: currentCard.id,
//                                 reserveId: reserveCard.id
//                             }
//                         });

//                         currentCard = reserveCard;

//                         // proc opening skill when switch in
//                         var openingSkill = currentCard.getRandomOpeningSkill();
//                         if (openingSkill) {
//                             var data: SkillLogicData = {
//                                 executor: currentCard,
//                                 skill: openingSkill
//                             };
//                             if (openingSkill.willBeExecuted(data)) {
//                                 this.logger.addMajorEvent({
//                                     description: currentCard.name + " procs " + openingSkill.name,
//                                     executorId: currentCard.id,
//                                     skillId: openingSkill.id
//                                 });
//                                 openingSkill.execute(data);
//                             }
//                         }
//                     }
//                     else {
//                         continue;
//                     }
//                 }

//                 var missTurn = !currentCard.canAttack();
//                 if (missTurn) {
//                     this.logger.addMajorEvent({
//                         description: currentCard.name + " missed a turn"
//                     });
//                 }

//                 // procs active skill if we can
//                 this.processActivePhase(currentCard, "FIRST");
//                 if (this.isFinished) break;

//                 // hopefully there's no mounted fam with extra turn buff and extra turn passive, coz
//                 // I have no idea how all that logic will play with each other
//                 var passiveSkill = currentCard.getPassiveSkill();
//                 if (!currentCard.isDead && currentCard.status.willAttackAgain !== 0) {
//                     this.processActivePhase(currentCard, "FIRST");
//                     // todo: send a minor event log and handle it
//                     currentCard.status.willAttackAgain = 0;
//                     if (this.isFinished) break;
//                 }
//                 else if (!currentCard.isDead && passiveSkill && passiveSkill.skillFunc === ENUM.SkillFunc.EXTRA_TURN_PASSIVE) {
//                     if (passiveSkill.willBeExecuted({ executor: currentCard, skill: passiveSkill })) {
//                         this.logger.addMinorEvent({
//                             description: currentCard.name + " gets an extra turn!",
//                             type: ENUM.MinorEventType.TEXT
//                         });
//                         this.processActivePhase(currentCard, "FIRST");
//                         if (this.isFinished) break;
//                     }
//                 }

//                 if (!currentCard.isDead) {
//                     // update the affliction
//                     if (currentCard.getAfflictionType() !== ENUM.AfflictionType.BURN) {
//                         var cured = currentCard.updateAffliction();
//                     }

//                     // if cured, make a log
//                     if (!currentCard.affliction && cured) {
//                         var desc = currentCard.name + " is cured of affliction!";

//                         this.logger.addMinorEvent({
//                             targetId: currentCard.id,
//                             type: ENUM.MinorEventType.AFFLICTION,
//                             affliction: {
//                                 type: currentCard.getAfflictionType(),
//                                 isFinished: true,
//                             },
//                             description: desc,
//                         });
//                     }

//                     this.processOnDeathPhase();
//                 }

//                 this.checkFinish();
//             }

//             if (!this.isFinished) {
//                 this.processEndTurn();
//             }
//         }

//         // create debugger if needed
//         if (BattleDebugger.IS_DEBUG_MODE) {
//             BattleDebugger.getInstance().displayDebugger();
//         }

//         return this;
//     }


//     function performOpeningSkills () {
//         // the cards sorted by proc order
//         var p1cards = cardManager.getPlayerCurrentMainCardsByProcOrder(player1);
//         var p2cards = cardManager.getPlayerCurrentMainCardsByProcOrder(player2);
//         for (var i = 0; i < p1cards.length; i++) {
//             var skill1 = p1cards[i].getRandomOpeningSkill();
//             if (skill1) {
//                 var data = {
//                     executor: p1cards[i],
//                     skill: skill1
//                 };
//                 if (skill1.willBeExecuted(data)) {
//                     this.logger.addMajorEvent({
//                         description: p1cards[i].name + " procs " + skill1.name,
//                         executorId: p1cards[i].id,
//                         skillId: skill1.id
//                     });
//                     skill1.execute(data);
//                 }
//             }
//         }
//         for (var i = 0; i < p2cards.length; i++) {
//             var skill2 = p2cards[i].getRandomOpeningSkill();
//             if (skill2) {
//                 data = {
//                     executor: p2cards[i],
//                     skill: skill2
//                 };
//                 if (skill2.willBeExecuted(data)) {
//                     this.logger.addMajorEvent({
//                         description: p2cards[i].name + " procs " + skill2.name,
//                         executorId: p2cards[i].id,
//                         skillId: skill2.id
//                     });
//                     skill2.execute(data);
//                 }
//             }
//         }
//         this.turnOrderChanged = false;
//     }

// } 
