class CardManager2 {

	constructor(){
		if (CardManager2._instance) {
            throw new Error("Error: Instantiation failed: Use getInstance() instead of new.");
        }
        CardManager2._instance = this;
	}
	
	getPlayersCurrentMainCards(player){
		var playerCards = player.cards.main;
		var copy = [];
		for (var i = 0; i < playerCards.length; i++) {
		    copy.push(playerCards[i]);
		}
		copy.sort(function (a, b) { return a.procIndex - b.procIndex; });
		return copy;
	}	

	getPlayerCurrentMainCards = function (player) {
        var battle = this.battle;
        if (player === battle.player1) {
            return battle.player.cards.main;
        }
        else if (player === battle.player2) {
            return battle.player.cards.main;
        }
        else {
            throw new Error("Invalid player");
        }
    };

	getInstance = function () {
        if (CardManager2._instance === null) {
            throw new Error("Error: you should not make this object this way");
        }
        return CardManager2._instance;
    };
    removeInstance = function () {
        this._instance = null;
    };
}