//console.log('%cWelcome to blood brothers pvp simulator! \n', 'background: #222; color: #33BEFF');

var team_one = {
	1:{id:1,name:"Ghislandi"}, 
	2:{id:2,name:"Zero"},
	3:{id:3,name:"Aegir"},
	4:{id:4,name:"Sabnock"},
	5:{id:5,name:"Van"}
};

var team_two = {
	1:{id:6,name:"Sekhmet"}, 
	2:{id:7,name:"Batraz"},
	3:{id:8,name:"Gorlin"},
	4:{id:9,name:"Gregory"},
	5:{id:10,name:"Kobold"}
};

function getRandomStat(hero) {
	if (!!hero) {
		function randomNumber(){return Math.round( Math.random() * (2000 - 1000) + 1000)}
		Object.assign(hero, 
			{
				HP:randomNumber(),
				ATK:randomNumber(),
				DEF:Math.round(randomNumber()*0.4),
				WIS:randomNumber(),
				AGI:randomNumber()
			}
		);
		return hero;
	}
}

var teams = {team_one,team_two};

function add_stats(teams) {
	for (var team in teams) {
		for (var hero in teams[team]) {
			getRandomStat(teams[team][hero]);
			teams[team][hero]["alive"] = true;
			teams[team][hero]["status"] = "alive";
		}
	}
}
add_stats(teams);

// console.log('%cTeam One! \n', 'background: #222; color: #33BEFF');
// console.table(team_one);
// console.log('%cTeam Two! \n', 'background: #222; color: #33BEFF');
// console.table(team_two);

function fight(teams) {
	/* Get attack speed (wis)
	Order each hero based on attack speed, attack the enemy in response
	hp - atk
	*/
	//attack speed
	/*
	get the attack speed for every fam in both teams and order them 
	*/

	/*
	Check turn
		check if turn one
			Opening skills
				Get all opening skills for both teams and add to an array
				Order by wis
				Check based on percentage if it should run
				Remove from array if it shouldn't.
				~~ Recalculate this based on the an opening spells condition? ~~
		If not turn one
			Check if dead
				If dead
					Remove from living array
					Add to death array for that team
						Add order of death independant of team to this
	Order attackers
		Order team one based on AGI 
		Order team two based on AGI
		Add each to an attack order array
	Start fight
		Opening skills
			Order all teams OPENING SKILL heros by wisdom
			Get hero opening skill percentage and check if it should run
	*/

	/* 
	Battle 
		Order attackers
			Order team one based on AGI 
			Order team two based on AGI
			Add each to an attack order array
		Start fight
			Get first id from attack array
			Get ATK of first hero via it's ID
				Get 1 random id from opposit team of Attacker
				Get defence of that hero
				Reduce ATK of attacking hero by defence of defending hero
					Reduce hp of defending hero
						If hp is reduced to zero apply dead status to defending hero
							Remove hero from attacking and defending arrays
			Next attacker

	*/
	

	function orderTeamsByAGI(teams) {
		var teamsOrderedByAGI = [];

		for (var team in teams) {
			for (var hero in teams[team]) {
				var agi = [teams[team][hero]["AGI"]];

				var teamHeroAgi = [team,hero,agi];
				teamsOrderedByAGI.push(teamHeroAgi);
			}
		}
		
		function Comparator(a, b) {
			if (a[2] < b[2]) return 1;
			if (a[2] > b[2]) return -1;
			return 0;
		}
		return teamsOrderedByAGI.sort(Comparator);
		
	}

	//Attack order
	orderTeamsByAGI(teams).forEach(function(h) {
		var team = h[0];
		var hero = teams[team][h[1]];
		if (hero['alive']) {
			//get random enemy team hero, check if they're alive, hit them
			if (team == 'team_two') {var oppositTeam = team_one }else{var oppositTeam = team_two}

			var randomProperty = function (obj) {
			    var keys = Object.keys(obj)
			    return obj[keys[ keys.length * Math.random() << 0]];
			};
		}
		//console.log(hero)
	});
	var _atk = team_two[1]; //attacker
	var _dfn = team_one[1]; //defender

	var health = _dfn['HP'];
	var defence = _dfn['DEF'];
	var enemyATK = _atk['ATK'] - defence;

	var result = health - enemyATK; //resultingHealth

	if (result < 0) {
		_dfn['HP'] = result.toString();
		_dfn['status'] = "dead";
	}else{
		_dfn['HP'] = result.toString();
	}
}
fight(teams);
fight(teams);
//console.log('%cTeam One! \n', 'background: #222; color: #33BEFF');
console.table(teams['team_one']);
console.table(teams['team_two']);


